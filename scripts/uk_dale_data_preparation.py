import pandas as pd
import logging
from typing import List, Union, Dict
from datetime import datetime, date
import pickle
import json
from dateutil.parser import parse

import os
import sys


NUM_MAINS = 0  # FIXME make sure not already attributed in labels!!


def preprocess(df: pd.DataFrame, meta: Dict) -> pd.DataFrame:
    date_begin = datetime(year=2014, month=7, day=1)
    date_end = datetime(year=2014, month=10, day=15)
    datetime_index = pd.date_range(start=date_begin, end=date_end, freq="30min")
    df_prepro = pd.DataFrame([], index=datetime_index)

    for name, data_name in meta.items():
        logging.debug(f"{name} {data_name}")
        if data_name["begin"] > date_begin or data_name["end"] < date_end:
            logging.debug(f"{name} will not be considered")
        else:
            df_prepro[name] = df.loc[date_begin:date_end, name]

    del df_prepro[
        "aggregate"
    ]  # FIXME good idea? see what aggregate corresponds to! it seems it is not equal to the sum of the other columns...

    return df_prepro


def json_serial(obj):  # found on the internet
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError(f"Type {type(obj)} not serializable")


def deserialize_dates(
    data,
):  # TODO could probably do it right when loading the json data #TODO make sure no misinterpretation when reading dates (inversion of month and day, for instance)
    meta = {}
    for name, data_name in data.items():
        begin = parse(data_name["begin"])
        end = parse(data_name["end"])
        meta.update({name: {"begin": begin, "end": end}})
    return meta


def resample_dataframes(dfs: Dict[str, pd.Series]) -> Dict[str, pd.Series]:
    target_freq_str = "30min"

    resampled_dfs: Dict[str, pd.Series] = {}

    for name, series in dfs.items():
        series = series.resample(target_freq_str).mean()
        resampled_dfs.update({name: series.copy(deep=True)})

    return resampled_dfs


def get_dataframes(all_datafiles: Dict[int, str], labels_datafile: str):
    dfs: Dict[str, pd.Series] = dict()
    logging.debug(f"{labels_datafile}")

    df_labels = pd.read_csv(
        labels_datafile,
        sep=" ",
        header=None,
        index_col=[0],
        names=["index", "name"],
        usecols=["index", "name"],
    )

    for num_channel, filepath in all_datafiles.items():
        if num_channel == NUM_MAINS:
            name_channel = "mains"
        else:
            name_channel = df_labels.loc[num_channel, "name"]
        logging.debug(f"num channel: {num_channel}, name: {name_channel}")
        df = pd.read_csv(
            filepath,
            sep=" ",
            header=None,
            names=["date_and_time", "power"],
            usecols=["date_and_time", "power"],
        )
        df.set_index("date_and_time", inplace=True)
        df.index = [datetime.fromtimestamp(timestamp) for timestamp in df.index]
        df = df[
            "power"
        ]  # using pd.Series(df) leaded to "truth value of a Series is ambiguous", therefore using this
        df.name = f"{name_channel}"
        dfs.update({name_channel: df.copy(deep=True)})

    return dfs


def filter_files(all_files: dict) -> Union[Dict[int, str], str]:
    all_datafiles: Dict[int, str] = dict()

    for parent_dir, list_filenames in all_files.items():
        for filename in list_filenames:
            if "press" not in filename and "label" not in filename:
                if "mains" not in filename:
                    num_channel = filename.split("_")[1].split(".")[0]
                else:
                    num_channel = NUM_MAINS
                all_datafiles.update({int(num_channel): parent_dir + filename})
            elif "labels" in filename:
                labels_datafile = parent_dir + filename
                logging.debug(f"{filename}")

    return all_datafiles, labels_datafile


def get_house_datafiles(rep_original_data):
    logging.debug(f"rep_original_data: {rep_original_data}")

    all_paths = list()
    all_dirpaths = list()
    all_dirnames = list()
    parent_dir_filenames = dict()

    for dirpath, dirnames, filenames in os.walk(rep_original_data):
        logging.debug(f"dirnames: {dirnames}")
        all_paths.extend(filenames)
        all_dirpaths.extend(dirnames)
        all_dirnames.extend(dirnames)
        if len(filenames) > 0:
            parent_dir_filenames.update({dirpath + "/": filenames})

    logging.debug(f"{all_dirpaths}")
    logging.debug(f"{all_dirnames}")
    logging.debug(f"{parent_dir_filenames.keys()}")

    all_files = {
        parent_dir: list(
            filename
            for filename in list_filenames
            if ".dat" in filename  # FIXME make sure this is sound
        )
        for parent_dir, list_filenames in parent_dir_filenames.items()
    }

    logging.debug(f"all files: {all_files}")

    all_datafiles, labels_datafile = filter_files(all_files)

    logging.debug(f"all correct data files: {all_datafiles}")

    return all_datafiles, labels_datafile


def main():
    logging.basicConfig(level=logging.DEBUG)
    num_house = 5  # 2
    logging.debug(f"Entering data preparation for house {num_house} of UK-Dale")

    rep_original_data = f"./data/inputs/uk-dale/house_{num_house}/"

    power_profile_filename = rep_original_data + "power_profile.pkl"
    meta_data_filename = rep_original_data + "meta.json"

    if os.path.exists(power_profile_filename):
        df = pd.read_pickle(power_profile_filename)
        with open(meta_data_filename, "r") as f:
            meta = json.load(f)
            meta = deserialize_dates(meta)
    else:
        all_datafiles, labels_datafile = get_house_datafiles(rep_original_data)
        dfs = get_dataframes(all_datafiles, labels_datafile)

        dfs = resample_dataframes(dfs)

        for name, df in dfs.items():
            logging.debug(f"{name}:\n{df.head()}\nlast date: {df.index[-1]}")

        df = pd.DataFrame()
        meta = {}
        for name, series in dfs.items():
            logging.debug(f"{name}, time range: {series.index[0]} {series.index[-1]}")
            meta.update({name: {"begin": series.index[0], "end": series.index[-1]}})

            # df = pd.concat([df, pd.DataFrame(series)], axis='index')
            df = pd.merge(
                left=df, right=series, left_index=True, right_index=True, how="outer"
            )

        df.to_pickle(power_profile_filename)
        with open(meta_data_filename, "w") as f:
            json.dump(meta, f, default=json_serial)

    logging.debug(f"{df.head()}\n{df.describe()}")

    if False:  # True
        vs.visualise_consumption_by_appliance(
            adviser_type="DataPreparation",
            energy_consumption=df,
            output_rep=rep_original_data,
        )

    df_prepro = preprocess(df, meta)

    logging.debug(f"df prepro index:\n{df_prepro.index}\nhead:\n{df_prepro.head()}")

    filename = rep_original_data + f"house_{num_house}" + ".csv"
    df_prepro.to_csv(filename)


if __name__ == "__main__":
    sys.path.append("")
    from src.utils.visualisation import visualisation as vs

    main()

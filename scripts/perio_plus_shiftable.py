# import modules
import sys
import logging
import pandas as pd
import numpy as np
import random
import datetime
import math
from scipy import stats

NB_SECONDS_IN_DAY = 24 * 60 * 60
PI = 3.14

NB_STD_SIGNIFICANT = float(2)


# functions
def is_stochastic(mat):
    if not np.isclose(np.sum(mat, axis=1), 1).all():
        raise
    if not (0 <= mat).all() or not (mat <= 1).all():
        raise


def compute_length(use_interval):
    return (use_interval[1] - use_interval[0]).total_seconds()


class Periodical:
    def __init__(self):
        self.period_in_days = 7
        self.pulsation_in_seconds = (
            2 * PI / (self.period_in_days * float(NB_SECONDS_IN_DAY))
        )
        self.average_power = 500

    def compute(self, nb_seconds: int):
        return (
            self.average_power
            / 2
            * (2 + math.cos(self.pulsation_in_seconds * nb_seconds))
        )

    def __call__(self, nb_seconds: int):
        return self.compute(nb_seconds)


class PeriodicalFromWholeProfile:
    def __init__(self, begin, end, values):
        self.begin = begin
        self.end = end
        self.values = values.loc[begin:end]
        self.total_seconds = compute_length((self.begin, self.end))

        print(
            f"length: {compute_length((self.values.index[0], self.values.index[-1]))}"
        )

    @classmethod
    def generate(cls, length_in_days: int, series: pd.Series):
        begin = series.index[0]
        end = begin + datetime.timedelta(days=length_in_days)

        def get_equivalent_date(date_and_time: datetime.datetime):
            diff = date_and_time - begin
            diff_in_days = diff.days
            if diff_in_days < length_in_days:
                eq_date = date_and_time
            else:
                remainder = diff_in_days % length_in_days
                eq_date = begin + datetime.timedelta(
                    days=remainder,
                    hours=date_and_time.time().hour,  # FIXME only what we want because begin time is mignight!! improve!!
                    minutes=date_and_time.time().minute
                    - 30,  # FIXME only what we want because begin time is mignight!! improve!!
                )
            # print(f'equivalent date: {eq_date} origina date_and_time: {date_and_time}')
            return eq_date

        df = pd.DataFrame(series)
        df["equivalent_date"] = df.index.to_series().apply(
            lambda date_and_time: get_equivalent_date(date_and_time)
        )
        grouped = df.groupby(by="equivalent_date")
        folded_series = grouped.agg(
            np.mean
        )  # should be the series averaged over length_in_days days
        # assert False, f'{folded_series}'
        return cls(
            begin, end, folded_series[0]
        )  # if not asking for the column "0", which is a series, we send back a dataframe, and this will cause problems with the call "return self.values[date_and_time]" in compute!

    def compute(self, nb_seconds: int):  # TODO in turns, factor, and call!
        remainder_seconds = nb_seconds % self.total_seconds  # to make it periodical
        date_and_time = self.begin + datetime.timedelta(seconds=remainder_seconds)
        # print(f'begin: {self.begin}, end: {self.end}, date_and_time: {date_and_time}')
        # print(f'index: {self.values.index}')
        # assert date_and_time in self.values.index, f'{date_and_time in self.values.index}'
        # error with the line below, I do not understand why, given the above... -> it was before, in the generate method, what was sent in the statement "return cls(begin, end, folded_series)" was a dataframe, and not a series (corrected now with folded_series[0])
        # TODO still one error, it seems to be a question of inclusion/exclusion for the self.end value, but understand, and correct!!
        return self.values[date_and_time]

    def __call__(self, nb_seconds: int):
        return self.compute(nb_seconds)


class WholeProfile:
    def __init__(self, begin, end, values):
        self.begin = begin
        self.end = end
        self.values = values.loc[begin:end]
        self.total_seconds = compute_length((self.begin, self.end))

        print(
            f"length: {compute_length((self.values.index[0], self.values.index[-1]))}"
        )

    @classmethod
    def generate(cls, series: pd.Series):
        begin = series.index[0]
        end = series.index[-1]
        return cls(begin, end, series)

    def compute(self, nb_seconds: int):  # TODO in turns, factor, and call!
        remainder_seconds = nb_seconds % self.total_seconds  # to make it periodical
        date_and_time = self.begin + datetime.timedelta(seconds=remainder_seconds)
        return self.values[date_and_time]

    def __call__(self, nb_seconds: int):
        return self.compute(nb_seconds)


class WMProfile:
    def __init__(self, begin, end, values):
        self.begin = begin
        self.end = end
        self.values = values.loc[begin:end]
        self.total_seconds = compute_length((self.begin, self.end))

        print(
            f"length: {compute_length((self.values.index[0], self.values.index[-1]))}"
        )

    @classmethod
    def generate(cls, wm_data: pd.Series):
        use_intervals_wm = wm_data.use_intervals
        print(len(use_intervals_wm))
        all_lengths_use_intervals = [
            compute_length(interval) for interval in use_intervals_wm
        ]
        mean_len_use_interval = np.mean(all_lengths_use_intervals)
        print(
            f"mean_len_use_interval: {mean_len_use_interval}\nin minutes: {mean_len_use_interval/60.}"
        )
        print(
            f"max_len_use_interval: {np.max(all_lengths_use_intervals)}\nin minutes: {np.max(all_lengths_use_intervals)/60.}"
        )

        random_use_interval = random.choice(use_intervals_wm)
        print(
            f"random use interval: {random_use_interval} length: {compute_length(random_use_interval)}"
        )
        return cls(
            random_use_interval[0], random_use_interval[1], wm_data.energy_profile
        )

    def compute(self, nb_seconds: int):
        remainder_seconds = nb_seconds % self.total_seconds  # to make it periodical
        date_and_time = self.begin + datetime.timedelta(seconds=remainder_seconds)
        return self.values[date_and_time]

    def __call__(self, nb_seconds: int):
        return self.compute(nb_seconds)


class ShiftableComponent:
    def __init__(self, wm_profile: WMProfile):
        self.wm_profile = wm_profile

        self.delays = self.compute_delays(self.wm_profile.total_seconds)
        print(f"all delays: {self.delays}")
        self.activation_times_in_days = np.cumsum(self.delays)
        print(f"activation times in days: {self.activation_times_in_days}")
        self.activation_times_in_seconds = (
            self.activation_times_in_days * NB_SECONDS_IN_DAY
        )

    def compute(self, nb_seconds: int):
        condition_array = np.where(self.activation_times_in_seconds > nb_seconds)
        if len(condition_array) > 0:
            index_min = np.min(condition_array)
            if index_min > 0:
                nb_seconds_min = self.activation_times_in_seconds[index_min - 1]
                diff_seconds = nb_seconds - nb_seconds_min
                if (
                    diff_seconds < self.wm_profile.total_seconds
                ):  # TODO here, or an equivalent check in wm_profile (see also remainder in wm_profile, with the modulo)?
                    # print(f'nb_seconds: {nb_seconds}, diff seconds: {diff_seconds}')
                    return self.wm_profile(diff_seconds)
        return 0

    def __call__(self, nb_seconds: int):
        return self.compute(nb_seconds)

    def compute_delays(self, wm_total_seconds):
        # TODO should make sure delays (between two usages of the wm) are further away than wm_total_seconds
        nb_jumps = 10
        delays = list()

        possible_delays_in_days = [1.5, 3.25, 5.25]
        sto_matrix = np.array([[0.1, 0.25, 0.65], [0.2, 0.2, 0.6], [0.3, 0.3, 0.4]])
        # making sure this is a stochastic matrix
        is_stochastic(sto_matrix)

        jump = random.choice(possible_delays_in_days)
        delays.append(jump)
        # see https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.rv_discrete.html for scipy rvs
        for _ in range(nb_jumps - 1):
            custm = stats.rv_discrete(
                name="custm",
                values=(
                    possible_delays_in_days,
                    sto_matrix[possible_delays_in_days.index(jump), :],
                ),
            )
            jump = custm.rvs()
            delays.append(jump)

        return np.array(delays)


def create_df_conso(
    date_begin: datetime.datetime, nb_days: int, periodical, shiftable_component
) -> pd.DataFrame:
    # date_begin = whole_profile.begin #whole_wo_wm_household_consumption.index[0]#datetime.datetime(year=2000, month=1, day=1, hour=8, minute=0)
    date_end = date_begin + datetime.timedelta(days=nb_days)
    # date_end = date_end.combine(date_end.date(), datetime.time(hour=date_begin.hour, minute=date_begin.hour))
    date_range = pd.date_range(date_begin, date_end, freq="30min")

    df_conso = pd.DataFrame(
        0, index=date_range, columns=["total", "periodical", "shiftable_component"]
    )

    # TODO add noise + trend?
    rng = np.random.default_rng()
    noise_pc = 0.5  # fix better!

    for date_and_time in date_range:
        diff_in_seconds = compute_length((date_begin, date_and_time))
        # value_wm_profile = wm_profile(diff_in_seconds)  #TODO beware, this is the wm PROFILE, not the value of the shitable component (which is more stg like wm_profile(diff_in_seconds - S_n), where S_n = J_0 + ... + J_n, and (J_n) is the Markov chain with the jumps!
        value_shiftable_component = shiftable_component(diff_in_seconds)
        df_conso.loc[date_and_time, "shiftable_component"] = value_shiftable_component

        # value_periodical = periodical(diff_in_seconds)
        # value_periodical = whole_wo_wm_household_consumption[date_and_time]
        value_periodical = periodical(diff_in_seconds)
        value_periodical *= (
            1 + noise_pc * rng.random()
        )  # multiplicative model for the noise, correct?
        df_conso.loc[date_and_time, "periodical"] = value_periodical

        df_conso.loc[date_and_time, "total"] = (
            value_periodical + value_shiftable_component
        )
        # print(f'{date_and_time}: value periodical: {value_periodical}\nvalue wm PROFILE: {value_wm_profile} value shiftable component : {value_shiftable_component}')

    return df_conso


def main():
    logging.basicConfig(level=logging.DEBUG)
    cfg = mn.get_config()

    plot_handler = ph.PlotHandler(cfg["output_rep"])

    external_world = ew.generate_external_world(cfg["external_world"])
    household = ihs.generate_household(cfg["household"], external_world)

    wo_wm_household_consumption = household.appliances_handler.energy_profile
    del wo_wm_household_consumption["production"]

    total_wo_wm_household_conso = wo_wm_household_consumption.sum(axis="columns")
    total_wo_wm_household_conso.name = "whole"
    total_wo_wm_household_conso = pd.DataFrame(total_wo_wm_household_conso)

    # TODO replace by the synthetic washing machine after the sum below? or not??
    # TODO + visualise the wm consumption, and the "use_intervals" computed, to make sure they are sound!
    del wo_wm_household_consumption["washing_machine"]

    whole_wo_wm_household_consumption = wo_wm_household_consumption.sum(axis="columns")

    wm_data = household.appliances_handler.shiftable_appliances["washing machine"]

    wm_profile = WMProfile.generate(wm_data)
    periodical = Periodical()
    shiftable_component = ShiftableComponent(wm_profile)

    # whole_profile = WholeProfile.generate(whole_wo_wm_household_consumption)
    whole_profile = PeriodicalFromWholeProfile.generate(  # std deviations in the profile create below should be zero!
        length_in_days=7, series=whole_wo_wm_household_consumption.copy(deep=True)
    )

    nb_days = 31
    df_conso = create_df_conso(
        date_begin=whole_profile.begin,
        nb_days=nb_days,
        periodical=whole_profile,
        shiftable_component=shiftable_component,
    )
    print(
        f'sanity check: sum of periodical must be approximately null equal to its average if a number of complete periods have happened:\nnumbers of periods: {nb_days/float(periodical.period_in_days)}, sum of periodical: {df_conso["periodical"].sum()}'
    )

    print(f"df conso:\n{df_conso.head(15)}")
    print(f"df conso stats:\n{df_conso.describe()}")

    #    household_conso_w_separated_wm = total_wo_wm_household_conso.copy(deep=True)
    #    household_conso_w_separated_wm = household_conso_w_separated_wm[
    #        df_conso.index[0]:df_conso.index[-1]
    #    ]
    household_conso_w_separated_wm = pd.DataFrame(df_conso["periodical"])
    household_conso_w_separated_wm.rename(columns={"periodical": "whole"}, inplace=True)
    household_conso_w_separated_wm["shiftable_component"] = df_conso[
        "shiftable_component"
    ]

    # creating profile
    target_freq = "6H"
    household_conso_w_separated_wm = household_conso_w_separated_wm.resample(
        target_freq
    ).mean()
    household_conso_w_separated_wm[
        "weekday"
    ] = household_conso_w_separated_wm.index.to_series().apply(
        lambda date_and_time: date_and_time.date().weekday()
    )
    household_conso_w_separated_wm[
        "time"
    ] = household_conso_w_separated_wm.index.to_series().apply(
        lambda date_and_time: date_and_time.time()
    )
    print(f"total household conso with weekdays:\n{household_conso_w_separated_wm}")
    grouped = household_conso_w_separated_wm.groupby(by=["weekday", "time"])
    profile = grouped.agg([np.mean, np.std])
    profile.sort_index(level=["weekday", "time"], ascending=True, inplace=True)
    print(f"profile:\n{profile}")
    print(
        "I do not understand why, when there is the PeriodicalFromWholeProfile periodical component, the std deviations are not all zero, even if most of them are..."
    )
    # TODO cf. what Rémi said, it may be necessary to 1. remove the wm from the computation of the averages and mean for the whole profile 2. it may be necessary to average over finer periods (over the summer and winter for instance), as for households with electrical heating, averaging over winter and summer makes no sense

    # target_freq = '6H'  # given above
    df_by_period = df_conso.resample(target_freq).mean()
    df_by_period["has_shiftable"] = df_by_period["shiftable_component"] > 0
    print(f"df by period stats:\n{df_by_period.describe()}")
    print(
        f'df by period has shiftable info\n{df_by_period["has_shiftable"].value_counts()}'
    )
    df_by_period.index.name = "date_and_time"
    print(f"df_by_period : {df_by_period}")

    # plot_handler.plot_df(df_conso)

    def is_significant_deviation(value, mean, std):
        return abs(value - mean) > NB_STD_SIGNIFICANT * std

    def get_value_from_profile(type_value, profile, date_and_time):
        weekday = date_and_time.date().weekday()
        time = date_and_time.time()
        return profile.loc[(weekday, time), ("whole", type_value)]

    df_by_period.reset_index(inplace=True)
    df_by_period["detection"] = df_by_period.apply(
        lambda row: is_significant_deviation(
            value=row["total"],
            mean=get_value_from_profile("mean", profile, row["date_and_time"]),
            std=get_value_from_profile("std", profile, row["date_and_time"]),
        ),
        axis=1,
    )

    df_by_period["true_positive"] = df_by_period.apply(
        lambda row: row["detection"] and row["has_shiftable"], axis=1
    )
    df_by_period["false_positive"] = df_by_period.apply(
        lambda row: row["detection"] and not row["has_shiftable"], axis=1
    )

    df_by_period.set_index("date_and_time", inplace=True)

    value_counts_has_shiftable = df_by_period["has_shiftable"].value_counts()
    nb_positives = value_counts_has_shiftable[True]
    nb_negatives = value_counts_has_shiftable[False]

    nb_true_positives = df_by_period["true_positive"].sum()
    nb_false_positives = df_by_period["false_positive"].sum()
    tpr = nb_true_positives / nb_positives  # true positive rate
    fpr = nb_false_positives / nb_negatives  # false positive rate
    # tpr, and fpr, seem to be well computed: see https://en.wikipedia.org/wiki/Receiver_operating_characteristic

    print(f"df_by_period : {df_by_period}")
    print(f"tpr: {tpr}, fpr: {fpr}")
    print(
        f"number true positives: {nb_true_positives}, number false positives: {nb_false_positives}"
    )
    print(f"number positives: {nb_positives}, number negatives: {nb_negatives}")

    plot_handler.plot_wm_usages(df_by_period.copy(deep=True))


if __name__ == "__main__":
    sys.path.append("")

    from src.simulator import external_world as ew
    from src.simulator.household.iris import iris_household as ihs

    import main as mn

    from src.utils.visualisation import plot_handler as ph

    main()

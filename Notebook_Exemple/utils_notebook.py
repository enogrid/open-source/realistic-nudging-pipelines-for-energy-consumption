import copy
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from src.utils.processing import data_processing as pr
from src.real_world_classes.data_collection import data_center as dc
from src.simulator import simulation_manager as sm
from src.real_world_classes.advisers import (
    WeatherController,
    IdleAdvisor,
    CombinedController,
)


def run_simulation(cfg: str, run_optimisation=False):
    advisers_choice = {
        "No control": IdleAdvisor,
        "WeatherController": WeatherController,  # Weather Controller
        "CombinedController": CombinedController,  # Combined Controller
    }

    sim_outputs = dict()
    training_outputs = dict()
    conso_by_week = dict()  # FIXME for debugging
    changes_conso = dict()

    advisors_list = [
        adviser_class.make_adviser(cfg["advisers"].get(adviser_name))
        for adviser_name, adviser_class in advisers_choice.items()
    ]

    once_and_for_all_simulator = sm.make_simulator(cfg["simulation"])
    for advisor in advisors_list:
        simulator = copy.deepcopy(once_and_for_all_simulator)
        data_center = dc.DataCenter(simulator)

        advisor.set_historical_data(data_center)

        training_outputs_adviser = advisor.train()

        sim_tracking_data = simulator.run_simulation(data_center, advisor)

        conso_by_week_adv = sim_tracking_data["conso_by_week"]
        conso_by_week.update({advisor.name: conso_by_week_adv})
        changes_conso_adv = sim_tracking_data["changes_conso"]
        changes_conso.update({advisor.name: changes_conso_adv})

        sim_outputs.update({advisor.name: data_center.get_simulation_outputs()})
        training_outputs.update({advisor.name: copy.deepcopy(training_outputs_adviser)})

        if advisor.name == "CombinedController":  # FIXME improve implementation
            sim_outputs[advisor.name].update(
                {"predicted_excess": sim_tracking_data["predicted_excess"]}
            )

    processed_data = pr.process_outputs(sim_outputs, cfg["output_rep"])
    processed_training_outputs = pr.process_training_outputs(
        training_outputs, cfg["output_rep"]
    )
    (
        collections_infos,
        historical_conso_prod,
        adviser_conso_prod,
    ) = pr.collections_processed_data(processed_data, processed_training_outputs)

    if run_optimisation == True:
        (
            collections_infos,
            adviser_conso_prod,
        ) = pr.collect_optimisation_data(collections_infos, adviser_conso_prod)

    return collections_infos, historical_conso_prod, adviser_conso_prod


def visualisation_influence_of_production_level(
    coeff_sunshine: list, collection_infos_df: pd.DataFrame, nb_run: int
):
    self_consumption_10_percentile = []
    self_consumption_90_percentile = []
    self_production_10_percentile = []
    self_production_90_percentile = []
    self_consumption_mean = []
    self_production_mean = []
    for i in range(len(collection_infos_df)):
        if i % nb_run == 0:
            collection_infos_per_coefficient = collection_infos_df.iloc[i : i + nb_run]

            diff_w_consumpt = (
                collection_infos_per_coefficient[
                    "WeatherController_agg_self_consumption_rate"
                ]
                - collection_infos_per_coefficient[
                    "No control_agg_self_consumption_rate"
                ]
            )
            diff_e_consumpt = (
                collection_infos_per_coefficient[
                    "CombinedController_agg_self_consumption_rate"
                ]
                - collection_infos_per_coefficient[
                    "No control_agg_self_consumption_rate"
                ]
            )

            diff_w_prod = (
                collection_infos_per_coefficient[
                    "WeatherController_agg_self_production_rate"
                ]
                - collection_infos_per_coefficient[
                    "No control_agg_self_production_rate"
                ]
            )
            diff_e_prod = (
                collection_infos_per_coefficient[
                    "CombinedController_agg_self_production_rate"
                ]
                - collection_infos_per_coefficient[
                    "No control_agg_self_production_rate"
                ]
            )

            df_w_c_90 = np.percentile(diff_w_consumpt, 90)
            df_e_c_90 = np.percentile(diff_e_consumpt, 90)
            df_w_p_90 = np.percentile(diff_w_prod, 90)
            df_e_p_90 = np.percentile(diff_e_prod, 90)

            df_w_c_10 = np.percentile(diff_w_consumpt, 10)
            df_e_c_10 = np.percentile(diff_e_consumpt, 10)
            df_w_p_10 = np.percentile(diff_w_prod, 10)
            df_e_p_10 = np.percentile(diff_e_prod, 10)

            df_w_c_m = np.mean(diff_w_consumpt)
            df_e_c_m = np.mean(diff_e_consumpt)

            df_w_p_m = np.mean(diff_w_prod)
            df_e_p_m = np.mean(diff_e_prod)

            self_consumption_10_percentile.append([df_w_c_10, df_e_c_10])
            self_consumption_90_percentile.append([df_w_c_90, df_e_c_90])
            self_production_10_percentile.append([df_w_p_10, df_e_p_10])
            self_production_90_percentile.append([df_w_p_90, df_e_p_90])

            self_consumption_mean.append([df_w_c_m, df_e_c_m])
            self_production_mean.append([df_w_p_m, df_e_p_m])

    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(20, 10))

    fig.suptitle(
        "Augmentation of the self-consumption/ production rate due to the Combined Controller/ WeatherController with respect to a reference with no controller, as a function of the production"
    )
    axes[0].fill_between(
        coeff_sunshine,
        [i[0] for i in self_consumption_10_percentile],
        [i[0] for i in self_consumption_90_percentile],
        alpha=0.1,
        color="b",
        label="difference between Weather Controller and Reference",
    )
    axes[0].plot(coeff_sunshine, [i[0] for i in self_consumption_mean])
    axes[0].fill_between(
        coeff_sunshine,
        [i[1] for i in self_consumption_10_percentile],
        [i[1] for i in self_consumption_90_percentile],
        alpha=0.1,
        color="r",
        label="difference between Combined Controller and Reference",
    )
    axes[0].plot(coeff_sunshine, [i[1] for i in self_consumption_mean])
    axes[0].set_ylabel("self-consumption rate improvement(%)", fontsize=12)
    axes[0].set_xlabel("production level", fontsize=12)

    axes[0].legend()

    axes[1].fill_between(
        coeff_sunshine,
        [i[0] for i in self_production_10_percentile],
        [i[0] for i in self_production_90_percentile],
        alpha=0.1,
        color="b",
        label="difference between Weather Controller and Reference",
    )
    axes[1].plot(coeff_sunshine, [i[0] for i in self_production_mean])
    axes[1].fill_between(
        coeff_sunshine,
        [i[1] for i in self_production_10_percentile],
        [i[1] for i in self_production_90_percentile],
        alpha=0.1,
        color="r",
        label="difference between Combined Controller and Reference",
    )
    axes[1].plot(coeff_sunshine, [i[1] for i in self_production_mean])
    axes[1].set_ylabel("self-sufficiency rate improvement(%)", fontsize=12)
    axes[1].set_xlabel("production level", fontsize=12)
    axes[1].legend()
    plt.show()


def visualisation_results_of_all_simulations(
    all_result: pd.DataFrame, result_type: str
):
    all_result = (all_result.transpose()) * 100

    if result_type == "consumption":
        col_change_nb = -26
        ylabel = "self-consumption rate (%)"
    elif result_type == "production":
        col_change_nb = -25
        ylabel = "self-sufficiency rate (%)"
    else:
        raise NotImplementedError

    result = all_result[[col for col in all_result.columns if result_type in col]]
    for col in result.columns:
        result = result.rename(columns={col: col[:col_change_nb]})

    result = result.rename(
        columns={
            "No control": "Reference",
            "WeatherController": "Weather Controller",
            "CombinedController": "Combined Controller",
        }
    )

    result.boxplot(figsize=(8, 4))
    plt.xlabel("Controllers")
    plt.ylabel(ylabel)
    plt.show()

    return result


def average_result_visualisation(mean_result: pd.DataFrame, result_type: str):
    fix, ax = plt.subplots(figsize=(8, 4))
    COLOUR_ADVISER = {
        "Weather Controller": "orange",
        "Reference": "blue",
        "Combined Controller": "green",
        "Omegalpes": "yellow",
    }

    if result_type == "consumption":
        ylabel = "average self-consumption rate"
    elif result_type == "production":
        ylabel = "average self-sufficiency rate"
    else:
        raise NotImplementedError
    result = {key: values for key, values in mean_result.items()}

    colors_to_use = [COLOUR_ADVISER[adviser_type] for adviser_type in result.keys()]
    ax.bar(
        result.keys(),
        result.values(),
        color=colors_to_use,
    )
    plt.ylabel(ylabel)
    plt.title(f"{ylabel} for different advisers")
    plt.show()


def calculate_improvement_ratio(result: pd.DataFrame):
    ratio_weather = (result["Weather Controller"] - result["Reference"]) / (
        result["Omegalpes"] - result["Reference"]
    )
    ratio_combined = (result["Combined Controller"] - result["Reference"]) / (
        result["Omegalpes"] - result["Reference"]
    )
    ratio = pd.DataFrame(
        [ratio_weather, ratio_combined],
        index=["Ratio_WeatherController", "Ratio_CominedController"],
    )
    ratio = ratio.transpose()
    ratio = ratio * 100
    ratio.boxplot()
    plt.ylabel("improvement ratio (%)")
    plt.show()

    return ratio


def visualisation_influence_of_green_periods(
    nb_periods_max: list, collection_infos_df: pd.DataFrame, nb_run: int
):
    self_consumption_10_percentile = []
    self_consumption_90_percentile = []
    self_production_10_percentile = []
    self_production_90_percentile = []
    self_consumption_mean = []
    self_production_mean = []
    for i in range(len(collection_infos_df)):
        if i % nb_run == 0:
            collection_infos_per_coefficient = collection_infos_df.iloc[i : i + nb_run]

            diff_w_consumpt = (
                collection_infos_per_coefficient[
                    "WeatherController_agg_self_consumption_rate"
                ]
                - collection_infos_per_coefficient[
                    "No control_agg_self_consumption_rate"
                ]
            )
            diff_e_consumpt = (
                collection_infos_per_coefficient[
                    "CombinedController_agg_self_consumption_rate"
                ]
                - collection_infos_per_coefficient[
                    "No control_agg_self_consumption_rate"
                ]
            )

            diff_w_prod = (
                collection_infos_per_coefficient[
                    "WeatherController_agg_self_production_rate"
                ]
                - collection_infos_per_coefficient[
                    "No control_agg_self_production_rate"
                ]
            )
            diff_e_prod = (
                collection_infos_per_coefficient[
                    "CombinedController_agg_self_production_rate"
                ]
                - collection_infos_per_coefficient[
                    "No control_agg_self_production_rate"
                ]
            )

            df_w_c_90 = np.percentile(diff_w_consumpt, 90)
            df_e_c_90 = np.percentile(diff_e_consumpt, 90)
            df_w_p_90 = np.percentile(diff_w_prod, 90)
            df_e_p_90 = np.percentile(diff_e_prod, 90)

            df_w_c_10 = np.percentile(diff_w_consumpt, 10)
            df_e_c_10 = np.percentile(diff_e_consumpt, 10)
            df_w_p_10 = np.percentile(diff_w_prod, 10)
            df_e_p_10 = np.percentile(diff_e_prod, 10)

            df_w_c_m = np.mean(diff_w_consumpt)
            df_e_c_m = np.mean(diff_e_consumpt)

            df_w_p_m = np.mean(diff_w_prod)
            df_e_p_m = np.mean(diff_e_prod)

            self_consumption_10_percentile.append([df_w_c_10, df_e_c_10])
            self_consumption_90_percentile.append([df_w_c_90, df_e_c_90])
            self_production_10_percentile.append([df_w_p_10, df_e_p_10])
            self_production_90_percentile.append([df_w_p_90, df_e_p_90])

            self_consumption_mean.append([df_w_c_m, df_e_c_m])
            self_production_mean.append([df_w_p_m, df_e_p_m])

    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(20, 10))

    fig.suptitle(
        "Augmentation of the self-consumption/ production rate due to the Combined Controller/ WeatherController with respect to a reference with no controller, as a function of the number of green periods"
    )
    axes[0].fill_between(
        nb_periods_max,
        [i[0] for i in self_consumption_10_percentile],
        [i[0] for i in self_consumption_90_percentile],
        alpha=0.1,
        color="b",
        label="difference between Weather Controller and Reference",
    )
    axes[0].plot(nb_periods_max, [i[0] for i in self_consumption_mean])
    axes[0].fill_between(
        nb_periods_max,
        [i[1] for i in self_consumption_10_percentile],
        [i[1] for i in self_consumption_90_percentile],
        alpha=0.1,
        color="r",
        label="difference between Combined Controller and Reference",
    )
    axes[0].plot(nb_periods_max, [i[1] for i in self_consumption_mean])
    axes[0].set_ylabel("self-consumption rate improvement(%)", fontsize=12)
    axes[0].set_xlabel("Number of green periods", fontsize=12)

    axes[0].legend()

    axes[1].fill_between(
        nb_periods_max,
        [i[0] for i in self_production_10_percentile],
        [i[0] for i in self_production_90_percentile],
        alpha=0.1,
        color="b",
        label="difference between Weather Controller and Reference",
    )
    axes[1].plot(nb_periods_max, [i[0] for i in self_production_mean])
    axes[1].fill_between(
        nb_periods_max,
        [i[1] for i in self_production_10_percentile],
        [i[1] for i in self_production_90_percentile],
        alpha=0.1,
        color="r",
        label="difference between Combined Controller and Reference",
    )

    axes[1].plot(nb_periods_max, [i[1] for i in self_production_mean])
    axes[1].set_ylabel("self-sufficiency rate improvement(%)", fontsize=12)
    axes[1].set_xlabel("Number of green periods", fontsize=12)
    axes[1].legend()
    plt.show()

from pydantic import BaseModel
from datetime import datetime
import pandas as pd


# FIXME in turns, I think many things here, and in the whole code, should use PowerLib objects
class LoadForecastRequest(BaseModel):
    """Class to ask a forecast to predictors."""

    pred_from: datetime
    pred_to: datetime

    weather_forecast: pd.DataFrame

    class Config:
        arbitrary_types_allowed = True


class LoadForecast(BaseModel):
    """
    The class represents a forecast.

    Attributes:
    begin: beginning of the forecast period
    end: end of the forecast period

    forecast: values forecasted; in turns, should probably be a PowerLib LoadCurve
    """

    begin: datetime
    end: datetime

    forecast: pd.Series

    class Config:
        arbitrary_types_allowed = True  # using this because else, we get: no validator found for <class 'pandas.core.series.Series'>, see `arbitrary_types_allowed` in Config

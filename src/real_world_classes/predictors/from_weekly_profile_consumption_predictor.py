from datetime import datetime, timedelta
import pandas as pd
import numpy as np
import logging
from typing import Dict, List
from sklearn import linear_model

from src.real_world_classes.data_collection.data_models import (
    HouseholdEnergyData,
    ExternalWorldData,
)
from src.real_world_classes.predictors.load_forecast import (
    LoadForecast,
    LoadForecastRequest,
)


NB_DAYS_VALIDATION_SET = 21  # FIXME beware if too long for available data! #FIXME beware, modifying it may result in errors in train (as computations ad-hoc), but should not cause problems elsewhere!


class WeeklyProfileConsumptionPredictor:
    """
    Class description.

    More explanations.
    """

    def __init__(self, config):
        self.prediction_periods_length: timedelta = pd.Timedelta(
            config["prediction_periods_lengths"]
        )
        self.historical_data_as_dataframe: pd.DataFrame() = None
        self.daily_data_as_dataframe: pd.DataFrame() = None
        self.data_to_forecast = (
            "consumption"  # FIXME should probably be part of an enum or derived
        )

        self.model = linear_model.LinearRegression()
        logging.debug("From Weekly Profile Consumption Predictor initialised.")

    @classmethod
    def make_predictor(cls, config: dict):
        return cls(config)

    def train(self) -> dict:
        train_set = self.create_train_set()
        #        x_train, y_train = train_set['x_train'].values, train_set['y_train'].values
        #        x_train = x_train.reshape(-1, 1)  #in case only one feature
        #        y_train = y_train.reshape(-1, 1)
        #        self.model.fit(x_train, y_train)
        #
        #        # validation, probably put in another function
        x_val, y_val = train_set["x_val"], train_set["y_val"]
        pred_from = x_val.index[
            0
        ]  # TODO maybe predict from week to week, instead of right away over the whole period? #FIXME moreover, pb, as data for training will encompass pred_from to pred_to, as all is in historical?
        pred_to = x_val.index[-1]

        # FIXME DUPLICATED FROM POWER FORECAST, AWFUL!!!! done to have an idea of the quality of the prediction!!
        # creation of profile
        hist_begin = self.historical_data_as_dataframe.index[0]
        hist_lc = self.historical_data_as_dataframe.loc[
            hist_begin:pred_from, "consumption"
        ]  # FIXME possible that overlap is pred_from is included!!
        hist_lc = pd.DataFrame(hist_lc)
        hist_lc.index.name = "datetime"
        hist_lc.reset_index(inplace=True)

        hist_lc["dayofweek"] = hist_lc["datetime"].apply(lambda x: x.dayofweek)
        hist_lc["time"] = hist_lc["datetime"].apply(lambda x: x.time())

        grouped = hist_lc.groupby(by=["dayofweek", "time"], group_keys=True)
        weekly_profile = grouped[["consumption"]].agg(np.mean)

        x_pred = pd.date_range(start=pred_from, end=pred_to, freq="30min")
        y_pred = pd.DataFrame(index=x_pred)
        y_pred["consumption"] = x_pred.to_series().apply(
            lambda date_time: weekly_profile.loc[
                (date_time.dayofweek, date_time.time()), "consumption"
            ]
        )

        training_outputs = {"y_val": y_val, "y_pred": y_pred}
        return training_outputs

    def create_train_set(self) -> dict:
        train_begin = self.historical_data_as_dataframe.index[0]
        val_end = self.historical_data_as_dataframe.index[-1]

        train_end = val_end - timedelta(days=NB_DAYS_VALIDATION_SET - 1)
        val_begin = train_end + timedelta(minutes=30)

        x_train = self.historical_data_as_dataframe.loc[
            train_begin:train_end, "consumption"
        ]
        y_train = self.historical_data_as_dataframe.loc[
            train_begin:train_end, "consumption"
        ]

        x_val = self.historical_data_as_dataframe.loc[val_begin:val_end, "consumption"]
        y_val = self.historical_data_as_dataframe.loc[val_begin:val_end, "consumption"]

        train_set = {
            "x_train": x_train,
            "y_train": y_train,
            "x_val": x_val,
            "y_val": y_val,
        }
        return train_set

    def set_historical_data(  # FIXME in several modules, factor! and same for function getting daily data!
        self,
        household_data: List[HouseholdEnergyData],
        external_world_data: List[ExternalWorldData],
    ) -> None:
        all_datetimes = [record.begin for record in household_data]

        index = pd.DatetimeIndex(all_datetimes)
        columns_to_use = ["consumption", "production", "sunshine_coefficient"]
        df_hist = pd.DataFrame(0, index=index, columns=columns_to_use)
        df_hist.sort_index(ascending=True, inplace=True)

        for record in household_data:
            df_hist.loc[record.begin, "consumption"] = record.consumption
            df_hist.loc[record.begin, "production"] = record.production

        for record in external_world_data:
            df_hist.loc[
                record.begin, "sunshine_coefficient"
            ] = record.sunshine_coefficient
            df_hist.loc[record.begin, "temperature"] = record.temperature

        self.historical_data_as_dataframe = df_hist
        self.historical_data_as_dataframe.sort_index(inplace=True)
        self.historical_data_as_dataframe = self.historical_data_as_dataframe.dropna()

    def set_daily_data(
        self,
        household_data: List[HouseholdEnergyData],
        external_world_data: List[ExternalWorldData],
    ) -> None:
        all_datetimes = [record.begin for record in household_data]

        index = pd.DatetimeIndex(all_datetimes)
        columns_to_use = ["consumption", "production", "sunshine_coefficient"]
        df_daily = pd.DataFrame(0, index=index, columns=columns_to_use)
        df_daily.sort_index(ascending=True, inplace=True)

        for record in household_data:
            df_daily.loc[record.begin, "consumption"] = record.consumption
            df_daily.loc[record.begin, "production"] = record.production

        for record in external_world_data:
            df_daily.loc[
                record.begin, "sunshine_coefficient"
            ] = record.sunshine_coefficient
            df_daily.loc[record.begin, "temperature"] = record.temperature

        self.daily_data_as_dataframe = df_daily

    def power_forecast(self, lf_request: LoadForecastRequest) -> LoadForecast:
        """
        Predicts the load curve from pred_from to pred_to.

        Predictions are sampled according to the parameter self.prediction_periods_length.

        :param pred_from: beginning of requested prediction
        :param pred_to: end of requested prediction
        @return NOT CLEARLY DEFINED YET!
        """
        pred_from = lf_request.pred_from
        pred_to = lf_request.pred_to

        # creation of profile
        hist_lc = self.historical_data_as_dataframe["consumption"]
        hist_lc = pd.DataFrame(hist_lc)
        hist_lc.index.name = "datetime"
        hist_lc.reset_index(inplace=True)

        running_lc = self.daily_data_as_dataframe["consumption"]
        running_lc = pd.DataFrame(running_lc)
        running_lc.index.name = "datetime"
        running_lc.reset_index(inplace=True)
        past_lc = pd.concat(
            [hist_lc, running_lc]
        )  # TODO if "holes" in the data, maybe not good idea to concatenante...

        past_lc["dayofweek"] = past_lc["datetime"].apply(lambda x: x.dayofweek)
        past_lc["time"] = past_lc["datetime"].apply(lambda x: x.time())

        grouped = past_lc.groupby(by=["dayofweek", "time"], group_keys=True)
        weekly_profile = grouped[["consumption"]].agg(np.mean)

        x_pred = pd.date_range(start=pred_from, end=pred_to, freq="30min")
        y_pred = pd.DataFrame(index=x_pred)
        y_pred["consumption"] = x_pred.to_series().apply(
            lambda date_time: weekly_profile.loc[
                (date_time.dayofweek, date_time.time()), "consumption"
            ]
        )
        forecasted_data = y_pred["consumption"]
        forecast = LoadForecast(
            begin=lf_request.pred_from, end=lf_request.pred_to, forecast=forecasted_data
        )
        return forecast

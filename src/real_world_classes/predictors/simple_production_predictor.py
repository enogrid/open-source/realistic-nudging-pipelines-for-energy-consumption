from datetime import timedelta
import pandas as pd
import logging
from typing import List
from sklearn import linear_model

from src.real_world_classes.data_collection.data_models import (
    HouseholdEnergyData,
    ExternalWorldData,
)
from src.real_world_classes.predictors.load_forecast import (
    LoadForecast,
    LoadForecastRequest,
)


NB_DAYS_VALIDATION_SET = 28  # FIXME beware if too long for available data!


class SimpleProductionPredictor:
    def __init__(self, config: dict):
        self.prediction_periods_length: timedelta = pd.Timedelta(
            config["prediction_periods_lengths"]
        )
        self.historical_data_as_dataframe: pd.DataFrame() = None
        self.daily_data_as_dataframe: pd.DataFrame() = None
        self.data_to_forecast = (
            "production"  # FIXME should probably be part of an enum or derived
        )

        self.model = linear_model.LinearRegression()
        logging.debug("Simple Production Predictor initialised.")

    @classmethod
    def make_predictor(cls, config: dict):
        return cls(config)

    def train(self) -> dict:
        train_set = self.create_train_set()
        x_train, y_train = train_set["x_train"].values, train_set["y_train"].values
        x_train = x_train.reshape(-1, 1)  # in case only one feature
        y_train = y_train.reshape(-1, 1)
        self.model.fit(x_train, y_train)

        # validation, probably put in another function
        x_val = train_set["x_val"].values
        x_val = x_val.reshape(-1, 1)
        # y_val = y_val.reshape(-1, 1)
        y_pred = self.model.predict(x_val)

        y_pred = pd.Series(y_pred[:, 0], index=train_set["x_val"].index)

        training_outputs = {"y_val": train_set["y_val"], "y_pred": y_pred}
        return training_outputs

    def create_train_set(self) -> dict:
        train_begin = self.historical_data_as_dataframe.index[0]
        val_end = self.historical_data_as_dataframe.index[-1]

        train_end = val_end - timedelta(days=NB_DAYS_VALIDATION_SET - 1)
        val_begin = train_end + timedelta(minutes=30)

        x_train = self.historical_data_as_dataframe.loc[
            train_begin:train_end, "sunshine_coefficient"
        ]
        y_train = self.historical_data_as_dataframe.loc[
            train_begin:train_end, "production"
        ]

        x_val = self.historical_data_as_dataframe.loc[
            val_begin:val_end, "sunshine_coefficient"
        ]
        y_val = self.historical_data_as_dataframe.loc[val_begin:val_end, "production"]

        train_set = {
            "x_train": x_train,
            "y_train": y_train,
            "x_val": x_val,
            "y_val": y_val,
        }
        return train_set

    def set_historical_data(
        self,
        household_data: List[HouseholdEnergyData],
        external_world_data: List[ExternalWorldData],
    ) -> None:
        all_datetimes = [record.begin for record in household_data]

        index = pd.DatetimeIndex(all_datetimes)
        columns_to_use = ["consumption", "production", "sunshine_coefficient"]
        df_hist = pd.DataFrame(0, index=index, columns=columns_to_use)
        df_hist.sort_index(ascending=True, inplace=True)

        for record in household_data:
            df_hist.loc[record.begin, "consumption"] = record.consumption
            df_hist.loc[record.begin, "production"] = record.production

        for record in external_world_data:
            df_hist.loc[
                record.begin, "sunshine_coefficient"
            ] = record.sunshine_coefficient
            df_hist.loc[record.begin, "temperature"] = record.temperature

        self.historical_data_as_dataframe = df_hist
        self.historical_data_as_dataframe.sort_index(inplace=True)
        self.historical_data_as_dataframe = self.historical_data_as_dataframe.dropna()

    def set_daily_data(
        self,
        household_data: List[HouseholdEnergyData],
        external_world_data: List[ExternalWorldData],
    ) -> None:
        all_datetimes = [record.begin for record in household_data]

        index = pd.DatetimeIndex(all_datetimes)
        columns_to_use = ["consumption", "production", "sunshine_coefficient"]
        df_daily = pd.DataFrame(0, index=index, columns=columns_to_use)
        df_daily.sort_index(ascending=True, inplace=True)

        for record in household_data:
            df_daily.loc[record.begin, "consumption"] = record.consumption
            df_daily.loc[record.begin, "production"] = record.production

        for record in external_world_data:
            df_daily.loc[
                record.begin, "sunshine_coefficient"
            ] = record.sunshine_coefficient
            df_daily.loc[record.begin, "temperature"] = record.temperature

        self.daily_data_as_dataframe = df_daily

    def power_forecast(self, lf_request: LoadForecastRequest) -> LoadForecast:
        """
        Predicts the load curve from pred_from to pred_to.

        Predictions are sampled according to the parameter self.prediction_periods_length.

        :param pred_from: beginning of requested prediction
        :param pred_to: end of requested prediction
        @return NOT CLEARLY DEFINED YET!
        """
        pred_from = lf_request.pred_from
        pred_to = lf_request.pred_to
        sun_coeff_forecast = lf_request.weather_forecast["sunshine_coeff"]

        #        #FIXME reestablish! so far, fails at the end of simulation!!!
        #        if pred_from != sun_coeff_forecast.index[0] or pred_to != sun_coeff_forecast.index[-1]:
        #            raise

        x_pred = sun_coeff_forecast.values.reshape(
            -1, 1
        )  # TODO make a function "prepare_data_for_pred" or something?
        y_pred = self.model.predict(x_pred)
        y_pred = y_pred[:, 0]  # FIXME correct??? also used in train!
        forecasted_data = pd.Series(y_pred, index=sun_coeff_forecast.index)
        forecast = LoadForecast(
            begin=lf_request.pred_from, end=lf_request.pred_to, forecast=forecasted_data
        )
        return forecast

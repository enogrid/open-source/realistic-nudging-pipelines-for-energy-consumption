# import modules
import pandas as pd

from datetime import datetime, date


# class
class Predictor:
    """Computes the load predictions."""

    def __init__(self):
        # self.online_data = dict()#FIXME will maybe also be offline data, from which the model is trained
        self.energy_consumption = pd.Series(dtype=float)
        self.energy_production = pd.Series(dtype=float)
        self.online_data = None
        print("Predictor initialised.")

    @classmethod
    def make_predictor(cls, cfg: dict):
        pred = cls()
        return pred

    def predict(self, day: int, new_daily_household_data, new_daily_external_data):
        self.online_data.update({day: new_daily_household_data})
        return -1  # FIXME only to fix ideas!

    def set_historical_data(self, whole_consumption: pd.Series, production: pd.Series):
        self.energy_consumption = whole_consumption.copy(deep=True)
        self.energy_production = production.copy(deep=True)

    def get_daily_predict_data(self, day: date):
        # print(self.sunshine_coefficient)
        def is_right_day(
            date_and_time: datetime,
        ) -> bool:  # FIXME already defined in external world, maybe put in utils
            return date_and_time.date() == day

        daily_data = self.energy_profile[
            self.energy_profile.index.to_series().apply(is_right_day)
        ]
        return daily_data

    def get_consumption_predict(
        self, date: date, nb_days_ahead: int
    ):  # TODO should make sure that the prediction is still relevant, for the "perfect predictor", as consumption is changing through time, and the predictor only has historical data!
        def get_next_days(date_and_time: datetime) -> bool:
            return (
                date + pd.Timedelta(days=1)
                <= date_and_time.date()
                <= date + pd.Timedelta(days=nb_days_ahead)
            )

        return self.energy_consumption[
            self.energy_consumption.index.to_series().apply(get_next_days)
        ]

    def get_production_predict(self, date: date, nb_days_ahead: int):
        def get_next_days(date_and_time: datetime) -> bool:
            return (
                date + pd.Timedelta(days=1)
                <= date_and_time.date()
                <= date + pd.Timedelta(days=nb_days_ahead)
            )

        return self.energy_production[
            self.energy_production.index.to_series().apply(get_next_days)
        ]

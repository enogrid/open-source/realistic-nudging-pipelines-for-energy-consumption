import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestRegressor
from datetime import timedelta, datetime
from typing import List, Union
import warnings
import logging

from src.real_world_classes.data_collection.data_models import (
    HouseholdEnergyData,
    ExternalWorldData,
)
from src.real_world_classes.predictors.load_forecast import (
    LoadForecast,
    LoadForecastRequest,
)

from src.utils.util_functions import Vacances
from src.utils import exceptions

warnings.simplefilter(action="ignore", category=pd.errors.PerformanceWarning)


NB_HOURS_DAY = 24
NB_HALF_HOURS_IN_ONE_DAY = NB_HOURS_DAY * 2
NB_DAYS_IN_WEEK = 7
NB_MONTHS_IN_YEAR = 12
NB_DAYS_NON_LEAP_YEAR = 365
NB_DAYS_LEAP_YEAR = NB_DAYS_NON_LEAP_YEAR + 1

NB_DAYS_VALIDATION_SET = 28


class LoadRF:
    """Predicts future load, using random forests."""

    def __init__(
        self, config: dict, data_to_forecast: str
    ):  # FIXME the data_type parameter is a bit strange, or isn't it?
        # forecasting parameters
        self.prediction_periods_length: timedelta = pd.Timedelta(
            config["prediction_periods_lengths"]
        )  # FIXME unused so far, it seems

        self.data_to_forecast = data_to_forecast

        self.historical_data_as_dataframe: pd.DataFrame() = None
        self.daily_data_as_dataframe: pd.DataFrame() = None

        self.household_x_forecast_data: pd.Series() = None  # FIXME what is this attribute? it should be possible to remove it, see how to do it, if possible

        self.model = RandomForestRegressor(n_estimators=10, random_state=0, verbose=1)
        logging.debug("Load Predictor using Random Forests initialised.")

    @classmethod
    def make_predictor(cls, config: dict, data_to_forecast: str):
        if data_to_forecast not in ["production", "consumption", "surplus"]:
            raise exceptions.UnvalidForecastingDataType(data_to_forecast)
        pred = cls(config, data_to_forecast)
        return pred

    def train(self) -> dict:
        (
            load_data,
            weather_data,
        ) = (
            self.get_load_and_weather_data()
        )  # TODO make sure "load" applies to both "consumption" and "production", or change the name
        self.household_x_forecast_data = (
            load_data  # FIXME in turns, should probably be moved or something!
        )

        train_set = self.create_train_set(load_data, weather_data)

        x_train, y_train = train_set["x_train"].values, train_set["y_train"].values
        self.model.fit(x_train, y_train)

        training_outputs = self.compute_validation_outputs(
            train_set["x_val"], train_set["y_val"]
        )
        return training_outputs

    def power_forecast(self, lf_request: LoadForecastRequest) -> LoadForecast:
        """
        Predicts the load curve from pred_from to pred_to.

        Predictions are sampled according to the parameter self.prediction_periods_length.

        :param pred_from: beginning of requested prediction
        :param pred_to: end of requested prediction
        @return NOT CLEARLY DEFINED YET!
        """
        # FIXME how is the length of the prediction decided???
        all_weather_data = self.collect_all_weather_data(lf_request.weather_forecast)
        forecast_set = self.create_forecast_set(all_weather_data)

        x_forecast = forecast_set.values
        y_pred = self.model.predict(x_forecast)

        forecasted_data = pd.Series(y_pred, index=forecast_set.index)  # old, works
        forecast = LoadForecast(
            begin=lf_request.pred_from, end=lf_request.pred_to, forecast=forecasted_data
        )
        return forecast

    def set_historical_data(
        self,
        household_data: List[HouseholdEnergyData],
        external_world_data: List[ExternalWorldData],
    ) -> None:
        all_datetimes = [record.begin for record in household_data]

        index = pd.DatetimeIndex(all_datetimes)
        df_hist = pd.DataFrame(
            0, index=index, columns=["consumption", "sunshine_coefficient"]
        )
        df_hist.sort_index(ascending=True, inplace=True)

        for record in household_data:
            df_hist.loc[record.begin, "consumption"] = record.consumption
            df_hist.loc[record.begin, "production"] = record.production

        for record in external_world_data:
            df_hist.loc[
                record.begin, "sunshine_coefficient"
            ] = record.sunshine_coefficient
            df_hist.loc[record.begin, "temperature"] = record.temperature
            df_hist.loc[record.begin, "rayonnement"] = record.rayonnement

        self.historical_data_as_dataframe = df_hist

    def set_daily_data(
        self,
        household_data: List[HouseholdEnergyData],
        external_world_data: List[ExternalWorldData],
    ) -> None:
        all_datetimes = [record.begin for record in household_data]

        index = pd.DatetimeIndex(all_datetimes)
        df_daily = pd.DataFrame(
            0, index=index, columns=["consumption", "sunshine_coefficient"]
        )
        df_daily.sort_index(ascending=True, inplace=True)

        for record in household_data:
            df_daily.loc[record.begin, "consumption"] = record.consumption
            df_daily.loc[record.begin, "production"] = record.production

        for record in external_world_data:
            df_daily.loc[
                record.begin, "sunshine_coefficient"
            ] = record.sunshine_coefficient
            df_daily.loc[record.begin, "temperature"] = record.temperature
            df_daily.loc[record.begin, "rayonnement"] = record.rayonnement

        self.daily_data_as_dataframe = df_daily

    def collect_all_weather_data(
        self, weather_forecast_data: pd.DataFrame
    ) -> pd.DataFrame:
        weather_hist = self.historical_data_as_dataframe[
            ["temperature", "sunshine_coefficient"]
        ]
        weather_forecast_data = weather_forecast_data[["temperature", "sunshine_coeff"]]
        weather_forecast_data = weather_forecast_data.rename(
            columns={
                "sunshine_coeff": "sunshine_coefficient"  # FIXME fix things so that sunshine_coeff name is uniform throughout the code base!
            }
        )
        all_weather_data = pd.concat([weather_hist, weather_forecast_data])
        all_weather_data = all_weather_data[
            ~all_weather_data.index.duplicated(keep="first")
        ]
        return all_weather_data

    def get_load_and_weather_data(
        self,
    ) -> Union[
        pd.Series, pd.DataFrame
    ]:  # TODO make sure "load" applies to both "consumption" and "production", or change the function name
        # FIXME It seems the function is trained on the surplus process; I thought we should train it on consumption and prediction, and only when making the forecast, predicting the surplus as the difference between the two
        # breakpoint()
        if self.data_to_forecast in ["production", "consumption"]:
            load_data = self.historical_data_as_dataframe[self.data_to_forecast]
        elif self.data_to_forecast == "surplus":
            load_data = (
                self.historical_data_as_dataframe["production"]
                - self.historical_data_as_dataframe["consumption"]
            )

        weather_data = self.historical_data_as_dataframe[
            ["temperature", "sunshine_coefficient"]
        ]
        return load_data, weather_data

    def create_train_set(
        self, load_data: pd.Series, weather_data: pd.DataFrame
    ) -> dict:
        features = self.collect_training_features(load_data, weather_data)
        features.sort_index(inplace=True)
        # FIXME make sure these values are the right one, I'm not sure (visualise, there are "gaps"); see in other predictors (simple_production, or from_weekly_profile)
        val_end = features.index[-1]
        val_start = val_end - timedelta(days=NB_DAYS_VALIDATION_SET - 1)
        val_start = val_start.replace(hour=0, minute=0, second=0)
        train_end = val_start - timedelta(
            days=1
        )  # TODO maybe could improve, take the date just before new_val_start in self.features.index! here, some time slots are discarded, I think

        train_set = features.loc[:train_end, :]
        train_set = train_set.fillna(0)
        val_set = features.loc[val_start:val_end, :]
        val_set = val_set.fillna(0)

        column_to_predict = self.data_to_forecast
        input_columns = list(train_set.columns)
        input_columns.remove(column_to_predict)

        x_train = train_set.loc[:, input_columns]
        y_train = train_set.loc[:, column_to_predict]
        x_val = val_set.loc[:, input_columns]
        y_val = val_set.loc[:, column_to_predict]

        train_set = {
            "x_train": x_train,
            "y_train": y_train,
            "x_val": x_val,
            "y_val": y_val,
        }
        return train_set

    def collect_training_features(
        self, load_data: pd.Series, weather_data: pd.DataFrame
    ) -> pd.DataFrame:
        train_set = pd.DataFrame(index=range(0, len(weather_data.index), 1))
        train_set = add_weather_data(train_set, weather_data)
        train_set = add_load_training_data(train_set, self.data_to_forecast, load_data)

        # Interpolating the nan values
        for col in train_set.columns:
            if train_set[col].isnull().any():
                train_set = train_set.interpolate(method="nearest")

        # TODO why are these two needed?
        train_set = train_set.interpolate("linear")
        train_set = train_set.interpolate("backfill")
        return train_set

    def compute_validation_outputs(
        self, x_val_series: pd.Series, y_val_series: pd.Series
    ) -> dict:
        x_val = x_val_series.values
        y_val = y_val_series.values

        y_pred = self.model.predict(
            x_val
        )  # see if possible/good idea to use power forecast here

        y_val = y_val_series
        y_pred = pd.Series(y_pred, index=x_val_series.index)

        training_outputs = {
            "y_val": y_val,
            "y_pred": y_pred,
        }
        return training_outputs

    def add_lagg_load_data_for_forecast(
        self,
        forecast_set: pd.DataFrame,
        lagg_days: int,
        end: datetime,
        weather_no_lag: pd.DataFrame,
    ) -> (
        pd.DataFrame
    ):  # FIXME improve this function, or factorise it if possible with add_lagg_load_data
        self.household_x_forecast_data.sort_index(inplace=True)
        load_data_lag_last_date = end.replace(hour=23, minute=0, second=0) - timedelta(
            days=lagg_days
        )
        load_data_lag = self.household_x_forecast_data[
            :load_data_lag_last_date
        ]  # FIXME is it possible to remove the use of household_x_forecast_data? to use it before, for instance?
        load_data_lag = load_data_lag.iloc[
            -len(weather_no_lag.index) :
        ]  # FIXME avoid positional indexing

        column_name = f"{self.data_to_forecast}_{lagg_days}d_lag"
        forecast_set[column_name] = load_data_lag.values
        return forecast_set

    def add_load_data_for_forecast(
        self, forecast_set: pd.DataFrame, end: datetime, weather_no_lag: pd.DataFrame
    ) -> pd.DataFrame:  # FIXME see if possible to factorise with add_load_training_data
        all_lags_in_days = [14, 28]
        for nb_days_lag in all_lags_in_days:
            logging.debug(f"Adding {nb_days_lag} days lagged exterior load_data")
            forecast_set = self.add_lagg_load_data_for_forecast(
                forecast_set, nb_days_lag, end, weather_no_lag
            )
        return forecast_set

    def create_forecast_set(self, all_weather_data: pd.DataFrame) -> pd.DataFrame:
        start = self.household_x_forecast_data.index[-1]  # FIXME why is it so?
        end = all_weather_data.index[-1]

        weather_no_lag = all_weather_data[start:end]
        forecast_set = pd.DataFrame(index=range(0, len(weather_no_lag.index), 1))

        forecast_set = add_weather_data(forecast_set, weather_no_lag)
        forecast_set = self.add_load_data_for_forecast(
            forecast_set, end, weather_no_lag
        )

        # adding heuristic data for forecast set  #FIXME why is it removed?
        # self.add_heuristique_data_for_forecast_set(weather_no_lag)

        forecast_set = forecast_set.fillna(0)  # FIXME don't know why I have to add it
        forecast_set = add_types_of_days_information(weather_no_lag, forecast_set)
        forecast_set.index = weather_no_lag.index

        # Interpolating the nan values
        for col in forecast_set.columns:
            if forecast_set[col].isnull().any():
                forecast_set = forecast_set.interpolate(method="nearest")

        return forecast_set


# util functions
def add_one_hot_hours(data: pd.DataFrame) -> pd.DataFrame:
    logging.debug("Adding One Hot Encoded Hours columns")
    hours = list(range(0, NB_HOURS_DAY, 1))
    df_hod = pd.get_dummies(data.index.hour)
    for hr in hours:
        if not (hr in df_hod.columns.values.tolist()):
            df_hod[hr] = 0
    df_hod = df_hod.sort_index(axis=1)
    df_hod.columns = ["hour_" + str(col) for col in df_hod.columns]

    return df_hod


def add_one_hot_days(data):
    logging.debug("Adding One Hot Encoded Day of week columns")
    day_of_week = list(range(0, NB_DAYS_IN_WEEK, 1))
    df_dow = pd.get_dummies(data.index.dayofweek)
    for dow in day_of_week:
        if not (dow in df_dow.columns.values.tolist()):
            df_dow[dow] = 0
    df_dow = df_dow.sort_index(axis=1)
    df_dow.columns = ["dayofweek_" + str(col) for col in df_dow.columns]

    return df_dow


def add_one_hot_dayofyears(data):
    logging.debug("Adding One Hot Encoded day of year columns")
    years = list(set(data.index.year))
    df_doyg = pd.DataFrame()

    def is_leap_year(year: int):
        return (
            year % 4 == 0
        )  # FIXME I thought the criterion was more complex than that, make sure.

    for yr in years:
        df_year = data[data.index.year == yr]
        df_doy = pd.get_dummies(df_year.index.dayofyear)

        days_in_year = list(
            range(1, NB_DAYS_LEAP_YEAR + 1, 1)
        )  # leap year for all, to ensure same number of columns, for the pd.concat to be called later
        for doy in days_in_year:
            if not (doy in df_doy.columns.values.tolist()):
                df_doy[doy] = 0

        doy_cols = df_doy.columns.values.tolist()

        if is_leap_year(data.index.year[0]):
            doy_cols.remove(60)  # to remove the 29.02
            doy_cols.append(
                60
            )  # to ensure same number of columns, for the pd.concat to be called later
            df_doy.columns = doy_cols
            df_doy = df_doy.reindex(sorted(df_doy.columns), axis=1)

        df_doy = df_doy.sort_index(axis=1)

        df_doy.columns = ["dayofyear_" + str(col) for col in df_doy.columns]
        df_doyg = pd.concat([df_doyg, df_doy], axis=0, ignore_index=True)

        logging.debug(df_doyg.shape)

    return df_doyg


def add_one_hot_weekofyears(data):
    logging.debug("Adding One Hot Encoded week of year columns")
    week_of_year = list(range(1, 54, 1))  # FIXME why 54 and not 53 here?
    df_woy = pd.get_dummies(data.index.isocalendar().week.values)
    for woy in week_of_year:
        if not (woy in df_woy.columns.values.tolist()):
            df_woy[woy] = 0
    df_woy = df_woy.sort_index(axis=1)
    df_woy.columns = ["weekofyear_" + str(col) for col in df_woy.columns]

    return df_woy


def add_one_hot_monthofyears(data):
    logging.debug("Adding One Hot Encoded month of year columns")
    month_of_year = list(range(1, NB_MONTHS_IN_YEAR + 1, 1))
    df_moy = pd.get_dummies(data.index.month)
    for moy in month_of_year:
        if not (moy in df_moy.columns.values.tolist()):
            df_moy[moy] = 0
    df_moy = df_moy.sort_index(axis=1)
    df_moy.columns = ["month_" + str(col) for col in df_moy.columns]

    return df_moy


def add_types_of_days_information(data: pd.Series, train_forecast_data: pd.DataFrame):
    df_hod = add_one_hot_hours(data)
    df_dow = add_one_hot_days(data)
    df_doyg = add_one_hot_dayofyears(data)
    df_woy = add_one_hot_weekofyears(data)
    df_moy = add_one_hot_monthofyears(data)

    df_list = [train_forecast_data, df_hod, df_dow, df_doyg, df_woy, df_moy]

    for dataframe in df_list:
        if dataframe.empty:
            df_list.remove(dataframe)
    # print("concatenating dataframes")
    train_forecast_data = pd.concat(df_list, axis=1)
    train_forecast_data.index = data.index
    train_forecast_data.index.name = "index"
    index_title = train_forecast_data.index.name
    train_forecast_data[index_title] = train_forecast_data.index
    new_columns = ["is_weekend", "is_holiday"]

    logging.debug("Adding is_weekend")
    train_forecast_data["is_weekend"] = train_forecast_data[index_title].apply(
        lambda x: 0 if x.dayofweek < 5 else 1
    )
    # Adding is_holiday
    train_forecast_data["date"] = train_forecast_data.index.date

    calendar = (
        Vacances()
    )  # FIXME in turns, Vacances should be named BankHolidays, or Holidays
    holidays = calendar.holidays(
        start=train_forecast_data[index_title].min(),
        end=train_forecast_data[index_title].max(),
    )
    train_forecast_data["date"] = train_forecast_data.index.date
    train_forecast_data["is_holiday"] = train_forecast_data["date"].apply(
        lambda x: 1 if x in holidays else 0
    )
    # Dropping date column
    train_forecast_data = train_forecast_data.drop([index_title, "date"], axis=1)

    return train_forecast_data


def add_lagg_weather(
    train_set: pd.DataFrame, weather: pd.DataFrame, lagg_hour: int
) -> pd.DataFrame:
    weather_lagg = weather.shift(lagg_hour)

    cols_to_consider = ["temperature", "sunshine_coefficient"]
    for col in cols_to_consider:
        if lagg_hour > 0 and lagg_hour <= NB_HALF_HOURS_IN_ONE_DAY:
            col_in_set = f"{col}_{lagg_hour}hr_lag"
        elif lagg_hour > NB_HALF_HOURS_IN_ONE_DAY:
            quotient = lagg_hour / NB_HALF_HOURS_IN_ONE_DAY
            col_in_set = f"{col}_{quotient}d_lag"
        else:
            col_in_set = col

        train_set[col_in_set] = weather_lagg[col].values

    return train_set


def add_lagg_load_data(
    train_set: pd.DataFrame,
    data_to_forecast: str,  # FIXME should be in Enum or derived
    load_data: pd.DataFrame,
    nb_half_hours_lag: int,
) -> pd.DataFrame:
    load_data_lagg = load_data.shift(nb_half_hours_lag)

    columns = data_to_forecast

    if nb_half_hours_lag > 0 and nb_half_hours_lag <= NB_HALF_HOURS_IN_ONE_DAY:
        columns = f"{columns}_{nb_half_hours_lag}hr_lag"  # FIXME here, the "hr" should probably be changed in "hfhr", for "half-hour", but make sure it does not break anything later on
    elif nb_half_hours_lag > NB_HALF_HOURS_IN_ONE_DAY:
        columns = f"{columns}_{nb_half_hours_lag / NB_HALF_HOURS_IN_ONE_DAY}d_lag"

    train_set[columns] = load_data_lagg.values
    return train_set


def add_heuristic_data(
    load_data: pd.Series, train_set: pd.DataFrame, data_to_forecast: str
) -> pd.DataFrame:
    train_set = add_types_of_days_information(load_data, train_set)
    train_set[data_to_forecast] = load_data.values
    # Resetting index again
    train_set.index = load_data.index
    return train_set


def add_load_training_data(
    train_set: pd.DataFrame, data_to_forecast: str, load_data: pd.DataFrame
) -> pd.DataFrame:
    all_lags_in_days = [14, 28]
    for nb_days_lag in all_lags_in_days:
        logging.debug(f"Adding {nb_days_lag} days lagged exterior load_data")
        train_set = add_lagg_load_data(
            train_set,
            data_to_forecast,
            load_data,
            NB_HALF_HOURS_IN_ONE_DAY * nb_days_lag,
        )
    train_set = add_heuristic_data(load_data, train_set, data_to_forecast)
    return train_set


def add_weather_data(
    train_set: pd.DataFrame, weather_data: pd.DataFrame
) -> pd.DataFrame:
    lagg_list = [0, 2, 12, 24, 48, 48 * 7, 48 * 30]

    for lagg_hours in lagg_list:
        train_set = add_lagg_weather(train_set, weather_data, lagg_hours)

    nb_hours_avg = 6
    logging.debug(
        f"Adding average of last {nb_hours_avg} hours of exterior temperature"
    )
    weather_rolling_mean = (
        weather_data.rolling(window=nb_hours_avg, min_periods=1, closed="left")
        .mean()
        .shift(1)
    )

    cols_to_consider = ["temperature", "sunshine_coefficient"]
    for col in cols_to_consider:
        train_set[f"{col}_{nb_hours_avg}h_mean"] = weather_rolling_mean[col].values

    return train_set

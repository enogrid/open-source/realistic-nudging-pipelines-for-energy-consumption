# import modules
from typing import List
from src.real_world_classes.data_collection.data_models import (
    DisaggregationData,
    PredictionShiftableConso,
)


# class
class MarkovPredictor:
    """
    Predicts whether or not some shiftable (occasional) consumption occurs.

    In the case of a decomposition: consumption = periodic + occasional
    """

    def __init__(self):
        self.disaggregation_data = list()
        print("Markov Predictor initialised.")

    @classmethod
    def make_predictor(cls, cfg: dict):
        return cls()

    def add_disaggregated_data(self, data: List[DisaggregationData]):
        self.disaggregation_data.extend(data)

    def predict_shiftable_conso(self) -> List[PredictionShiftableConso]:
        return []

    def train(self, data: List[DisaggregationData]):
        pass

from pydantic import BaseModel, Field
from typing import Optional
import datetime


class HouseholdEnergyData(BaseModel):
    """All data related to energy in the broad sense (can also be power)."""

    # FIXME add units? power units, energy units?
    begin: datetime.datetime  # FIXME or use timestamp?
    length: datetime.timedelta
    consumption: float = Field(
        ge=0.0
    )  # ge: greater or equal; gt: greather than (strict) I think
    production: float = Field(ge=0.0)


class ExternalWorldData(BaseModel):
    begin: datetime.datetime
    length: datetime.timedelta
    sunshine_coefficient: float = Field(..., ge=0.0, le=1.0)
    temperature: float
    rayonnement: float


class SimulatorShiftableTrackingData(BaseModel):
    """
    Data not available in the real setting, but collected by the simulator.

    Allow to monitor what happened.
    """

    begin: datetime.datetime
    consumption: float = Field(ge=0.0)
    name_appliance: str


class SimulatorTrackingData(BaseModel):
    """
    Data not available in the real setting, but collected by the simulator.

    Allow to monitor what happened.
    """

    day: datetime.date
    changes_in_excess_prod: Optional[float]
    changes_in_deficit_prod: Optional[float]


class DisaggregationData(BaseModel):
    """Data coming from disaggregation, at least for periodic_occasional_decomposition."""

    begin: datetime.datetime
    end: datetime.datetime
    power: float = Field(ge=0)


class PredictionShiftableConso(
    BaseModel
):  # FIXME so far, identical to DisaggregationData, but may not remain so
    begin: datetime.datetime
    end: datetime.datetime
    power: float = Field(ge=0)

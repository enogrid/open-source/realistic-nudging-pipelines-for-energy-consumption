# import modules
import pandas as pd
from datetime import datetime, date, timedelta
import copy
from typing import List, Dict, Optional, Union
import logging

# custom modules
from src.real_world_classes.data_collection.data_models import (
    HouseholdEnergyData,
    SimulatorTrackingData,
    ExternalWorldData,
)


# class
class DataCenter:  # FIXME change the name in data adapter or data gateway, or something else!
    """
    The class represents the interface between the 'real or simulated world' (household, external world, with the weather in particular) and 'the Enogrid teams' which deploy weather_adviser and other algorithms.

    It is a bit like the data collection and obtention APIs developped by Enogrid.

    It insulates the simulated world (household, external world) from the 'true algortihms world' (weather_adviser, etc.), so that the latter work in a setting close to the 'real world setting'.
    """

    def __init__(self, simulator):
        """
        Initialises the data center.

        #FIXME docstring no longer up to date!
        :param date_range: series with the days of the simulation
        :param external_range: external world object, which can be queried to get data
        """
        self.simulator = simulator  # FIXME good idea to have it here?? represents all the "real world queries we can make" (weather forecasts, etc.)

        historical_data = simulator.get_historical_data()
        self.historical_household_data: List[HouseholdEnergyData] = historical_data[
            "household"
        ]
        self.historical_external_world_data: List[ExternalWorldData] = historical_data[
            "external_world"
        ]

        self.real_household_data: Dict[
            datetime.date, List[HouseholdEnergyData]
        ] = dict()  # stores only the household data, maybe evolve in the future
        self.real_external_world_data: Dict[
            datetime.date, List[ExternalWorldData]
        ] = dict()
        self.whole_energy_profile = pd.DataFrame()
        self.shiftable_energy_profile = pd.DataFrame()
        self.household_production = pd.DataFrame()
        self.tracking_data: Dict[datetime.date, List[SimulatorTrackingData]] = dict()

    def get_historical_data(
        self, data_type: str
    ) -> Union[List[HouseholdEnergyData], List[ExternalWorldData]]:
        if data_type == "household":
            return self.historical_household_data
        elif data_type == "external_world":
            return self.historical_external_world_data

    def collect_daily_household_data(self, day: date, daily_data: dict) -> None:
        self.real_household_data.update({day: daily_data["real"]["household"]})
        self.tracking_data.update({day: dict()})
        self.tracking_data[day] = daily_data["tracking"]

    def get_daily_household_data(
        self, day: date
    ) -> Optional[List[HouseholdEnergyData]]:
        return self.real_household_data.get(day)

    def get_daily_external_world_data(
        self, day: date
    ) -> Optional[List[ExternalWorldData]]:
        return self.real_external_world_data.get(day)

    def collect_daily_external_world_data(self, day: date, daily_data: dict) -> None:
        # does it have a link with the error, obtained with simulation length = 45?
        # KeyError: "None of [Index(['temperature', 'rayonnement'], dtype='object')] are in the [columns]"

        self.real_external_world_data.update({day: daily_data["sunshine_coefficient"]})

    def get_weather_forecast(self, day: date, data_type: str, day_ahead: int):
        if data_type == "sunshine_coeff":
            return self.simulator.external_world.predict_sunshine_coefficient(
                day, day_ahead
            )
        elif data_type == "temperature":  # FIXME in turns, modify that name!
            return self.simulator.external_world.predict_smoothed_weather(
                day, day_ahead
            )["Température réalisée lissée (°C)"]

    def reset(self):
        self.household_data = pd.DataFrame()
        self.external_world_data = pd.DataFrame()

    def get_simulation_outputs(self):
        sim_outputs = {
            "meta": {
                "date_range": -1  # FIXME self.date_range no longer stored right from the beginning! should still have the date range, but proceed differently!#self.date_range,
            },
            "data": {
                "real": {
                    "household": copy.copy(self.real_household_data),
                    "external_world": copy.copy(self.real_external_world_data),
                },
                "tracking": copy.copy(self.tracking_data),
                "historical": {
                    "household": copy.copy(
                        self.historical_household_data
                    ),  # historical data should be the same for every adviser, so as soon as there are several advisers, this becomes redundant
                    "external_world": copy.copy(self.historical_external_world_data),
                },
            },
        }
        return sim_outputs

    def collect_general_household_information(self, household):  #: hs.BaseHousehold):
        self.whole_energy_profile = household.appliances_handler.energy_profile.sum(
            axis="columns"
        )
        self.shiftable_energy_profile = (
            household.appliances_handler.shiftable_energy_profile.sum(axis="columns")
        )
        self.household_production = household.production_unit.production_profile

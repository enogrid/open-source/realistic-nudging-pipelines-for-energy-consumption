# import modules
from pydantic import BaseModel, Field
import datetime
from typing import Optional


# class
class RecommandationPeriod(
    BaseModel
):  # FIXME beware, if using this interface, errors are possible, when an adviser constructs a begin, and end, times which corresponded to "different days". To prevent this: use a begin and end "datetime". But now, this could mean the period runs over several days, see if this is an issue!
    day: datetime.date
    begin: datetime.time
    end: datetime.time
    strength: float = Field(ge=0.0, le=1.0)

    @property
    def begin_as_datetime(
        self,
    ) -> (
        datetime.datetime
    ):  # TODO this, or store begin and end as datetimes, and extract later date, and time?
        return datetime.datetime.combine(self.day, self.begin)

    @property
    def end_as_datetime(self) -> datetime.datetime:
        # FIXME hack, because if end=midnight, then end_as_datetime is day at midnight, and not day+1 at midnight, which is obviously wrong
        # FIXME in turn, improve the implementation, instead of using a hack
        end_to_return = datetime.datetime.combine(self.day, self.end)
        if self.end == datetime.time(hour=0, minute=0):
            end_to_return = end_to_return + datetime.timedelta(days=1)
        return end_to_return


class FlexibilityPotential:  # TODO modify according to discussions! #TODO formalise the contents of green, orange and red!
    """
    Represents changes consumers might wish to make in their energy consumption over some period of time.

    Whole implementation of class still in debate!
    """

    def __init__(
        self,
        begin_validity: Optional[datetime.datetime] = None,
        end_validity: Optional[datetime.datetime] = None,
    ):
        # when either is None, the potential is considered void (see method is_void)
        self.begin_validity = begin_validity
        self.end_validity = end_validity

        # TODO make enum type for colors!
        self.green_periods: List[RecommandationPeriod] = list()
        self.orange_periods: List[RecommandationPeriod] = list()
        self.red_periods: List[RecommandationPeriod] = list()

    @property
    def has_green(self):
        return len(self.green_periods) > 0

    @property
    def has_orange(self):
        return len(self.orange_periods) > 0

    @property
    def has_red(self):
        return len(self.red_periods) > 0

    def __repr__(self):
        """Gives a human readable representation of the flexibility potential."""
        fp_str = f"Green:\n{self.green_periods}\nOrange:\n{self.orange_periods}\nRed:\n{self.red_periods}"
        return fp_str

    def add_recommandation_period(self, color: str, reco: RecommandationPeriod):
        if color == "GREEN":
            self.green_periods.append(reco)
        elif color == "RED":
            self.red_periods.append(reco)

    def is_void(self):
        if (
            len(self.green_periods) == 0
            and len(self.orange_periods) == 0
            and len(self.red_periods) == 0
            or self.begin_validity is None
            or self.end_validity is None
        ):
            return True
        return False

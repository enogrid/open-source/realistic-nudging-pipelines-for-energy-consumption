# Real world algorithms

Contains real classes, which should be deployed in production in the future.

# Flexibility potential 
The Flexibility potential class defines the "information" sent by the algorithms, and which will be transformed in a nudge.

It contains recommandation periods, divided in "green", "orange" and "red" periods.

Each recommandation period consists of
- a day
- a time of beginning within that day;
- a time of end within that day;
- a strength (float between 0 and 1).



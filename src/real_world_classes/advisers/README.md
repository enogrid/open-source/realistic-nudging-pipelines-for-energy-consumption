# Advisers
These are the algorithms which gather consumption data from the data center, and output flexibility potentials.

# Set-up
The adviser is instantiated by the class method `make_adviser`, which takes as input a dictionary `cfg` containing the parameters needed. These parameters may vary between advisers.

# Workings
The main method is 'advise'. It:
1. takes as arguments the date, and the data center [data gateway?];
2. asks whatever data it needs from the data center;
3. maybe stores some of it for later use;
4. does whatever processing is needed;
5. outputs the Flexibility Potential.

# import modules
import datetime

# custom modules modules
from src.real_world_classes import flexibility_potential as fp
from src.real_world_classes.advisers import base_adviser as ad
from src.real_world_classes.data_collection import data_center as dc


class IdleAdvisor(ad.Adviser):
    """To represent the absence of advisor."""

    def __init__(self, cfg: dict):
        super().__init__()

        self.name = "No control"

        print("Reference (idle) adviser initialised")

    @classmethod
    def make_adviser(cls, cfg: dict) -> ad.Adviser:
        return cls(cfg)

    def advise(
        self, day: datetime.date, data_center: dc.DataCenter
    ) -> fp.FlexibilityPotential:
        pass

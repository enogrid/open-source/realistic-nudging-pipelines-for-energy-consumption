from datetime import date, time, datetime, timedelta
import pandas as pd
import copy
import logging
from typing import Optional, Dict

# custom modules
from src.real_world_classes.advisers import base_adviser as ad
from src.real_world_classes import flexibility_potential as fp
from src.real_world_classes.data_collection import data_center as dc
from src.real_world_classes.predictors import (
    LoadRF,
    LoadForecastRequest,
    SimpleProductionPredictor,
    WeeklyProfileConsumptionPredictor,
)
from src.utils import util_functions as uf

NB_SECONDS_DAY = 24 * 60 * 60

# 7  #FIXME in turns, should be in the config file

NB_DAYS_WEEK = 7


class CombinedController(ad.Adviser):
    def __init__(
        self,
        cfg: dict,
        consumption_predictor: Optional[
            LoadRF
        ] = None,  # FIXME change the types if needed!
        production_predictor: Optional[LoadRF] = None,
        excess_predictor: Optional[LoadRF] = None,
    ):
        super().__init__()
        self.name = "CombinedController"
        self.nb_periods_max = cfg["nb_periods_max"]
        self.nudging_periods_length: timedelta = pd.Timedelta(
            cfg["nudging_periods"]["length"]
        )  # must be pandas compatible
        self.length_nudging_validity: timedelta = timedelta(
            days=cfg["nudging_periods"]["prediction_days_ahead"]
        )  # nb of days ahead requested for weather forecasts

        self.excess_predictor = excess_predictor
        self.consumption_predictor = consumption_predictor
        self.production_predictor = production_predictor

        logging.debug("CombinedController adviser initialised")

        self.predicted_excess = pd.DataFrame()

    def get_all_predictors(
        self,
    ) -> Dict[
        str, LoadRF
    ]:  # do not make it a property, or would be passed by value, and this would be a bad choice, as attributes would no longer be updated, I think
        all_attributes_predictors = [
            self.excess_predictor,
            self.consumption_predictor,
            self.production_predictor,
        ]
        all_predictors = {
            predictor.data_to_forecast: predictor
            for predictor in all_attributes_predictors
            if predictor is not None
        }
        return all_predictors

    @classmethod
    def make_adviser(cls, cfg: dict) -> ad.Adviser:
        if cfg["predictor"]["type"] == "random_forest":
            #        #former
            excess_predictor = LoadRF.make_predictor(cfg["predictor"], "surplus")
            return cls(cfg, excess_predictor=excess_predictor)
        elif cfg["predictor"]["type"] == "simple":
            production_predictor = SimpleProductionPredictor.make_predictor(
                cfg["predictor"]
            )
            consumption_predictor = WeeklyProfileConsumptionPredictor.make_predictor(
                cfg["predictor"]
            )
            return cls(
                cfg,
                consumption_predictor=consumption_predictor,
                production_predictor=production_predictor,
            )

    def set_historical_data(self, data_center: dc.DataCenter):
        for predictor in self.get_all_predictors().values():
            predictor.set_historical_data(
                household_data=data_center.get_historical_data("household"),
                external_world_data=data_center.get_historical_data("external_world"),
            )

    def collect_data(self, day: date, data_center: dc.DataCenter) -> None:
        day_household_data = data_center.get_daily_household_data(day)
        day_external_world_data = data_center.get_daily_external_world_data(day)

        for predictor in self.get_all_predictors().values():
            predictor.set_daily_data(day_household_data, day_external_world_data)

    def train(self) -> dict:
        training_outputs = {}
        for data_forecasted, predictor in self.get_all_predictors().items():
            training_outputs_predictor = predictor.train()
            training_outputs.update(
                {data_forecasted: copy.deepcopy(training_outputs_predictor)}
            )
        return training_outputs

    def advise(self, day: date, data_center: dc.DataCenter) -> fp.FlexibilityPotential:
        """Computes, then ouputs the advise, as a FlexibilityPotential object."""
        # collecting daily data  #TODO data of the day, or of the day before? depends at which time the adviser acts!
        # FIXME data collection should be in another method, called for instance "collect_data", and happen at some frequency which may be different from the frequency at which the adviser issues advice!
        weather_data_needed = ["sunshine_coeff", "temperature"]
        nb_days = (
            self.length_nudging_validity.total_seconds() / NB_SECONDS_DAY
        )  # FIXME improve, hack because get_weather_forecast requires an int!
        weather_forecast = None
        for data_type in weather_data_needed:
            new_data = data_center.get_weather_forecast(day, data_type, nb_days)
            if (
                weather_forecast is None
            ):  # FIXME hack to avoid missing dates issue at the end of the simulation...
                weather_forecast = pd.DataFrame(index=new_data.index)
            weather_forecast[data_type] = new_data

        pred_from = datetime.combine(day, time(hour=0, minute=0))
        pred_from += timedelta(
            days=1
        )  # FIXME pred_from: from the day after, if advise delivered between 23:30 and 00:00!!

        pred_to = pred_from + self.length_nudging_validity
        pred_to = pred_to.replace(hour=23, minute=30)
        pred_to -= timedelta(minutes=30)
        lf_request = LoadForecastRequest(
            pred_from=pred_from, pred_to=pred_to, weather_forecast=weather_forecast
        )

        all_predictions = {}
        for data_forecasted, predictor in self.get_all_predictors().items():
            load_forecast = predictor.power_forecast(lf_request)
            y_pred = (
                load_forecast.forecast
            )  # FIXME so far, just wrapping up around the series y_pred originally sent back by power_forecast, in turns, should maybe transmit the forecast, and not directly load_forecast.forecast, to the rest of the code!
            all_predictions.update({data_forecasted: y_pred.copy(deep=True)})

        if "excess" in all_predictions.keys():
            y_pred = all_predictions["excess"]
        elif (
            "consumption" in all_predictions.keys()
            and "production" in all_predictions.keys()
        ):
            y_pred = all_predictions["production"] - all_predictions["consumption"]
        y_pred.name = "excess"
        y_pred.index.name = (
            "index"  # TODO improve, but so far needed in create_flex_pot_from_y_pred
        )

        self.predicted_excess = pd.concat([self.predicted_excess, y_pred])

        # FIXME are begin and end validity sound???
        begin_validity = uf.get_date_at_midnight(
            datetime(year=day.year, month=day.month, day=day.day) + timedelta(days=1)
        )  # FIXME day + 1: advise starts the day after if advise delivered between 23:30 and 00:00!!
        flex_pot = create_flex_pot_from_y_pred(
            nudging_periods_length=self.nudging_periods_length,
            excess_forecast=y_pred,
            nb_period_max=self.nb_periods_max,
            begin_validity=begin_validity,
            end_validity=uf.get_date_at_2330(
                begin_validity + self.length_nudging_validity - timedelta(days=1)
            ),  # FIXME -1: fix, handle length better!!
        )
        return flex_pot


def create_flex_pot_from_y_pred(
    nudging_periods_length: timedelta,
    excess_forecast: pd.Series,
    nb_period_max: int,
    begin_validity: datetime,
    end_validity: datetime,
) -> (
    fp.FlexibilityPotential
):  # TODO in, turns, improve this function! in particular, the resampling frequency should be a parameter!
    # FIXME when arriving here, the excess forecast runs over dates which can be "whatever", whereas we would like it to be between pred_from and pred_to or something! and correct for all predictors (simple and rf)
    excess_forecast = pd.DataFrame(excess_forecast)
    is_within_validity_range = excess_forecast.index.to_series().apply(
        lambda date_time: date_time >= begin_validity and date_time <= end_validity
    )
    by_nudging_periods_pred = excess_forecast[is_within_validity_range]
    by_nudging_periods_pred = by_nudging_periods_pred.resample(
        nudging_periods_length
    ).mean()
    by_nudging_periods_pred["green"] = by_nudging_periods_pred["excess"] > 0
    by_nudging_periods_pred["red"] = by_nudging_periods_pred["excess"] < 0

    # getting the (day, time begin, time end) intervals with green signals
    by_nudging_periods_pred.reset_index(inplace=True)

    by_nudging_periods_pred["day"] = by_nudging_periods_pred["index"].apply(
        lambda d: d.date()
    )
    by_nudging_periods_pred["time_bgn"] = by_nudging_periods_pred["index"].apply(
        lambda d: d.time()
    )
    by_nudging_periods_pred["time_end"] = by_nudging_periods_pred["index"].apply(
        lambda d: (d + nudging_periods_length).time()
    )  # FIXME make sure this is the frequency of the dataframe!!

    choices_ascending = {"red": True, "green": False}
    fl_pot = fp.FlexibilityPotential(begin_validity, end_validity)
    for color in ["red", "green"]:
        by_nudging_periods_pred.sort_values(
            by=["excess"],
            ascending=choices_ascending[color],
            inplace=True,
            ignore_index=True,  # gives a new index
        )
        periods_color = by_nudging_periods_pred[by_nudging_periods_pred[color]]
        periods_color = periods_color.head(nb_period_max)

        columns_to_keep = ["day", "time_bgn", "time_end", "excess"]
        periods_color = periods_color[columns_to_keep]

        breakpoint()
        for (
            index,
            row,
        ) in (
            periods_color.iterrows()
        ):  # FIXME only keep a certain number of periods! or else, make it so that the simulator only accepts a certain number of them!
            reco_period = fp.RecommandationPeriod(
                day=row["day"],
                begin=row[
                    "time_bgn"
                ],  # FIXME red periods in Random and Weather advisers are twice as large as that for excess watcher, as things stand!
                end=row["time_end"],
                strength=1.0
                - float(index)
                / len(
                    periods_color
                ),  # FIXME should use strength to indicate which periods are best! make sure done correctly for red and green, and that ordering of periods_color above works as intended!!!
            )
            fl_pot.add_recommandation_period(color=color.upper(), reco=reco_period)

    for num_day in range(NB_DAYS_WEEK):
        begin = begin_validity.date() + timedelta(days=num_day)
        begin = datetime.combine(begin, time(hour=0, minute=30))
        end = begin + timedelta(hours=23)  # FIXME make sure this is sound!
        reco_period = fp.RecommandationPeriod(
            day=begin.date(), begin=begin.time(), end=end.time(), strength=0.5
        )
        fl_pot.add_recommandation_period(color="RED", reco=reco_period)

    breakpoint()
    if (
        len(fl_pot.green_periods) > nb_period_max
    ):  # or len(fl_pot.red_periods) > NB_PERIODS_MAX:
        raise  # FIXME just to make sure, but should not happen

    return fl_pot

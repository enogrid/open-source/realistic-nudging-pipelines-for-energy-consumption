# import modules
from datetime import datetime, timedelta, date, time
import pandas as pd
import numpy as np
import logging

# custom modules
from src.real_world_classes.advisers import base_adviser as ad
from src.real_world_classes import flexibility_potential as fp
from src.real_world_classes.data_collection import data_center as dc
from src.utils import util_functions as uf

# 7  #7 #30  #2  #nb maximum of periods the WeatherController signals in one advice (too many would be too burdensome for the receiver)
NB_DAYS_WEEK = 7


# class
class WeatherController(ad.Adviser):
    def __init__(self, cfg: dict):
        super().__init__()

        self.name = "WeatherController"
        # self.weather_forecast_days_ahead = cfg['weather_forecast_days_ahead']  #nb of days ahead requested for weather forecasts
        self.nb_periods_max = cfg["nb_periods_max"]
        self.nudging_periods_length: timedelta = pd.Timedelta(
            cfg["nudging_periods"]["length"]
        )  # must be pandas compatible
        self.length_nudging_validity: timedelta = timedelta(
            days=cfg["nudging_periods"]["prediction_days_ahead"]
        )  # nb of days ahead requested for weather forecasts

        logging.debug("WeatherController adviser initialised")

    @classmethod
    def make_adviser(cls, cfg: dict) -> ad.Adviser:
        return cls(cfg)

    def advise(self, day: date, data_center: dc.DataCenter) -> fp.FlexibilityPotential:
        """Computes, then ouputs the advise, as a FlexibilityPotential object."""
        predicted_sunshine_coeff = data_center.get_weather_forecast(
            day,
            data_type="sunshine_coeff",
            day_ahead=self.length_nudging_validity.days
            # FIXME get_weather_forecast takes as argument an int, should probably improve!
        )
        # print(predicted_sunshine_coeff)
        # assert False

        begin_validity = uf.get_date_at_midnight(
            datetime(year=day.year, month=day.month, day=day.day) + timedelta(days=1)
        )
        end_validity = uf.get_date_at_2330(
            begin_validity + self.length_nudging_validity - timedelta(days=1)
        )
        flex_pot = create_flex_pot_from_sunshine(
            self.nudging_periods_length,
            predicted_sunshine_coeff,
            self.nb_periods_max,
            begin_validity=begin_validity,
            end_validity=end_validity,
        )
        return flex_pot


def create_flex_pot_from_sunshine(
    nudging_periods_length: timedelta,
    predicted_sunshine_coeff: pd.Series,
    nb_periods_max: int,
    begin_validity: datetime,
    end_validity: datetime,
) -> (
    fp.FlexibilityPotential
):  # TODO in, turns, improve this function! in particular, the resampling frequency should be a parameter!
    is_within_validity_range = predicted_sunshine_coeff.index.to_series().apply(
        lambda date_time: date_time >= begin_validity and date_time <= end_validity
    )
    by_quarter_day_pred = predicted_sunshine_coeff[is_within_validity_range]

    by_quarter_day_pred = by_quarter_day_pred.resample(nudging_periods_length).agg(
        np.mean
    )
    by_quarter_day_pred.name = "mean_prediction"
    by_quarter_day_pred = pd.DataFrame(by_quarter_day_pred)
    by_quarter_day_pred[
        "green"
    ] = True  # FIXME removing the threshold, make sure sound, with what follows, should take the "best" periods, not those above a threshold #by_quarter_day_pred['mean_prediction'] >= threshold_green

    # getting the frequency, which should be 6H
    index_freq = uf.get_index_frequency(by_quarter_day_pred.index)

    # getting the (day, time begin, time end) intervals with green signals
    by_quarter_day_pred.reset_index(inplace=True)
    by_quarter_day_pred["day"] = by_quarter_day_pred["date"].apply(lambda d: d.date())
    by_quarter_day_pred["time_bgn"] = by_quarter_day_pred["date"].apply(
        lambda d: d.time()
    )
    by_quarter_day_pred["time_end"] = by_quarter_day_pred["date"].apply(
        lambda d: (d + nudging_periods_length).time()
    )  # FIXME make sure this is the frequency of the dataframe!!
    by_quarter_day_pred.sort_values(
        by=["mean_prediction"], ascending=False, inplace=True
    )
    by_quarter_day_pred = by_quarter_day_pred[by_quarter_day_pred["green"]]
    by_quarter_day_pred = by_quarter_day_pred.head(nb_periods_max)
    columns_to_keep = ["day", "time_bgn", "time_end", "mean_prediction"]
    by_quarter_day_pred = by_quarter_day_pred[columns_to_keep]

    fl_pot = fp.FlexibilityPotential(begin_validity, end_validity)
    by_quarter_day_pred = by_quarter_day_pred.reset_index()
    by_quarter_day_pred.sort_values(
        by=["mean_prediction"], ascending=False, inplace=True
    )  # FIXME maybe not necessary to sort again here, but make sure!

    for (
        index,
        row,
    ) in (
        by_quarter_day_pred.iterrows()
    ):  # FIXME only keep a certain number of periods! or else, make it so that the simulator only accepts a certain number of them!
        reco_period = fp.RecommandationPeriod(
            day=row["day"],
            begin=row["time_bgn"],
            end=row["time_end"],
            strength=1 - float(index) / len(by_quarter_day_pred)
            # strength reflects the order, with periods with highest sunshine first
        )
        fl_pot.add_recommandation_period(color="GREEN", reco=reco_period)

    # adding red periods: evenings; "less dark" than nights, but more likely to be consumption
    for num_day in range(NB_DAYS_WEEK):
        begin = begin_validity.date() + timedelta(days=num_day)
        begin = datetime.combine(begin, time(hour=0, minute=30))
        end = begin + timedelta(hours=23)  # FIXME make sure this is sound!
        reco_period = fp.RecommandationPeriod(
            day=begin.date(), begin=begin.time(), end=end.time(), strength=1
        )
        fl_pot.add_recommandation_period(color="RED", reco=reco_period)

    if (
        len(fl_pot.green_periods) > nb_periods_max
    ):  # or len(fl_pot.red_periods) > NB_PERIODS_MAX:
        raise  # FIXME just to make sure, but should not happen

    return fl_pot

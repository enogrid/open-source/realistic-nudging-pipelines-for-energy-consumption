# import modules
import datetime

from src.real_world_classes import flexibility_potential as fp
from src.real_world_classes.data_collection import data_center as dc


# class
class Adviser:
    """Computes the FlexibilityPotential. So far, either WeatherController pipeline, or ml based one."""

    def __init__(self):
        # derived classes should have a name attribute
        pass

    @classmethod
    def make_adviser(cls, cfg: dict):
        return NotImplementedError()

    def set_historical_data(self, data_center: dc.DataCenter):
        """
        If needed by the advisor, collects historical data.

        May be overloaded by derived classes.
        """
        pass

    def collect_data(self, day: datetime.date, data_center: dc.DataCenter):
        """Daily collection of data."""
        pass

    def set_general_information(self, config: dict, data_center: dc.DataCenter):
        """
        If needed by the advisor, collects general information.

        May be overloaded by derived classes.
        """
        pass

    def train(self):
        """
        Trains the adviser, if needed.

        Would normally be called before the simulation/real world events unfolding starts.
        May be overloaded by derived classes.
        """
        pass

    def advise(
        self, day: datetime.date, data_center: dc.DataCenter
    ) -> fp.FlexibilityPotential:  # should be implemented in the derived classes
        return NotImplementedError()

# import modules
import re
import os
from typing import List, Dict, Union
import datetime
import pandas as pd
from pandas.tseries.offsets import Day
from pandas.tseries.holiday import (
    AbstractHolidayCalendar,
    Holiday,
    EasterMonday,
    Easter,
)
import logging

from src.real_world_classes.data_collection.data_models import (
    HouseholdEnergyData,
    ExternalWorldData,
    SimulatorShiftableTrackingData,
)


# functions
def combine_datetime_time(
    datetime_or_date: Union[datetime.datetime, datetime.date], hour: int, minute: int
) -> datetime.datetime:
    new_datetime = datetime.datetime.combine(
        datetime.datetime(
            year=datetime_or_date.year,
            month=datetime_or_date.month,
            day=datetime_or_date.day,
        ),
        datetime.time(hour=hour, minute=minute),
    )
    return new_datetime


def get_date_at_2330(datetime_or_date: Union[datetime.datetime, datetime.date]):
    date_at_midnight = combine_datetime_time(datetime_or_date, hour=23, minute=30)
    return date_at_midnight


def get_date_at_midnight(datetime_or_date: Union[datetime.datetime, datetime.date]):
    date_at_midnight = combine_datetime_time(datetime_or_date, hour=0, minute=0)
    return date_at_midnight


def add_shiftable_data_from_dict(
    df: pd.DataFrame, data: Dict[datetime.date, List[SimulatorShiftableTrackingData]]
) -> (
    pd.DataFrame
):  # FIXME no longer coherent if household energy data and external world data are not sampled the same
    all_names = set(
        record.name_appliance
        for daily_data in data.values()
        for record in daily_data["shiftable"]
    )
    # breakpoint()
    for name in all_names:
        df[name] = 0.0

    # breakpoint()

    for day, daily_data in data.items():
        for record in daily_data["shiftable"]:
            df.loc[record.begin, record.name_appliance] = record.consumption

    return df, all_names


def add_external_world_data_from_list(  # FIXME the functions get_datalist_as_dataframe, and add_external[...]_from_list, are a consequence historical, and "live" data are not formatted the same, should improve and refactor!
    df: pd.DataFrame, data: List[ExternalWorldData]
) -> (
    pd.DataFrame
):  # FIXME no longer coherent if household energy data and external world data are not sampled the same
    df["sunshine_coefficient"] = 0.0

    for record in data:
        df.loc[record.begin, "sunshine_coefficient"] = record.sunshine_coefficient

    return df


def add_external_world_data_from_dict(
    df: pd.DataFrame, data: Dict[datetime.date, List[ExternalWorldData]]
) -> (
    pd.DataFrame
):  # FIXME no longer coherent if household energy data and external world data are not sampled the same
    df["sunshine_coefficient"] = 0.0

    for day, daily_data in data.items():
        for record in daily_data:
            df.loc[record.begin, "sunshine_coefficient"] = record.sunshine_coefficient

    return df


def get_datalist_as_dataframe(data: List[HouseholdEnergyData]):
    # FIXME code copied and adapted from periodic_occasional_decomposition module, improve!
    all_datetimes = [record.begin for record in data]
    index = pd.DatetimeIndex(all_datetimes)
    df = pd.DataFrame(0, index=index, columns=["consumption"])
    df.sort_index(ascending=True, inplace=True)

    for record in data:
        df.loc[record.begin, "consumption"] = record.consumption
        df.loc[record.begin, "production"] = record.production

    return df


def get_datadict_as_dataframe(
    data: Dict[datetime.date, List[HouseholdEnergyData]]
):  # FIXME name a bit misleading, works not for all datadicts, but for the ones satisfying the requested type
    # FIXME code copied and adapted from periodic_occasional_decomposition module, improve!
    all_datetimes = [
        record.begin for daily_data in data.values() for record in daily_data
    ]
    index = pd.DatetimeIndex(all_datetimes)
    df = pd.DataFrame(0, index=index, columns=["consumption"])
    df.sort_index(ascending=True, inplace=True)

    for day, daily_data in data.items():
        for record in daily_data:
            df.loc[record.begin, "consumption"] = record.consumption
            df.loc[record.begin, "production"] = record.production

    logging.debug(f'total sim conso during processing: {df["consumption"].sum()}')
    return df


def get_index_frequency(index):
    regex_str = r"(?P<nb_hours>[0-9])H"  # also possible to obtain the groups by number, match.group(1), match.group(2), etc. (groups are between parentheses) # math.group(0) is the whole pattern
    pattern = re.compile(regex_str)
    match = pattern.match(index.freqstr)
    if match is not None:
        return int(match.group("nb_hours"))
    else:
        raise  # FIXME improve


def dataframe_with_no_tzinfo(dataframe, column_name):
    """
    MAKING DATAFRAMES WITH NO TIMEZONE INFO.

    :param dataframe:
    :param column_name:
    :return: dataframe:
    """
    # Sorting dataframe according to given column
    dataframe = dataframe.sort_values(by=column_name)

    # Converting column to DateTImeIndex
    dataframe[column_name] = pd.to_datetime(
        dataframe[column_name], errors="coerce", utc=True
    )

    # Making the tzinfo =None i.e. removing the timezone info

    dataframe[column_name] = dataframe[column_name].dt.tz_convert("Europe/Paris")
    dataframe[column_name] = dataframe[column_name].dt.tz_localize(None)

    # Setting the column as dataframe index
    dataframe = dataframe.set_index([column_name])

    return dataframe


class Vacances(AbstractHolidayCalendar):
    """Custom Holiday calendar for France based on https://en.wikipedia.org/wiki/Public_holidays_in_France."""

    # defining the days which should be declared as holidays
    rules = [
        Holiday("New Years Day", month=1, day=1),
        EasterMonday,
        Holiday("Labour Day", month=5, day=1),
        Holiday("Victory in Europe Day", month=5, day=8),
        Holiday("Ascension Day", month=1, day=1, offset=[Easter(), Day(39)]),
        Holiday("Bastille Day", month=7, day=14),
        Holiday("Assumption of Mary to Heaven", month=8, day=15),
        Holiday("All Saints Day", month=11, day=1),
        Holiday("Armistice Day", month=11, day=11),
        Holiday("Christmas Day", month=12, day=25),
    ]


def get_all_file_path_in_one_dir(dir_path):
    file_path_list = []
    for dirpath, dirnames, filenames in os.walk(dir_path):
        for file in filenames:
            if ".csv" in os.path.splitext(file)[1]:
                file_path_list.append(os.path.join(dirpath, file))
    return file_path_list

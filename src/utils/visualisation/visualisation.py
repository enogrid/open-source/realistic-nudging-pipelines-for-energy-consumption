# import modules
import matplotlib.pyplot as plt
import plotly.graph_objects as go
from plotly.offline import plot

from typing import List
import pandas as pd
import numpy as np
import logging


# global constants
NB_COL = 4

COLOUR_ADVISER = {
    "WeatherController": "orange",
    "No control": "blue",
    "CombinedController": "green",
    "CombinedControllerSimple": "yellow",
}


# functions


# utils
def get_line_col(num, nb_col):
    line = int(num / nb_col)
    col = num % nb_col

    # print(f'line {line}, col {col}')
    return line, col


# visualisation functions
def visualise_agg_self_prod_by_adviser(agg_self_prod_by_adviser: dict, output_rep: str):
    fix, ax = plt.subplots()

    colors_to_use = [
        COLOUR_ADVISER[adviser_type] for adviser_type in agg_self_prod_by_adviser.keys()
    ]
    ax.bar(
        agg_self_prod_by_adviser.keys(),
        agg_self_prod_by_adviser.values(),
        color=colors_to_use,
    )

    plt.legend()

    plt.title(f"Self production rate for different advisers")

    filename = output_rep + "by_adviser_self_production_rate" + ".png"
    plt.savefig(filename)
    plt.close()


def visualise_agg_self_conso_by_adviser(
    agg_self_conso_by_adviser: dict, output_rep: str
):
    fix, ax = plt.subplots()

    colors_to_use = [
        COLOUR_ADVISER[adviser_type]
        for adviser_type in agg_self_conso_by_adviser.keys()
    ]
    ax.bar(
        agg_self_conso_by_adviser.keys(),
        agg_self_conso_by_adviser.values(),
        color=colors_to_use,
    )

    plt.legend()

    plt.title(f"Self consumption rate for different advisers")

    filename = output_rep + "by_adviser_self_consumption_rate" + ".png"
    plt.savefig(filename)
    plt.close()


def visualise_discrepancy_production(
    adviser_type: str, excess_prod: pd.Series, deficit_prod: pd.Series, output_rep: str
):
    plt.rcParams["figure.figsize"] = (15, 10)
    fix, ax = plt.subplots()
    ax.plot(excess_prod.index, excess_prod.values, label="Excess production")
    ax.plot(deficit_prod.index, deficit_prod.values, label="Deficit production")

    plt.setp(ax.get_xticklabels(), rotation=30, ha="right")
    ax.set_xlabel("Time")
    ax.set_ylabel("Power (kW) CORRECT??")
    plt.legend()

    plt.title(
        f"Discrepancy between production and consumption with {adviser_type} as adviser"
    )

    filename = output_rep + "discrepancy_prod_" + adviser_type + ".png"
    plt.savefig(filename)
    plt.close()


def visualise_agg_self_production_rate(
    adviser_type: str, agg_self_production_rate: float, output_rep: str
):
    plt.rcParams["figure.figsize"] = (15, 10)
    fix, ax = plt.subplots()

    if not (0 <= agg_self_production_rate and agg_self_production_rate <= 1):
        raise ValueError()

    data = {
        "agg_self_production_rate": agg_self_production_rate,
        "diff": 1 - agg_self_production_rate,
    }

    ax.pie(data.values(), labels=data.keys(), autopct="%1.1f%%", startangle=90)

    plt.title(f"Aggregate self production rate with {adviser_type} as adviser")

    filename = output_rep + "aggregate_self_production_rate_" + adviser_type + ".png"
    plt.savefig(filename)
    plt.close()


def visualise_agg_self_consumption_rate(
    adviser_type: str, agg_self_consumption_rate: float, output_rep: str
):
    fix, ax = plt.subplots()

    if not (0 <= agg_self_consumption_rate and agg_self_consumption_rate <= 1):
        raise ValueError()

    data = {
        "agg_self_consumption_rate": agg_self_consumption_rate,
        "diff": 1 - agg_self_consumption_rate,
    }

    ax.pie(data.values(), labels=data.keys(), autopct="%1.1f%%", startangle=90)

    plt.title(f"Aggregate self consumption rate with {adviser_type} as adviser")

    filename = output_rep + "aggregate_self_consumption_rate_" + adviser_type + ".png"
    plt.savefig(filename)
    plt.close()


def visualise_changes_in_disc_prod_by_advisors(
    type_disc: str, by_advisor_changes_in_disc_prod: dict, output_rep: str
):
    fix, ax = plt.subplots()

    # FIXME later, need to visualise the efficiency by advise, not by day!
    for adviser_type, data in by_advisor_changes_in_disc_prod.items():
        changes_in_disc_prod = data[f"changes_in_{type_disc}_prod"]
        # disc_prod = data[f'{type_disc}_production']
        print(f"{type_disc}, {adviser_type}\n{changes_in_disc_prod.value_counts()}")
        ax.boxplot(changes_in_disc_prod.dropna().values)

    # plt.setp(ax.get_xticklabels(), rotation=30, ha="right")
    ax.set_xticklabels(list(by_advisor_changes_in_disc_prod.keys()))
    ax.set_xlabel("Adviser")
    ax.set_ylabel("Energy (kWh) CORRECT??")

    plt.legend()

    plt.title(f"Changes in {type_disc} production for different advisers")

    filename = output_rep + f"by_adviser_changes_in_{type_disc}_prod" + ".png"
    plt.savefig(filename)
    plt.close()


def visualise_changes_in_temporal_disc_prod_by_advisors(
    type_disc: str, by_advisor_changes_in_disc_prod: dict, output_rep: str
):
    fix, ax = plt.subplots()

    for adviser_type, data in by_advisor_changes_in_disc_prod.items():
        changes_in_disc_prod = data[f"changes_in_{type_disc}_prod"]
        disc_prod = data[f"{type_disc}_production"]
        print(f"{type_disc}, {adviser_type}\n{changes_in_disc_prod.value_counts()}")
        ax.plot(
            changes_in_disc_prod.index,
            changes_in_disc_prod.values,
            label=adviser_type,
            color=COLOUR_ADVISER[adviser_type],
        )
        ax.plot(
            disc_prod.index,
            disc_prod.values,
            linestyle="--",
            label=adviser_type + f"_{type_disc}_prod",
            color=COLOUR_ADVISER[adviser_type],
        )

    plt.setp(ax.get_xticklabels(), rotation=30, ha="right")
    ax.set_xlabel("Time")
    ax.set_ylabel("Energy (kWh) CORRECT??")

    plt.legend()

    plt.title(f"Changes in {type_disc} production through time for different advisers")

    filename = (
        output_rep + f"by_adviser_changes_in_{type_disc}_prod_through_time" + ".png"
    )
    plt.savefig(filename)
    plt.close()


def visualise_self_consumption_between_advisors(
    by_advisor_self_consumption_rates: dict, output_rep: str
):
    fix, ax = plt.subplots()

    for (
        adviser_type,
        self_consumption_rate,
    ) in by_advisor_self_consumption_rates.items():
        values_without_nan = self_consumption_rate.fillna(0).values
        ax.plot(
            self_consumption_rate.index,
            np.cumsum(values_without_nan),
            label=adviser_type,
        )

    plt.setp(ax.get_xticklabels(), rotation=30, ha="right")
    ax.set_xlabel("Time")
    ax.set_ylabel("Energy (kWh) CORRECT??")

    plt.legend()

    plt.title(f"Self consumption rate for different advisers")

    filename = output_rep + "by_adviser_self_consumption_rate" + ".png"
    plt.savefig(filename)
    plt.close()


def old_visualise_discrepancy_production(
    type_disc_prod: str,
    adviser_type: str,
    production: pd.Series,
    whole_consumption: pd.Series,
    output_rep: str,
):
    fix, ax = plt.subplots()

    disc = production - whole_consumption
    if type_disc_prod == "excess":
        disc = disc[disc >= 0]
    elif type_disc_prod == "deficit":
        disc = disc[disc < 0]
    ax.plot(disc.index, disc.values, label="{type_disc_prod} production")

    plt.setp(ax.get_xticklabels(), rotation=30, ha="right")
    ax.set_xlabel("Time")
    ax.set_ylabel("Energy (kWh) CORRECT??")

    plt.title(f"{type_disc_prod} production ({adviser_type} as adviser)")

    filename = output_rep + type_disc_prod + "_production_" + adviser_type + ".png"
    plt.savefig(filename)
    plt.close()


def visualise_historical_prod_and_coeff(
    production: pd.Series, sunshine_coeff: pd.Series, output_rep: str
):
    fix, ax = plt.subplots()
    ax2 = ax.twinx()

    ax.plot(production.index, production.values, label="Production")
    ax2.plot(
        sunshine_coeff.index,
        sunshine_coeff.values,
        label="Sunshine coefficient",
        color="orange",
    )  # FIXME beware, since for now, the two are proportional, depending on the scale, they may be indistinguishable!

    plt.setp(ax.get_xticklabels(), rotation=30, ha="right")
    ax.set_xlabel("Time")
    ax.set_ylabel("Power (kW)")

    ax2.set_ylabel("Sunshine coefficient")
    ax2.set_ylim(None, 1)

    plt.title(f"Production historical")
    plt.legend()

    filename = output_rep + "production_historical" + ".png"
    plt.savefig(filename)
    plt.close()


def visualise_daily_self_production_rate(  # FIXME not daily, but by 30min, and same for self_consumption_rate
    adviser_type: str, self_production_rate: pd.Series, output_rep: str
):
    fix, ax = plt.subplots()

    ax.plot(
        self_production_rate.index,
        self_production_rate.values,
        label="Self production rate",
    )

    plt.setp(ax.get_xticklabels(), rotation=30, ha="right")
    ax.set_xlabel("Time")
    ax.set_ylabel("Energy (kWh) CORRECT??")

    plt.title(f"Self production rate ({adviser_type} as adviser)")

    filename = output_rep + "self_production_rate_" + adviser_type + ".png"
    plt.savefig(filename)
    plt.close()


def visualise_daily_self_consumption_rate(  # FIXME not daily, but by 30min, and same for self_production_rate
    adviser_type: str, self_consumption_rate: pd.Series, output_rep: str
):
    fix, ax = plt.subplots()

    ax.plot(
        self_consumption_rate.index,
        self_consumption_rate.values,
        label="Self consumption rate",
    )

    plt.setp(ax.get_xticklabels(), rotation=30, ha="right")
    ax.set_xlabel("Time")
    ax.set_ylabel("Energy (kWh) CORRECT??")

    plt.title(f"Self consumption rate with {adviser_type} as adviser")

    filename = output_rep + "self_consumption_rate_" + adviser_type + ".png"
    plt.savefig(filename)
    plt.close()


def visualise_shiftable_consumption_by_adviser(  # mainly copied from visualise_consumption_by_appliance
    shiftable_conso_by_adviser: dict, output_rep: str
):
    all_appliances = list(shiftable_conso_by_adviser.keys())
    nb_appliances = len(all_appliances)

    nb_col = 3
    nb_lines = int(nb_appliances / 2)

    fix, axs = plt.subplots(nb_lines, nb_col, figsize=(64, 32))  # (16, 32))

    for num_app, (name_appliance, consos_appliance) in enumerate(
        shiftable_conso_by_adviser.items()
    ):
        line, col = get_line_col(num_app, nb_col)
        for adviser_type, conso_adviser in consos_appliance.items():
            axs[line, col].plot(
                conso_adviser.index, conso_adviser.values, label=f"{adviser_type}"
            )

        axs[line, col].set_title(f"{name_appliance}")
        axs[line, col].legend()

        axs[line, col].set_xlabel("Time")
        plt.setp(axs[line, col].get_xticklabels(), rotation=30, ha="right")
        axs[line, col].set_ylabel("Power (kW)")

    plt.suptitle(f"Power consumption by appliance between advisers")
    plt.legend()

    filename = output_rep + "shiftable_consumption_between_advisers" + ".png"
    plt.savefig(filename)
    plt.close()


def visualise_consumption_by_appliance(
    adviser_type: str, energy_consumption: pd.DataFrame, output_rep: str
):
    all_appliances = list(energy_consumption.columns)
    print(all_appliances)
    columns_to_remove: List[
        str
    ] = (
        []
    )  # ['mains', 'autoconso_rate', 'autoprod_rate', 'production']  #FIXME is mains really to remove?? to implement it better: remove the columns which are not in the list of appliances, which is in household.all_appliances
    for column_name in columns_to_remove:
        all_appliances.remove(column_name)
    nb_appliances = len(all_appliances)

    nb_lines = int(nb_appliances / NB_COL) + 1

    fix, axs = plt.subplots(nb_lines, NB_COL, figsize=(64, 32))  # (16, 32))

    for num_app, appliance in enumerate(all_appliances):
        line, col = get_line_col(num_app, NB_COL)
        appliance_consumption = energy_consumption.loc[:, appliance]

        axs[line, col].plot(
            energy_consumption.index, appliance_consumption, label=f"{appliance}"
        )
        axs[line, col].set_title(f"{appliance}")
        axs[line, col].legend()

        axs[line, col].set_xlabel("Time")
        plt.setp(axs[line, col].get_xticklabels(), rotation=30, ha="right")
        axs[line, col].set_ylabel("Energy (kWh) CORRECT??")

    plt.suptitle(f"Energy consumption by appliance with {adviser_type} as adviser")
    plt.legend()

    filename = output_rep + "load_curves_by_appliance_" + adviser_type + ".png"
    plt.savefig(filename)
    plt.close()


def visualise_shiftable_vs_production(
    adviser_type: str,
    power_production: pd.Series,
    power_consumption: pd.Series,
    total_shiftable: pd.Series,
    total_not_shiftable: pd.Series,
    output_rep: str,
    predicted_excess: pd.DataFrame = None,  # FIXME for excess watcher, not ideal to have it after output_rep
):
    #    fix, ax = plt.subplots()
    #
    #    ax.plot(energy_production.index, energy_production.values, label='Production')
    #
    #    plt.setp(ax.get_xticklabels(), rotation=30, ha="right")
    #    ax.set_xlabel('Time')
    #    ax.set_ylabel('Power (kW)')
    #
    #    plt.title(f'Power production.')
    #    plt.savefig(filename)
    #    plt.close()

    fig = go.Figure()
    fig.add_trace(
        go.Scatter(
            x=total_shiftable.index,
            y=total_shiftable.values + total_not_shiftable.values,
            name="total shiftable manually on top of not shift",
            # mode='markers',
            # marker_color=df_ply['color_acc'],  #color_acc or color_pdl
            # text=df_ply['hover_text']  #FIXME only visible when using fig.show()
        )
    )
    fig.add_trace(
        go.Scatter(
            x=total_not_shiftable.index,
            y=total_not_shiftable.values,
            name="total not shiftable",
            # mode='markers',
            # marker_color=df_ply['color_acc'],  #color_acc or color_pdl
            # text=df_ply['hover_text']  #FIXME only visible when using fig.show()
        )
    )
    fig.add_trace(
        go.Scatter(
            x=power_production.index, y=power_production.values, name="production"
        )
    )

    fig.add_trace(
        go.Scatter(
            x=power_production.index,
            y=power_production.values - power_consumption.values,
            name="true excess",
        )
    )

    if predicted_excess is not None:
        predicted_excess = predicted_excess[
            0
        ]  # TODO improve, due to predicted excess being a DataFrame, whose sole column has the default name "0"
        breakpoint()
        fig.add_trace(
            go.Scatter(
                x=predicted_excess.index,  # FIXME when using predicted_excess.index, overlapping values or something, maybe only when using the random forest predictor, understand the issue!!
                y=predicted_excess.values,
                name="predicted excess",
            )
        )

    fig.update_layout(
        title_text=f"Shiftable consumption vs production for {adviser_type}"
    )
    fig.update_xaxes(title_text="Time")
    fig.update_yaxes(title_text="Power (kW MAKE SURE)")
    # fig.show()
    logging.debug(f"activate fig.show to get interactive plots")

    filename = output_rep + f"shiftable_vs_production_{adviser_type}" + ".png"
    fig.write_image(
        filename
    )  # FIXME requires kaleido, and cannot install it from poetry (use poetry add kaleido=<num_version>, and this works)


def visualise_production(
    adviser_type: str, energy_production: pd.Series, output_rep: str
):
    fix, ax = plt.subplots()

    ax.plot(energy_production.index, energy_production.values, label="Production")

    plt.setp(ax.get_xticklabels(), rotation=30, ha="right")
    ax.set_xlabel("Time")
    ax.set_ylabel("Power (kW)")

    plt.title(f"Power production.")

    filename = output_rep + "production" + ".png"
    plt.savefig(filename)
    plt.close()


def visualise_aggregate_consumption(
    adviser_type: str, energy_consumption: pd.Series, output_rep: str
):
    fix, ax = plt.subplots()

    ax.plot(
        energy_consumption.index,
        energy_consumption.values,
        label="Aggregate consumption",
    )

    plt.setp(ax.get_xticklabels(), rotation=30, ha="right")
    ax.set_xlabel("Time")
    ax.set_ylabel("Energy (kWh) CORRECT??")

    plt.title(f"Aggregate energy consumption with {adviser_type} as adviser")

    filename = output_rep + "load_curve_" + adviser_type + ".png"
    plt.savefig(filename)
    plt.close()


def rf_predictor_visualisation_for_validation(
    forecaster_type: str, df_ture_pred: pd.DataFrame, output_rep: str
):
    ax = df_ture_pred.plot(figsize=(16, 9), lw=5)

    # font = 36  #the visual was not great with these settings, but maybe it's only on my laptop

    # ax.tick_params(axis='both', which='minor', labelsize=font)
    plt.xticks()  # (fontsize=font)
    plt.setp(ax.get_xticklabels(), rotation=30, ha="right")
    plt.xlabel("Time (hr)")  # , fontsize=font)

    plt.yticks()  # fontsize=font)
    plt.ylabel("Power_predict (Watt)")  # , fontsize=28)

    plt.legend()  # fontsize=font)
    title = "Power prediction"
    plt.title(title)  # , fontsize=16)
    plt.legend(["Actual Curve", "RF Regressed Prediction"])  # , fontsize=font)

    filename = output_rep + f"rf_{forecaster_type}_model" + ".png"
    plt.savefig(filename)


def visualise_training_outputs(training_outputs: dict, output_rep: str) -> None:
    adviser_name = "CombinedController"
    training_outputs = training_outputs.get(adviser_name)

    if training_outputs is not None:
        logging.debug(f"Visualising training output for {adviser_name}")

        for forecaster_type in training_outputs.keys():
            logging.debug(f"Forecaster type: {forecaster_type}")
            rf_predictor_visualisation_for_validation(
                forecaster_type,
                training_outputs[forecaster_type]["prediction"],
                output_rep,
            )
            logging.debug("metrics of validation result")
            metrics = training_outputs[forecaster_type]["validation_metrics"]
            logging.debug(f"metrics = {metrics}")
            if metrics["R2Score"] < 0.7:
                logging.warning(
                    "R2Score is a little bit small, the model may be not a good model"
                )
            if metrics["MAPE"] > 0.35:
                logging.warning(
                    "Mean absolute pourcentage error is bigger than 35%,"
                    " the model may be not a good one "
                )


def visualise_outputs(processed_data: dict, output_rep: str):
    # assert False, f'should visualise shiftable appliances load curves wrt. no control!'

    # visualise historical data
    hist_data = processed_data["historical"]
    visualise_historical_prod_and_coeff(
        hist_data["production"], hist_data["sunshine_coefficient"], output_rep
    )
    visualise_aggregate_consumption("historical", hist_data["consumption"], output_rep)

    # visualise by adviser data
    for adviser_type, data_adviser in processed_data["by_adviser"].items():
        # FIXME SHOULD plot, for each adviser, production, non-shiftable consumption, and on top of the latter total shiftable consumption, in a plotly graph!!
        # visualise_shiftable_vs_production(adviser_type, data_adviser['production'], data_adviser['consumption'], data_adviser['total_shiftable'], data_adviser['total_not_shiftable'], output_rep, data_adviser.get('predicted_excess'))  #FIXME use plotly!!!

        visualise_production(
            adviser_type, data_adviser["production"], output_rep
        )  # FIXME should be the same for every adviser, no need to plot it again an again for each adviser, improve!
        visualise_aggregate_consumption(
            adviser_type, data_adviser["consumption"], output_rep
        )
        visualise_daily_self_consumption_rate(
            adviser_type, data_adviser["by_30min_self_consumption_rate"], output_rep
        )
        visualise_daily_self_production_rate(
            adviser_type, data_adviser["by_30min_self_production_rate"], output_rep
        )

        visualise_agg_self_consumption_rate(
            adviser_type, data_adviser["agg_self_consumption_rate"], output_rep
        )
        visualise_agg_self_production_rate(
            adviser_type, data_adviser["agg_self_production_rate"], output_rep
        )

        visualise_discrepancy_production(
            adviser_type,
            data_adviser["by_30min_excess_prod"],
            data_adviser["by_30min_deficit_prod"],
            output_rep,
        )
    logging.debug(
        f"should also visualise by appliance consumption, at least for the shiftable appliances!"
    )

    # comparing advisors
    agg_self_conso_by_adviser = {
        adviser_type: data_adviser["agg_self_consumption_rate"]
        for adviser_type, data_adviser in processed_data["by_adviser"].items()
    }
    visualise_agg_self_conso_by_adviser(agg_self_conso_by_adviser, output_rep)

    agg_self_prod_by_adviser = {
        adviser_type: data_adviser["agg_self_production_rate"]
        for adviser_type, data_adviser in processed_data["by_adviser"].items()
    }
    visualise_agg_self_prod_by_adviser(agg_self_prod_by_adviser, output_rep)

    first_adviser_data = processed_data["by_adviser"][
        list(processed_data["by_adviser"].keys())[0]
    ]
    all_shiftable_appliances_names = list(first_adviser_data["shiftable"].keys())
    shiftable_conso_by_adviser = {
        name: {
            adviser_type: data_adviser["shiftable"][name]
            for adviser_type, data_adviser in processed_data["by_adviser"].items()
        }
        for name in all_shiftable_appliances_names
    }
    visualise_shiftable_consumption_by_adviser(shiftable_conso_by_adviser, output_rep)


def collective_simulation_result_plot(
    collection_infos_df: pd.DataFrame, output_rep: str
):
    collection_infos_df = collection_infos_df.transpose() * 100

    self_consumption_col = [
        col for col in collection_infos_df.columns if "self_consumption" in col
    ]
    self_production_col = [
        col for col in collection_infos_df.columns if "self_production" in col
    ]

    self_consumption_result = collection_infos_df[self_consumption_col]
    self_production_result = collection_infos_df[self_production_col]

    for col in self_consumption_result.columns:
        self_consumption_result = self_consumption_result.rename(
            columns={col: col[:-26]}
        )

    for col in self_production_result.columns:
        self_production_result = self_production_result.rename(columns={col: col[:-25]})

    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(15, 10))

    self_consumption_result.boxplot(ax=axes[0])
    axes[0].set_title("Self-consumption rates with various controllers")
    axes[0].set_ylabel("self-consumption rate (%)")
    axes[0].set_xlabel(f"Mean values  : {self_consumption_result.mean()}")

    self_production_result.boxplot(ax=axes[1])
    axes[1].set_title("Self-production rates with various controllers")
    axes[1].set_ylabel("self-production rate (%)")
    axes[1].set_xlabel(f"Mean values : {self_production_result.mean()}")
    plt.show()

    filename = output_rep + "collective_simulation_result_" + ".png"
    fig.savefig(filename)

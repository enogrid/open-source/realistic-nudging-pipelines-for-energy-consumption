from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.consumption_units import (
    VariableConsumptionUnit,
    FixedConsumptionUnit,
    ShiftableConsumptionUnit,
)

from omegalpes.energy.units.production_units import (
    FixedProductionUnit,
    VariableProductionUnit,
)
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.time import TimeUnit
import pandas as pd
import logging


def create_omegalpe_model(
    production: pd.Series,
    shift: dict,
    non_shift: pd.Series,
    nb_sample_day: int,
    dt: float,
):
    production = production.values.tolist()
    shiftable_consommation = ()
    non_shift = non_shift.values.tolist()
    time = TimeUnit(periods=nb_sample_day, dt=dt)
    model = OptimisationModel(time=time, name="opt_adviser")
    non_shiftable_consommation = FixedConsumptionUnit(
        time, "non_shiftable_consommation", energy_type="Electrical", p=non_shift
    )

    for col in shift.columns:
        shift_profile = shift[col].values.tolist()
        if sum(shift_profile) == 0:
            shift_profile[-1] = 0.1
        shiftable_consommation = shiftable_consommation + (
            ShiftableConsumptionUnit(
                time,
                name=f"shift_app_{col}",
                power_values=shift_profile,
                energy_type="Electrical",
            ),
        )

    elec_grid_imports = VariableProductionUnit(
        time, "elec_grid_imports", energy_type="Electrical"
    )

    # Create the electrical grid exports as a consumption
    elec_grid_exports = VariableConsumptionUnit(
        time, "elec_grid_exports", energy_type="Electrical"
    )

    prod = FixedProductionUnit(
        time, name="production", energy_type="Electrical", p=production
    )

    elec_grid_exports.minimize_consumption()

    # elec_grid_imports.minimize_production()

    elec_node = EnergyNode(time, "elec_node", energy_type="Electrical")

    elec_node.connect_units(
        *shiftable_consommation,
        non_shiftable_consommation,
        elec_grid_imports,
        elec_grid_exports,
        prod,
    )

    model.add_nodes(elec_node)
    model.solve_and_update()

    return elec_node, model  # , consumpt_orig


def get_optim_result(elec_node):
    flows = elec_node.get_flows
    profile_table = pd.DataFrame()
    for flow in flows:
        label = flow.parent.name
        parent = flow.parent
        # Get the power profiles
        if isinstance(flow.value, list):
            energy_flow = flow.value
        elif isinstance(flow.value, dict):
            energy_flow = list(flow.value.values())
        energy_flow_data = pd.DataFrame(energy_flow, columns=[label])

        profile_table = pd.concat([profile_table, energy_flow_data], axis=1)

    return profile_table

"""
Goal (so far) of the analysis: train a model which predicts excesses, and deficits, in production.

When inputted
1. a time period of length 3 hours (say)
2. a sunshine coefficient
outputs (predicts)
1. an excess production
2. a deficit production
(if 1. is not zero, then 2. is zero, and reciprocally)
"""


# from typing import Dict, List
from pydantic import BaseModel
import datetime
import pandas as pd

# from src.real_world_classes.data_collection.data_models import HouseholdEnergyData
# from src.utils import util_functions as uf


LENGTH_PERIOD_DISCREPANCY_IN_HOURS = 3
NB_SECONDS_IN_HOUR = 60 * 60


class ProductionDiscrepancy(BaseModel):
    """
    Discrepancy between the production, and the consumption, on the [begin, end) time interval.

    Excess and deficit are energies (beware, not powers).
    """

    begin: datetime.datetime
    end: datetime.datetime
    excess: float
    deficit: float


def compute_energy_in_kwh(power_in_kwh: float, length_period: datetime.timedelta):
    energy_in_kwh = length_period.total_seconds() / NB_SECONDS_IN_HOUR * power_in_kwh
    return energy_in_kwh


def compute_aggregate_statistics_by_periods(df_periods):
    agg_stats = dict()

    corr_excess_prod_sun_coeff = df_periods["excess_prod_energy"].corr(
        df_periods["sunshine_coefficient"]
    )
    corr_deficit_prod_sun_coeff = df_periods["deficit_prod_energy"].corr(
        df_periods["sunshine_coefficient"]
    )

    agg_stats.update(
        {
            "corr_excess_prod_energy_sun": corr_excess_prod_sun_coeff,
            "corr_deficit_prod_energy_sun": corr_deficit_prod_sun_coeff,
        }
    )

    print(f"historical aggregate statistics:\n{agg_stats}")

    return agg_stats


def compute_by_period_production_discrepancy(df_stats: pd.DataFrame):
    # by_period_production_discrepancy: List[ProductionDiscrepancy] = list()
    target_freq = f"{LENGTH_PERIOD_DISCREPANCY_IN_HOURS}H"
    length_period = datetime.timedelta(
        hours=float(f"{LENGTH_PERIOD_DISCREPANCY_IN_HOURS}")
    )

    df_periods = df_stats.resample(target_freq).mean()
    columns_to_keep = [
        "consumption",
        "production",
        "excess_prod",
        "deficit_prod",
        "sunshine_coefficient",
    ]
    df_periods = df_periods[columns_to_keep]

    df_periods["excess_prod_energy"] = df_periods["excess_prod"].apply(
        compute_energy_in_kwh, length_period=length_period
    )
    df_periods["deficit_prod_energy"] = df_periods["deficit_prod"].apply(
        compute_energy_in_kwh, length_period=length_period
    )
    # assert False, f'{df_periods[["excess_prod_energy", "deficit_prod_energy"]].describe()}'

    return df_periods


def analyse_historical_data(df_stats: pd.DataFrame) -> dict:
    processed_historical_data: dict = dict()

    df_periods = compute_by_period_production_discrepancy(df_stats)
    by_period_agg_stats = compute_aggregate_statistics_by_periods(df_periods)

    #    processed_historical_data.update({
    #        'by_period_production_discrepancy': by_period_production_discrepancy
    #    })

    return processed_historical_data

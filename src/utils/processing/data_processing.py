import datetime
import pandas as pd
import numpy as np
import logging
from sklearn import metrics
from typing import List
from src.utils.processing import data_analysis as da
from src.utils import util_functions as uf
from src.utils.processing import omegalpes_optimisation as opt


def compute_deficit_prod(
    date_and_time: datetime.datetime, consumption: pd.Series, production: pd.Series
) -> float:
    conso = consumption[date_and_time]
    prod = production[date_and_time]
    return min(prod - conso, 0.0)


def compute_excess_prod(
    date_and_time: datetime.datetime, consumption: pd.Series, production: pd.Series
) -> float:
    conso = consumption[date_and_time]
    prod = production[date_and_time]
    return max(prod - conso, 0.0)


def compute_self_production_rate(
    date_and_time: datetime.datetime, consumption: pd.Series, production: pd.Series
) -> float:
    conso = consumption[date_and_time]
    prod = production[date_and_time]

    if (
        conso == 0
    ):  # FIXME in that case, the self production rate is not defined, as there is no production
        return np.NAN
    return min(prod / conso, 1.0)


def compute_self_consumption_rate(
    date_and_time: datetime.datetime, consumption: pd.Series, production: pd.Series
) -> float:
    conso = consumption[date_and_time]
    prod = production[date_and_time]

    if (
        prod == 0
    ):  # FIXME in that case, the self consumption rate is not defined, as there is no production
        return np.NAN
    return min(conso / prod, 1.0)


def compute_self_production(
    date_and_time: datetime.datetime, consumption: pd.Series, production: pd.Series
) -> float:
    conso = consumption[date_and_time]
    prod = production[date_and_time]
    return min(conso, prod)


def compute_self_consumption(
    date_and_time: datetime.datetime, consumption: pd.Series, production: pd.Series
) -> float:
    conso = consumption[date_and_time]
    prod = production[date_and_time]
    return min(conso, prod)


def compute_aggregate_statistics(df_stats):
    agg_stats = dict()

    # FIXME these two statistics should be the same for every adviser, but this is not the case! fix this!!
    total_consumption = df_stats["consumption"].sum()
    total_production = df_stats["production"].sum()

    total_self_consumption = df_stats["self_consumption"].sum()
    total_self_production = df_stats["self_production"].sum()

    agg_self_consumption_rate = min(total_self_consumption / total_production, 1)

    agg_self_production_rate = min(total_self_production / total_consumption, 1)

    agg_stats.update(
        {
            "total_self_consumption": total_self_consumption,
            "total_self_production": total_self_production,
            "agg_self_consumption_rate": agg_self_consumption_rate,
            "agg_self_production_rate": agg_self_production_rate,
            "total_consumption": total_consumption,
            "total_production": total_production,
        }
    )

    return agg_stats


def compute_df_statistics(df: pd.DataFrame) -> pd.DataFrame:
    df_stats = df.copy(deep=True)

    stat_and_compute_function = {
        "self_consumption": compute_self_consumption,
        "self_production": compute_self_production,
        "self_consumption_rate": compute_self_consumption_rate,
        "self_production_rate": compute_self_production_rate,
        "excess_prod": compute_excess_prod,
        "deficit_prod": compute_deficit_prod,
    }

    for type_stat, compute_function in stat_and_compute_function.items():
        df_stats[type_stat] = df_stats.index.to_series().apply(
            compute_function,
            consumption=df_stats["consumption"],
            production=df_stats["production"].astype(float),
        )

    return df_stats


def compute_total_shiftable_vs_not(
    df_adviser: pd.DataFrame, shiftable_appliances_names: List[str]
) -> pd.DataFrame:
    # not_shiftable_columns = ['production', 'consumption']
    df_adviser["total_shiftable"] = df_adviser[list(shiftable_appliances_names)].sum(
        axis="columns"
    )
    df_adviser["total_not_shiftable"] = (
        df_adviser["consumption"] - df_adviser["total_shiftable"]
    )
    if (
        len(df_adviser["total_not_shiftable"][df_adviser["total_not_shiftable"] < 0])
        > 0
    ):
        breakpoint()
    return df_adviser


def process_historical_data(sim_outputs, output_rep):
    processed_historical_data = dict()

    first_adviser = list(sim_outputs.keys())[
        0
    ]  # this is just a way to get historical data, as they are stored for every adviser
    historical_data = sim_outputs[first_adviser]["data"]["historical"]

    df_hdata = uf.get_datalist_as_dataframe(historical_data["household"])
    df_hdata = uf.add_external_world_data_from_list(
        df_hdata, historical_data["external_world"]
    )

    df_stats_hdata = compute_df_statistics(df_hdata)
    hist_aggregate_statistics = compute_aggregate_statistics(df_stats_hdata)

    processed_historical_data.update(
        {
            "consumption": df_hdata["consumption"],
            "production": df_hdata["production"],
            "sunshine_coefficient": df_hdata["sunshine_coefficient"],
            "by_30min_self_consumption_rate": df_stats_hdata["self_consumption_rate"],
            "by_30min_self_production_rate": df_stats_hdata["self_production_rate"],
            "by_30min_excess_prod": df_stats_hdata["excess_prod"],
            "by_30min_deficit_prod": df_stats_hdata["deficit_prod"],
        }
    )

    processed_historical_data.update(hist_aggregate_statistics)

    #    #TODO no longer used, but maybe could reactivate?
    #    processed_historical_data.update(
    #        da.analyse_historical_data(df_stats_hdata)
    #    )

    return processed_historical_data


def process_simulation_outputs(sim_outputs, output_rep):
    processed_data = {adviser_type: dict() for adviser_type in sim_outputs.keys()}

    for adviser_type, data_adviser in sim_outputs.items():
        df_adviser = uf.get_datadict_as_dataframe(
            data_adviser["data"]["real"]["household"]
        )

        logging.debug(
            f'processing, {adviser_type}, total consumption: {df_adviser["consumption"].sum()} first date: {df_adviser.index[0]} last date: {df_adviser.index[-1]}'
        )
        df_adviser = uf.add_external_world_data_from_dict(
            df_adviser, data_adviser["data"]["real"]["external_world"]
        )
        df_adviser, shiftable_appliances_names = uf.add_shiftable_data_from_dict(
            df_adviser, data_adviser["data"]["tracking"]
        )

        # FIXME SHOULD plot, for each adviser, production, non-shiftable consumption, and on top of the latter total shiftable consumption, in a plotly graph!!
        df_adviser = compute_total_shiftable_vs_not(
            df_adviser, shiftable_appliances_names
        )

        df_stats = compute_df_statistics(df_adviser)

        aggregate_statistics = compute_aggregate_statistics(df_stats)
        logging.debug(
            f"aggregate statistics for {adviser_type}:\n{aggregate_statistics}"
        )

        processed_data[adviser_type].update(
            {
                "consumption": df_adviser["consumption"],
                "shiftable_consumption": {
                    col: df_stats[col] for col in shiftable_appliances_names
                },
                "production": df_adviser[
                    "production"
                ],  # FIXME should probably not store it for each adviser, as is the same for all advisers!
                "total_shiftable": df_adviser["total_shiftable"],
                "total_not_shiftable": df_adviser["total_not_shiftable"],
                "by_30min_self_consumption_rate": df_stats["self_consumption_rate"],
                "by_30min_self_production_rate": df_stats["self_production_rate"],
                "by_30min_excess_prod": df_stats["excess_prod"],
                "by_30min_deficit_prod": df_stats["deficit_prod"],
            }
        )
        processed_data[adviser_type].update(
            {
                "shiftable": {
                    name: df_adviser[name] for name in shiftable_appliances_names
                }
            }
        )

        processed_data[adviser_type].update(aggregate_statistics)

        if adviser_type == "CombinedController":  # FIXME improve implementation
            processed_data[adviser_type].update(
                {"predicted_excess": sim_outputs[adviser_type]["predicted_excess"]}
            )

        # assert False, f'{data_adviser["data"].keys()}, {data_adviser["meta"].keys()}'

    return processed_data


def performance_analysis(
    y_true: np.array, y_pred: np.array
) -> (
    dict
):  # FIXME should maybe not be here, but in processing; not sure  #if in processing, make same changes for other predictors!!
    # regression metrics
    r2_score = metrics.r2_score(y_true, y_pred)
    mae = metrics.mean_absolute_error(y_true, y_pred)
    rmse = metrics.mean_squared_error(y_true, y_pred, squared=False)
    mape = metrics.mean_absolute_percentage_error(y_true, y_pred)
    return {"R2Score": r2_score, "MAE": mae, "RMSE": rmse, "MAPE": mape}


def process_training_outputs(training_outputs: dict, output_rep: str) -> dict:
    adviser_name_list = [
        "CombinedControllerSimple"
    ]  # ['CombinedController', 'CombinedControllerSimple']
    processed = {}
    for adviser_name in adviser_name_list:
        training_outputs_adviser = training_outputs.get(adviser_name)
        processed.update({adviser_name: {}})
        if training_outputs_adviser is not None:
            logging.debug(f"Processing training output for {adviser_name}")

            y_val = pd.DataFrame()
            y_pred = pd.DataFrame()

            for forecaster_type, forecaster_data in training_outputs_adviser.items():
                processed[adviser_name].update({forecaster_type: dict()})
                logging.debug(f"Forecaster type: {forecaster_type}")
                y_val[forecaster_type] = forecaster_data["y_val"]
                y_pred[forecaster_type] = forecaster_data["y_pred"]

                logging.debug("computing metrics of validation result")
                metrics = performance_analysis(
                    y_val[forecaster_type], y_pred[forecaster_type]
                )  # forecaster_data['validation_metrics']
                processed[adviser_name][forecaster_type]["validation_metrics"] = metrics

                df_predict = pd.DataFrame(index=y_val.index)
                df_predict["Actual Curve"] = y_val[forecaster_type]
                df_predict["Predicted Curve"] = y_pred[forecaster_type]
                processed[adviser_name][forecaster_type]["prediction"] = df_predict
            if "Simple" in adviser_name:
                y_val = y_val["production"] - y_val["consumption"]
                y_pred = y_pred["production"] - y_pred["consumption"]
                metrics = performance_analysis(
                    y_val, y_pred
                )  # forecaster_data['validation_metrics']
                processed[adviser_name].update(
                    {"surplus": {"validation_metrics": metrics}}
                )
            breakpoint()
    return processed


def process_outputs(sim_outputs: dict, output_rep: str) -> dict:
    # TODO make sure computation of different statistics, self_conso, self_prod, rates, agg_rates and so on is sound! there may be errors!

    processed_data: dict = {"historical": dict(), "by_adviser": dict()}

    historical_processed_data = process_historical_data(sim_outputs, output_rep)
    processed_data["historical"] = historical_processed_data

    by_adviser_processed_data = process_simulation_outputs(sim_outputs, output_rep)
    processed_data["by_adviser"] = by_adviser_processed_data

    return processed_data


def collections_processed_data(processed_data: dict, precessed_trainning_outputs: dict):
    collections_info = {}
    adviser_conso_prod = pd.DataFrame()
    for key, values in processed_data["by_adviser"].items():
        collections_info[f"{key}_agg_self_consumption_rate"] = values[
            "agg_self_consumption_rate"
        ]
        collections_info[f"{key}_agg_self_production_rate"] = values[
            "agg_self_production_rate"
        ]
        collections_info[f"{key}_nb_shift"] = len(values["shiftable"])

        adviser_conso_prod[f"{key}_consumption"] = values["consumption"]
        for shift_app_name, shift_app_profile in values[
            "shiftable_consumption"
        ].items():
            adviser_conso_prod[
                f"{key}_shiftable_consumption_{shift_app_name}"
            ] = shift_app_profile
        adviser_conso_prod[f"{key}_production"] = values["production"]
        adviser_conso_prod[f"{key}_total_shiftable"] = values["total_shiftable"]
        adviser_conso_prod[f"{key}_total_not_shiftable"] = values["total_not_shiftable"]

    historical_conso_prod = pd.concat(
        [
            processed_data["historical"]["consumption"],
            processed_data["historical"]["production"],
        ],
        axis=1,
    )
    # collections_info['MAPE_RF'] = precessed_trainning_outputs['CombinedController']['surplus']['validation_metrics']['MAPE']
    # collections_info['R2Score'] = precessed_trainning_outputs['CombinedController']['surplus']['validation_metrics']['R2Score']
    # collections_info['MAPE_Sim'] = precessed_trainning_outputs['CombinedControllerSimple']['surplus']['validation_metrics']['MAPE']
    # collections_info['R2Score_Sim'] = precessed_trainning_outputs['CombinedControllerSimple']['surplus']['validation_metrics']['R2Score']
    # collections_info['MAPE_pref'] = precessed_trainning_outputs['PerfectPredicter']['production']['validation_metrics']['MAPE']
    # collections_info['R2Score_pref'] = precessed_trainning_outputs['PerfectPredicter']['production']['validation_metrics']['R2Score']
    return collections_info, historical_conso_prod, adviser_conso_prod


def collect_optimisation_data(
    collections_info: pd.DataFrame, adviser_conso_prod: pd.DataFrame
):
    consumpt = adviser_conso_prod["No control_consumption"]
    prod = adviser_conso_prod["No control_production"]

    shift_consumpt = adviser_conso_prod[
        [
            col
            for col in adviser_conso_prod.columns
            if "No control_shiftable_consumption_" in col
        ]
    ]
    shift_consumpt = shift_consumpt.rename(
        columns={
            col: col.replace("No control_shiftable_consumption_", "")
            for col in shift_consumpt.columns
        }
    )
    shift_not_consumpt = adviser_conso_prod["No control_total_not_shiftable"]
    index = adviser_conso_prod.index
    delta = (index[1] - index[0]).seconds
    dt = delta / 3600
    nb_sample_day = 24 / dt  # windows length = 3 day

    omegalpes_data = pd.DataFrame()
    for i in range(len(consumpt)):
        if i % nb_sample_day == 0:
            nb_day = int(i / nb_sample_day)
            start = int(nb_day * nb_sample_day)
            end = int((nb_day + 1) * nb_sample_day)
            if end > len(prod):
                end = len(prod)
                nb_sample_day = end - start
            # time_interval = [start, end]
            production = prod[start:end]
            shift = shift_consumpt[start:end]
            non_shift = shift_not_consumpt[start:end]
            elec_node, model = opt.create_omegalpe_model(
                production, shift, non_shift, nb_sample_day, dt
            )
            profile_table = opt.get_optim_result(elec_node)

            consumpt_opti = (
                profile_table[[col for col in profile_table if "shift_app" in col]].sum(
                    axis=1
                )
                + profile_table["non_shiftable_consommation"]
            )
            total_shiftalbe = profile_table[
                [col for col in profile_table if "shift_app" in col]
            ].sum(axis=1)
            total_not_shiftalbe = profile_table["non_shiftable_consommation"]
            omegalpes_consumpt_prod = pd.DataFrame(index=production.index)
            omegalpes_consumpt_prod["Omegalpes_production"] = production.values
            omegalpes_consumpt_prod["Omegalpes_consumption"] = consumpt_opti.values
            omegalpes_consumpt_prod[
                "Omegalpes_total_shiftable"
            ] = total_shiftalbe.values
            omegalpes_consumpt_prod[
                "Omegalpes_total_not_shiftable"
            ] = total_not_shiftalbe.values
            # omegalpes_consumpt_prod = omegalpes_consumpt_prod.set_index('index', inplace=True)

            omegalpes_data = pd.concat(
                [omegalpes_data, omegalpes_consumpt_prod], axis=0
            )

    adviser_conso_prod = pd.concat([adviser_conso_prod, omegalpes_data], axis=1)

    omegalpes_self_consumpion = adviser_conso_prod.index.to_series().apply(
        compute_self_consumption,
        consumption=adviser_conso_prod["Omegalpes_consumption"],
        production=adviser_conso_prod["Omegalpes_production"].astype(float),
    )

    omegalpes_self_production = adviser_conso_prod.index.to_series().apply(
        compute_self_production,
        consumption=adviser_conso_prod["Omegalpes_consumption"],
        production=adviser_conso_prod["Omegalpes_production"].astype(float),
    )
    total_self_consumption = omegalpes_self_consumpion.sum()
    total_self_production = omegalpes_self_production.sum()
    total_production = adviser_conso_prod["Omegalpes_production"].sum()
    total_consumption = adviser_conso_prod["Omegalpes_consumption"].sum()
    self_consumption_rate = min(total_self_consumption / total_production, 1)
    self_production_rate = min(total_self_production / total_consumption, 1)

    collections_info["Omegalpes_agg_self_consumption_rate"] = self_consumption_rate
    collections_info["Omegalpes_agg_self_production_rate"] = self_production_rate

    return collections_info

# import modules

# custom modules

# class


class UnvalidForecastingDataType(Exception):
    def __init__(self, data_type: set):
        """
        Raise when data type asked for prediction is not valid.

        :param data_type: requested data type
        """
        message = f'{data_type} is not available for prediction, please choose between between "consumption", "production" and "surplus"'
        super().__init__(message)


class UnvalidDataType(Exception):
    def __init__(self, data_type: set):
        """
        Raise when data type is not valid.

        :param data_type: requested data type
        """
        message = f"{data_type} is not available"
        super().__init__(message)

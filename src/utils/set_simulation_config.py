import os
import json
import pandas as pd
from typing import Optional
import copy
from src.real_world_classes.data_collection import data_center as dc
from src.real_world_classes.advisers import (
    WeatherController,
    IdleAdvisor,
    CombinedController,
)
from src.simulator import simulation_manager as sm
from src.utils.processing import data_processing as pr


class SimulationConfig:
    def __init__(
        self,
        data_path: str,
        predictor_type: str = "simple",
        historical_data_begin: str = None,
        historical_data_end: str = None,
        begin_simulation: str = None,
        length_of_simulation: int = None,
    ):
        self.data_path = data_path
        self.nb_periods_max = 4
        self.predictor_type = predictor_type
        self.historical_data_begin = historical_data_begin
        self.historical_data_end = historical_data_end
        self.begin_simulation = begin_simulation
        self.length_of_simulation = length_of_simulation
        self.coeff_shunshin_to_prode = 1900
        self.household_name = os.path.split(self.data_path)[1][0:-4]

    def get_config(self) -> dict:
        """Loads the config, from the specified yaml file."""
        inputs_rep = "./data/inputs/"
        outputs_rep = "./data/outputs/"
        config_name = "config"
        filename = inputs_rep + config_name + ".json"
        config_output_rep = outputs_rep + config_name + "/"
        with open(filename, "r") as f:
            my_config = json.load(f)
            my_config.update({"output_rep": config_output_rep})
            if not os.path.exists(config_output_rep):
                os.makedirs(config_output_rep)
            return my_config
        raise FileNotFoundError

    def get_househould_timeline(self, cfg: str):
        inputs_rep = "./data/inputs/"
        file_name = "household_timeline"
        filename = inputs_rep + file_name + ".json"
        with open(filename, "r") as f:
            household_timeline = json.load(f)
            timeline = household_timeline[cfg["simulation"]["household"]["name"]]
            return timeline

    @property
    def simlation_config(self):
        cfg = self.get_config()

        cfg["simulation"]["household"]["name"] = self.household_name
        cfg["simulation"]["household"]["path"] = self.data_path

        if (
            self.historical_data_begin == None
            or self.historical_data_end == None
            or self.begin_simulation == None
            or self.length_of_simulation == None
        ):
            timeline = self.get_househould_timeline(cfg)
            self.historical_data_begin = timeline["historical_data"]["begin"]
            self.historical_data_end = timeline["historical_data"]["end"]
            self.begin_simulation = timeline["begin_simulation"]
            self.length_of_simulation = timeline["length"]
        for key, values in cfg["advisers"].items():
            if key != "IdleAdvisor":
                cfg["advisers"][key]["nb_periods_max"] = self.nb_periods_max

        cfg["simulation"]["timeline"]["historical_data"][
            "begin"
        ] = self.historical_data_begin
        cfg["simulation"]["timeline"]["historical_data"][
            "end"
        ] = self.historical_data_end
        cfg["simulation"]["timeline"]["begin_simulation"] = self.begin_simulation
        cfg["simulation"]["timeline"]["length"] = self.length_of_simulation

        cfg["simulation"]["household"][
            "coeff_shunshin_to_prode"
        ] = self.coeff_shunshin_to_prode

        house = pd.read_csv(self.data_path, index_col=0, parse_dates=True)

        nb_app = 0
        for col in house.columns:
            if "clothes_drier" in col or "clothes_dryer" in col:
                cfg["simulation"]["household"]["shiftable"][col] = "9H"
                nb_app += 1
            elif (
                "washing_machine" in col
                or "clothes iron" in col
                or "washer dryer" in col
                or "clothes_washer" in col
            ):
                cfg["simulation"]["household"]["shiftable"][col] = "9H"
                nb_app += 1
            elif "dish_washer" in col or "dish washer" in col or "dishwasher" in col:
                cfg["simulation"]["household"]["shiftable"][col] = "9H"
                nb_app += 1
            elif "water_heater" in col:
                cfg["simulation"]["household"]["shiftable"][col] = "10H"
            else:
                pass
        cfg["simulation"]["household"]["nb_appliances"] = nb_app

        return cfg


class RunSimulation:
    def __init__(
        self,
        simconf: SimulationConfig,
        nb_runs: Optional[int] = None,
        run_optimisation=False,
        coeff_sunshine: Optional[list[int]] = None,
        nb_periods_max: Optional[list[int]] = None,
    ):
        self.simconf = simconf
        self.nb_runs = nb_runs
        self.run_optimisation = run_optimisation
        self.coeff_sunshine = coeff_sunshine
        self.nb_periods_max = nb_periods_max

    def run_simulation(self):
        collections_processed_data = {}
        cfg = self.simconf.simlation_config
        if self.nb_periods_max is None:
            self.nb_periods_max = [self.simconf.nb_periods_max]

        if self.nb_runs is None:
            self.nb_runs = 1

        if self.coeff_sunshine is None:
            self.coeff_sunshine = [self.simconf.coeff_shunshin_to_prode]

        for coeff in self.coeff_sunshine:
            for nb_max in self.nb_periods_max:
                cfg["advisers"]["WeatherController"]["nb_periods_max"] = nb_max
                cfg["advisers"]["CombinedController"]["nb_periods_max"] = nb_max
                cfg["simulation"]["household"]["coeff_shunshin_to_prode"] = coeff

                for nb in range(self.nb_runs):
                    cfg["simulation"]["external_world"]["seed"] = nb * 1000
                    seed_value = cfg["simulation"]["external_world"]["seed"]
                    print(
                        f"simulation with production level = {coeff} and number of greens periods max ={nb_max}, "
                        f"simulation process at {nb+1} / {self.nb_runs} and seed_value={seed_value}"
                    )
                    (
                        collections_infos,
                        historical_conso_prod,
                        adviser_conso_prod,
                    ) = self.run_one_simulation(cfg)

                    house_key = cfg["simulation"]["household"]["name"]
                    collections_processed_data[
                        f"{house_key}_{coeff}_{nb_max}_{nb}"
                    ] = collections_infos
        return pd.DataFrame.from_dict(collections_processed_data)

    def run_one_simulation(self, cfg: str):
        advisers_choice = {
            "No control": IdleAdvisor,
            "WeatherController": WeatherController,  # Weather Controller
            "CombinedController": CombinedController,  # Combined Controller
        }

        sim_outputs = dict()
        training_outputs = dict()
        conso_by_week = dict()  # FIXME for debugging
        changes_conso = dict()

        advisors_list = [
            adviser_class.make_adviser(cfg["advisers"].get(adviser_name))
            for adviser_name, adviser_class in advisers_choice.items()
        ]

        once_and_for_all_simulator = sm.make_simulator(cfg["simulation"])
        for advisor in advisors_list:
            simulator = copy.deepcopy(once_and_for_all_simulator)
            data_center = dc.DataCenter(simulator)

            advisor.set_historical_data(data_center)

            training_outputs_adviser = advisor.train()

            sim_tracking_data = simulator.run_simulation(data_center, advisor)

            conso_by_week_adv = sim_tracking_data["conso_by_week"]
            conso_by_week.update({advisor.name: conso_by_week_adv})
            changes_conso_adv = sim_tracking_data["changes_conso"]
            changes_conso.update({advisor.name: changes_conso_adv})

            sim_outputs.update({advisor.name: data_center.get_simulation_outputs()})
            training_outputs.update(
                {advisor.name: copy.deepcopy(training_outputs_adviser)}
            )

            if advisor.name == "CombinedController":  # FIXME improve implementation
                sim_outputs[advisor.name].update(
                    {"predicted_excess": sim_tracking_data["predicted_excess"]}
                )

        processed_data = pr.process_outputs(sim_outputs, cfg["output_rep"])
        processed_training_outputs = pr.process_training_outputs(
            training_outputs, cfg["output_rep"]
        )
        (
            collections_infos,
            historical_conso_prod,
            adviser_conso_prod,
        ) = pr.collections_processed_data(processed_data, processed_training_outputs)

        if self.run_optimisation == True:
            collections_infos = pr.collect_optimisation_data(
                collections_infos, adviser_conso_prod
            )

        return collections_infos, historical_conso_prod, adviser_conso_prod

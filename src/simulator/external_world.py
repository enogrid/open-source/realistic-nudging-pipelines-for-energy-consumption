# import modules
import pandas as pd
import numpy as np
from datetime import datetime, date, timedelta, time
from typing import List
import logging
from scipy import stats
import calendar

from src.real_world_classes.data_collection.data_models import ExternalWorldData


ALL_STATES = ["sunny", "slightly_cloudy", "cloudy", "very_cloudy", "awful"]

STATES_TO_VAL = {
    "sunny": 1.0,
    "slightly_cloudy": 0.65,
    "cloudy": 0.35,
    "very_cloudy": 0.2,
    "awful": 0.1,
}

MONTH_TO_VAL = {
    "July": 1,
    "June": 0.9,
    "May": 0.7,
    "April": 0.55,
    "March": 0.35,
    "February": 0.2,
    "January": 0.3,
    "December": 0.4,
    "November": 0.45,
    "October": 0.55,
    "September": 0.7,
    "August": 0.85,
}


# class
class ExternalWorld:
    """Represents everything like the weather, and so on."""

    def __init__(self, seed_value):
        self.sunshine_coefficient = (
            pd.DataFrame()
        )  # FIXME rather, should be a pd.Series, but code uses the fact it is a DataFrame, when accessing it (df.loc...)
        self.smoothed_weather_data = pd.DataFrame()
        self.seed = seed_value

    def construct_simulated_sunshine_coefficient(
        self, begin: datetime, end: datetime
    ) -> None:
        # FIXME all construction could, and should probably, be improved!
        df_coeff = compute_cloud_sequence(begin, end, self.seed)

        df_coeff["cloud_coeff"] = df_coeff["cloud_level"].apply(
            lambda cloud_level: get_cloud_level_value(cloud_level)
        )

        df_coeff["time_coeff"] = df_coeff.index.to_series().apply(
            lambda date_time: get_time_value(date_time.time())
        )

        df_coeff["month_coeff"] = df_coeff.index.to_series().apply(
            lambda date_month: get_month_value(date_month.date().month)
        )

        df_coeff["sunshine_coeff"] = (
            df_coeff["cloud_coeff"] * df_coeff["time_coeff"] * df_coeff["month_coeff"]
        )

        # renormalisation
        df_coeff["sunshine_coeff"] /= df_coeff["sunshine_coeff"].max()

        self.sunshine_coefficient = pd.DataFrame(df_coeff["sunshine_coeff"])
        self.sunshine_coefficient.index.name = "date"  # issues later if not done, part of the code expects that name to be there

    def get_historical_data(
        self, begin: datetime, end: datetime
    ) -> List[ExternalWorldData]:
        historical_coeff = self.sunshine_coefficient.loc[begin:end, "sunshine_coeff"]
        historical_weather_data = self.smoothed_weather_data.loc[begin:end]

        historical_data = [
            ExternalWorldData(
                begin=date_and_time,
                length=timedelta(minutes=30),
                sunshine_coefficient=float(coeff),
                temperature=0,
                rayonnement=0,
            )
            for date_and_time, coeff in historical_coeff.items()
        ]

        for record in historical_data:
            if (
                record.begin in historical_weather_data.index
            ):  # FIXME hack, because weather file does not have right dates for uk-dale
                record.temperature = historical_weather_data[
                    "Température réalisée lissée (°C)"
                ][record.begin]
                record.rayonnement = historical_weather_data["Pseudo rayonnement (%)"][
                    record.begin
                ]
            else:
                record.temperature = 0
                record.rayonnement = 0

        return historical_data

    def get_daily_data(self, day: date):
        def is_right_day(date_and_time: datetime) -> bool:
            return date_and_time.date() == day

        df_daily_data = self.sunshine_coefficient[
            self.sunshine_coefficient.index.to_series().apply(is_right_day)
        ]

        daily_data = {
            "sunshine_coefficient": list(),
        }

        daily_data["sunshine_coefficient"] = [
            ExternalWorldData(
                begin=date_and_time,
                length=timedelta(minutes=30),  # FIXME will not always be 30!!
                sunshine_coefficient=float(coefficient),
                temperature=0,
                rayonnement=0,
            )
            for date_and_time, coefficient in df_daily_data["sunshine_coeff"].items()
        ]

        for record in daily_data["sunshine_coefficient"]:
            if (
                record.begin in self.smoothed_weather_data.index
            ):  # FIXME hack, because weather file does not have right dates for uk-dale
                record.temperature = self.smoothed_weather_data[
                    "Température réalisée lissée (°C)"
                ][record.begin]
                record.rayonnement = self.smoothed_weather_data[
                    "Pseudo rayonnement (%)"
                ][record.begin]
            else:
                record.temperature = 0
                record.rayonnement = 0

        return daily_data

    def predict_sunshine_coefficient(self, day_begin: date, nb_days_ahead: int):
        def get_next_days(date_and_time: datetime) -> bool:
            return (
                day_begin
                <= date_and_time.date()
                <= day_begin + pd.Timedelta(days=nb_days_ahead)
            )

        true_future_coeffs = self.sunshine_coefficient[
            self.sunshine_coefficient.index.to_series().apply(get_next_days)
        ]

        org_index = true_future_coeffs.index
        true_future_coeffs.reset_index(inplace=True)

        # helper function
        def add_perturbation(
            sunshine_coeff, diff_days
        ):  # FIXME no perturbation for now, see code below!!
            nb_days_ahead = diff_days.days
            perturbated_value = sunshine_coeff  # + nb_days_ahead * .1 * (2 * rng.random() - 1)  #FIXME constant values (".1") should be in config or something!

            perturbated_value = max(
                min(perturbated_value, 1), 0
            )  # constraining to [0,1]

            return perturbated_value

        rng = np.random.default_rng(seed=self.seed)
        # logging.warning(f'USING FIXED SEED FOR BEGINNING FOR weather forecast!!!')

        predicted_future_coeffs = true_future_coeffs.apply(
            lambda row: add_perturbation(
                row["sunshine_coeff"], row["date"].date() - day_begin
            ),
            axis="columns",
        )
        predicted_future_coeffs.index = org_index
        predicted_future_coeffs.name = "predicted_sunshine_coeff"

        return predicted_future_coeffs

    def import_smoothed_weather_data(self, cfg: dict) -> None:
        weather_file = cfg["smoothed_weather_path"]
        weather = pd.read_csv(weather_file, index_col=0, parse_dates=True)
        weather = weather.sort_index()
        weather = weather.dropna()
        weather = weather.resample("30T", closed="left").mean()
        self.smoothed_weather_data = weather

    def predict_smoothed_weather(self, day: date, nb_days_ahead: int) -> pd.DataFrame:
        def get_next_days(date_and_time: datetime) -> bool:
            return day <= date_and_time.date() <= day + pd.Timedelta(days=nb_days_ahead)

        future_weather = self.smoothed_weather_data[
            self.smoothed_weather_data.index.to_series().apply(get_next_days)
        ]

        return future_weather


# functions
def generate_external_world(cfg: dict):
    external_world = ExternalWorld(cfg["seed"])  # FIXME
    return external_world


def is_stochastic_matrix(mat: np.array) -> bool:
    nb_lines, nb_col = mat.shape
    if not (nb_lines == nb_col and np.isclose(np.sum(mat, axis=1), 1).all()):
        raise Exception("Is not stochastic matrix")
    return True


def compute_cloud_sequence(begin: datetime, end: datetime, seed_value):
    def coord(
        state_name: str,
    ) -> int:  # FIXME state_name should be of type (derived of) Enum
        return ALL_STATES.index(state_name)

    def fill(state_from: str, state_to: str, proba: float) -> None:
        trans_mat[coord(state_from), coord(state_to)] = proba

    nb_states = len(ALL_STATES)  # "sunny", "cloudy", etc.

    trans_mat = np.zeros((nb_states, nb_states), dtype=float)

    fill("sunny", "sunny", 0.3)
    fill("sunny", "slightly_cloudy", 0.25)
    fill("sunny", "cloudy", 0.2)
    fill("sunny", "very_cloudy", 0.15)
    fill("sunny", "awful", 0.1)

    fill("slightly_cloudy", "sunny", 0.2)
    fill("slightly_cloudy", "slightly_cloudy", 0.25)
    fill("slightly_cloudy", "cloudy", 0.3)
    fill("slightly_cloudy", "very_cloudy", 0.15)
    fill("slightly_cloudy", "awful", 0.1)

    fill("cloudy", "sunny", 0.15)
    fill("cloudy", "slightly_cloudy", 0.2)
    fill("cloudy", "cloudy", 0.2)
    fill("cloudy", "very_cloudy", 0.25)
    fill("cloudy", "awful", 0.1)

    fill("very_cloudy", "sunny", 0.1)
    fill("very_cloudy", "slightly_cloudy", 0.1)
    fill("very_cloudy", "cloudy", 0.25)
    fill("very_cloudy", "very_cloudy", 0.35)
    fill("very_cloudy", "awful", 0.2)

    fill("awful", "sunny", 0.05)
    fill("awful", "slightly_cloudy", 0.2)
    fill("awful", "cloudy", 0.25)
    fill("awful", "very_cloudy", 0.3)
    fill("awful", "awful", 0.2)

    #    for idx_state in range(nb_states):
    #        trans_mat[idx_state, idx_state] *= 3  #meaning change is less probable

    trans_mat = (
        np.diag(1 / np.sum(trans_mat, axis=1)) @ trans_mat
    )  # now the distributions on each line of the policy should be uniform

    is_stochastic_matrix(trans_mat)

    sampling_step_weather_changes = "4H"  #'6H'  #'3H'

    datetime_index = pd.date_range(
        start=begin, end=end, freq=sampling_step_weather_changes
    )
    df_cloud = pd.DataFrame(index=datetime_index)
    df_cloud["cloud_level"] = None

    np.random.seed(seed_value)  # scipy uses this

    # Markov chain
    state = "sunny"
    for slot in datetime_index:
        distrib = trans_mat[coord(state), :]

        random_variable = stats.rv_discrete(
            name="new_state_rv", values=(range(nb_states), distrib)
        )
        new_state_coord = random_variable.rvs(size=1)[0]
        new_state = ALL_STATES[new_state_coord]
        df_cloud.loc[slot, "cloud_level"] = new_state
        state = new_state

    target_freq = "30min"
    df_cloud = df_cloud.resample(target_freq).ffill()

    # adding last slot, in case ends at 23:00, and not at 23:00
    # FIXME very ad-hoc code, improve!!
    last_slot = df_cloud.index[-1]
    last_cloud = df_cloud.loc[last_slot, "cloud_level"]
    # last_slot = last_slot.replace(hour=23, minute=30)
    last_slot = end

    # df_cloud.loc[last_slot, 'cloud_level'] = last_cloud  #does not seem to work
    df_cloud.index.name = "datetime"
    df_cloud.reset_index(inplace=True)
    addition = pd.DataFrame([[last_slot, last_cloud]], columns=df_cloud.columns)
    df_cloud = pd.concat([df_cloud, addition])
    df_cloud.set_index("datetime", inplace=True)

    df_cloud = df_cloud.resample(target_freq).ffill()
    return df_cloud


def get_cloud_level_value(
    cloud_level: str,
) -> float:  # again, cloud_level should be of type Enum or derived
    return STATES_TO_VAL[cloud_level]


def get_time_value(time_in_day: time) -> float:
    for_comparison_time_in_day = datetime.combine(
        datetime.now().date(), time_in_day
    )  # comparaison between time objects is not allowed

    night_end = datetime.combine(datetime.now().date(), time(hour=6, minute=0))
    early_morning_end = datetime.combine(datetime.now().date(), time(hour=9, minute=30))
    midday = datetime.combine(datetime.now().date(), time(hour=12, minute=0))
    late_afternoon_begin = datetime.combine(
        datetime.now().date(), time(hour=16, minute=0)
    )
    evening_begin = datetime.combine(datetime.now().date(), time(hour=19, minute=0))
    night_begin = datetime.combine(datetime.now().date(), time(hour=21, minute=30))

    if for_comparison_time_in_day < night_end:
        return 0.0
    elif for_comparison_time_in_day < early_morning_end:
        return 0.25  # reversed: .7 #reversed: if production more important in mornings and evenings
    elif for_comparison_time_in_day < midday:
        return 0.7  # reversed: .5
    elif for_comparison_time_in_day < late_afternoon_begin:
        return 1  # reversed: .5
    elif for_comparison_time_in_day < evening_begin:
        return 0.65
    elif for_comparison_time_in_day < night_begin:
        return 0.25  # reversed: .7
    else:
        return 0.0


def get_month_value(
    month_number: int,
) -> float:  # again, cloud_level should be of type Enum or derived
    month_name = calendar.month_name[month_number]
    return MONTH_TO_VAL[month_name]

# Household and related classes

The classes here define the interface an household must respect, and implements basic functionalities.

# Household

## Set-up
The classmethod `generate_household` constructs the household. It takes as argument a config dictionary, the external world [maybe in the future this could be discarded], and timeline data.

## Attributes
The household attributes are:
- an **appliances handler**, which controls the way the several appliances of the household consume energy;
- a **production unit**, which represents the production of energy by e.g. photovoltaic pannels;
- an **advice handler**, which contains logic modelling human behaviour wrt. nudges.

## Workings
The main method is `elapse_day`. It takes as argument the date, and an advice (a Flexibility potential object). Then:

1. it asks the `advice_handler` whether the advice is accepted;
2. it lets the appliances handler modify consumption, taking into account the advice;
3. it asks the appliances handler to update an internal variable storing daily consumption data.

The method `get_daily_data` outputs both "real" data (which may be obtained in the real world), and "tracking" data, which can only be observed in a controlled environment.

# Appliances handler

## Set-up
The `init` method controls sets the object up, depending on the underlying dataset, or model used.

## Attributes
The main attributes are

- **energy_profile**: a pandas DataFrame with a DatetimeIndex, and a column for each appliance, which values are average power over the period represented by the index entry;
- **shiftable_appliances**: a dictionary with the names of the shiftable appliances as keys, and the shiftable appliances objets as values;
- **daily_energy_profile**: containing the consumption data for each day.

## Workings
The main methods are `update_daily_energy_profile`, and `apply_advice`.

`apply_advice` takes as argument the date, and an advice. It

1. asks every shiftable appliance to move its consumption if possible, by taking into account the advice;
2. then, it applies the changes, by modifying accordingly the `energy_profile` attribute.

# Shiftable appliance

## Set-up
It computes use intervals from the load curve of the appliance: intervals on which the appliance is used. 

## Attributes
The main attributes are
- the **energy profile** (the load curve of the appliance);
- the **use intervals**;
- the **time max shifting** variable, which represents the maximum time a use interval can be differed (e.g. max 6 hours for the dish-washer, or 3 days for the washing-machine).

## Workings
The main method is `shift_if_possible`, which takes as arguments the date, and an advice.

It compares the recommandation periods suggested by the advice, to the use intervals of the appliance, and matches them given in particular the `time_max_shifting` constraint. It outputs tuples (old use interval, new use interval) representing the shift in consumption.

# Production unit
In turns, there should probably be a class "Production Unit", as there is an "Appliances handler".

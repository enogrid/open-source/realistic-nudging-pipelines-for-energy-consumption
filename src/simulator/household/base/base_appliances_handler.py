import datetime
import numpy as np
import logging

from src.real_world_classes import flexibility_potential as fp
from src.utils import util_functions as uf


class BaseAppliancesHandler:
    """Gathers logic about appliances: appliances shiftable or not, and so on."""

    def __init__(self):
        self.daily_energy_profile = (
            dict()
        )  # FIXME overriden in derived classes, improve!  #FIXME energy, or power, profile?

    def get_daily_data(self, day: datetime.date):
        """Ouptuts the daily data, formatted as a smart sensor would do, with average consumption/or power? other 30min intervals."""
        daily_data = self.daily_energy_profile[day].copy(deep=True)
        # FIXME should these statistics be computed here, or in the DataCenter?
        daily_data["whole"] = daily_data[self.all_appliances_names].sum(
            axis="columns"
        )  # FIXME self.all_appliances should exist in the derived classes, improve!

        # FIXME for debugging
        tot_shift = daily_data[self.shiftable_appliances_names].sum(axis="columns")
        if len(daily_data["whole"][daily_data["whole"] < tot_shift]) > 0:
            breakpoint()
        return daily_data

    def update_daily_energy_profile(self, date: datetime.date):
        raise NotImplementedError()

    def apply_advice(
        self, date: datetime.date, advice: fp.FlexibilityPotential
    ) -> dict:
        # FIXME should make sure the whole way the shifting is done is sound!

        changes_in_consumption = dict()

        for name, appliance in self.shiftable_appliances.items():
            self.check_power_use_intervals(name, self.shiftable_appliances[name])

            changes_in_consumption.update({name: list()})
            candidates_moves = appliance.shift_if_possible(advice, date)

            if candidates_moves is not None:
                for (
                    slots
                ) in (
                    candidates_moves
                ):  # FIXME total consumption of shiftable appliances CHANGES when moving, correct!
                    old_lwr, old_upr = (
                        slots["old"].lower_bound,
                        slots["old"].upper_bound,
                    )
                    new_lwr, new_upr = (
                        slots["new"].lower_bound,
                        slots["new"].upper_bound,
                    )

                    total_power_new_slot_before_move = self.get_conso(
                        new_lwr, new_upr, name
                    )
                    logging.debug(
                        f"total power in new slot before move (should be 0 -- or will be enforced, check/improve mechanism; only move when 0): {total_power_new_slot_before_move}"
                    )

                    if total_power_new_slot_before_move == 0:
                        logging.debug(
                            f"total power in old slot before move: {self.get_conso(old_lwr, old_upr, name)}"
                        )
                        old_conso = self.get_conso(old_lwr, old_upr, name)

                        if (
                            slots["new"].upper_bound > advice.end_validity
                        ):  # FIXME in turn, could probably remove, and same for below
                            raise

                        if slots["new"].lower_bound < advice.begin_validity:
                            raise

                        change_performed = self.shiftable_appliances[
                            name
                        ].change_if_conform(slots)
                        if change_performed:
                            self.shift_conso(old_lwr, old_upr, new_lwr, new_upr, name)

                            logging.debug(
                                f"total power in old slot after move (should be 0): {self.get_conso(old_lwr, old_upr, name)}"
                            )
                            logging.debug(
                                f"total power in new slot after move: {self.get_conso(new_lwr, new_upr, name)}"
                            )

                            changes_in_consumption[name].append(
                                {
                                    "slots": slots,
                                    "old_conso": old_conso,
                                    "new_conso": self.get_conso(new_lwr, new_upr, name),
                                }
                            )

                self.check_power_use_intervals(name, self.shiftable_appliances[name])

        return changes_in_consumption

    def analyse_shiftable_potential(self) -> None:
        sim_begin = self.sim_power_profile.index[0]
        sim_end = self.sim_power_profile.index[-1]

        total_conso = self.get_conso(sim_begin, sim_end, "whole").sum()
        total_shiftable_conso = sum(
            [
                self.get_conso(sim_begin, sim_end, name).sum()
                for name in self.shiftable_appliances_names
            ]
        )
        pc_shiftable = total_shiftable_conso / total_conso * 100
        logging.debug(
            f"total conso: {total_conso}, total shiftable: {total_shiftable_conso}, pc: {pc_shiftable}"
        )
        # FIXME should compute (probably not in this class) part of shiftable consumption in excess of production (see graph with both production and consumption curves) -> therefore, production deficit part "due to" shiftable!
        # FIXME first, should compute total deficit, and excess

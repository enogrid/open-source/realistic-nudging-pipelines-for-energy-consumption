from datetime import datetime
import pandas as pd
import logging

from src.simulator import external_world as ew


# class
class ProductionUnit:
    """Production unit."""

    def __init__(self, production_profile):
        self.production_profile = production_profile  # TODO should make a production unit class, in the spirit of appliances_handler?

    @classmethod
    def generate(cls, cfg: dict, external_world: ew.ExternalWorld, timeline_data: dict):
        production_profile = (
            cfg["coeff_shunshin_to_prode"]
            * external_world.sunshine_coefficient["sunshine_coeff"]
        )
        production_profile.name = "production_profile"
        return cls(production_profile)

    def get_daily_production_profile(
        self, date: datetime
    ):  # FIXME should format as close as possible to appliances_handler.get_daily_data (even if no production handler so far)
        day_prod_profile = self.production_profile[
            self.production_profile.index.to_series().apply(
                lambda date_and_time: date_and_time.date() == date
            )
        ]
        return day_prod_profile.copy(deep=True)

    # FIXME partly duplicated from get_conso in the appliances handlers
    def get_prod(
        self, prod_from: datetime, prod_to: datetime
    ) -> (
        pd.Series
    ):  # FIXME starting to overhaul the whole energy load curves management system, as so far, cannot understand the non invariant issue
        """
        Bounds included.

        Can only be used for sim data (not for historical data).
        """
        return self.production_profile.loc[
            prod_from:prod_to
        ]  # FIXME in turns, replace energy profile by sim_energy_profile, and make another profile for historical data

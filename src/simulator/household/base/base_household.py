# import modules

from datetime import date, datetime, timedelta
from typing import List
import logging
import pandas as pd

# custom modules
from src.real_world_classes.data_collection.data_models import (
    HouseholdEnergyData,
    SimulatorTrackingData,
    SimulatorShiftableTrackingData,
)
from src.real_world_classes import flexibility_potential as fp
from src.simulator import external_world as ew
from src.simulator.household import advice_handler as ah
from src.simulator.household.base import base_appliances_handler as baph


# class
class BaseHousehold:  # FIXME BaseHousehold and BaseAppliancesHandler should define the interfaces! so far, contents are a bit untangled with their derived classes in iris! it will probably cause problems upon execution!
    """Simulates the energy consumption of a household."""

    def __init__(
        self, days_between_advice: int
    ):  # FIXME reorganise the interaction between init and the factory functions, so far poor!
        # TODO appliances handler and production unit are defined in the derived classes, but probably bad idea to write them here like this
        self.appliances_handler: baph.BaseAppliancesHandler = None  # fixed in the derivec classes #baph.AppliancesHandler(shiftable_appliances_names)
        self.production_unit = None

        self.advice_handler = ah.AdviceHandler(days_between_advice)

        # statistics; compute in this class, or elsewhere?
        self.changes_in_excess_prod = {}
        self.changes_in_deficit_prod = {}

    @classmethod
    def generate_household(
        cls,
        cfg: dict,
        external_world: ew.ExternalWorld,
        timeline_data: dict,
    ):  # no return type, because BaseHousehold not yet defined
        """Generates the household, together with all the data needed to make it run."""
        raise NotImplementedError()

    def get_historical_data(
        self, begin: datetime, end: datetime
    ) -> List[HouseholdEnergyData]:
        raise NotImplementedError()

    def elapse_day(
        self, date: date, advice: fp.FlexibilityPotential = None
    ) -> dict:  # FIXME total_conso_day sent back for debug
        if pd.Timestamp(date).dayofweek == 0:
            breakpoint()
        day_tracking_data = dict()
        if advice is not None and not advice.is_void():
            logging.debug("advice not void")
            if self.advice_handler.accept_advice(date, advice):
                changes_in_consumption = self.appliances_handler.apply_advice(
                    date, advice
                )
                day_tracking_data.update(
                    {"changes_in_consumption": changes_in_consumption}
                )

                # self.update_changes_in_discrepancy_production(date, changes_in_consumption)  #FIXME the method is still defined, with the self, but not in the class, and it does not provoke errors during execution???

        total_conso_day = self.appliances_handler.update_daily_energy_profile(date)

        logging.debug(
            f'Day {date.strftime("%A")} {date} has elapsed. Total conso day: {total_conso_day}'
        )

        day_tracking_data.update(
            {
                "total_conso_day": total_conso_day,
            }
        )

        return day_tracking_data  # total_conso_day, changes_in_consumption

    def get_daily_data(self, day: date):
        """Ouptuts the daily data, formatted as a smart sensor would do, with average consumption/or power? other 30min intervals."""
        app_handler_daily_data = self.appliances_handler.get_daily_data(day)
        shiftable_appliances_names = self.appliances_handler.shiftable_appliances_names
        daily_production_data = self.production_unit.get_daily_production_profile(day)
        daily_data = {"real": dict(), "tracking": dict()}

        # adding consumption and production data
        daily_data["real"].update({"household": list()})
        daily_data["real"]["household"] = [
            HouseholdEnergyData(
                begin=date_and_time,
                length=timedelta(minutes=30),  # FIXME will not always be 30!!
                consumption=float(value_conso),
                production=daily_production_data[date_and_time],
            )
            for date_and_time, value_conso in app_handler_daily_data["whole"].items()
        ]

        simulator_tracking_data = SimulatorTrackingData(
            day=day,
            changes_in_excess_prod=self.changes_in_excess_prod.get(
                day
            ),  # TODO no longer computed, see functions at the bottom of the file, which did it but are no longer in the class! see if need to reestablish!
            changes_in_deficit_prod=self.changes_in_deficit_prod.get(day),
        )
        shiftable_data = [
            SimulatorShiftableTrackingData(
                begin=date_time, consumption=float(value_conso), name_appliance=name
            )
            for name in shiftable_appliances_names
            for date_time, value_conso in app_handler_daily_data[name].items()
        ]
        daily_data.update(
            {
                "tracking": {
                    "aggregate": simulator_tracking_data,
                    "shiftable": shiftable_data,
                }
            }
        )

        return daily_data


# TODO below: kept just in case, but so far, no longer needed
def update_changes_in_discrepancy_production(
    self, date: date, changes_in_consumption
) -> None:  # FIXME here, or in data center (to be renamed)?
    self.changes_in_excess_prod.update({date: 0})
    self.changes_in_deficit_prod.update({date: 0})

    for name_appliance, list_conso_changes in changes_in_consumption.items():
        for conso_change_data in list_conso_changes:
            slots = conso_change_data["slots"]
            old_lwr, old_upr = slots["old"]
            new_lwr, new_upr = slots["new"]

            discrepancy_prod_before_shift = (
                self.compute_discrepancy_prod_conso_on_change_slots(
                    slots["old"],
                    slots["new"],
                    conso_change_data["old_conso"],
                    self.production_profile,
                )
            )
            discrepancy_prod_after_shift = (
                self.compute_discrepancy_prod_conso_on_change_slots(
                    slots["old"],
                    slots["new"],
                    conso_change_data["new_conso"],
                    self.production_profile,
                )
            )

            excess_reduction_by_shift = (
                discrepancy_prod_after_shift["excess"]
                - discrepancy_prod_before_shift["excess"]
            )  # FIXME beware, quantity may be not sound if moved_usages overlap! should maybe compute the global excess_prod before every shift, and after every shift!
            deficit_reduction_by_shift = (
                discrepancy_prod_after_shift["deficit"]
                - discrepancy_prod_before_shift["deficit"]
            )

            self.changes_in_excess_prod[date] += excess_reduction_by_shift
            self.changes_in_deficit_prod[date] += deficit_reduction_by_shift


def compute_discrepancy_prod_conso_on_change_slots(
    self, old_slot, new_slot, conso, production_profile
):
    old_lwr, old_upr = old_slot
    new_lwr, new_upr = new_slot

    disc_prod_old_slot = self.compute_discrepancy_production(
        conso, production_profile[old_lwr:old_upr]
    )
    disc_prod_new_slot = self.compute_discrepancy_production(
        conso, production_profile[new_lwr:new_upr]
    )

    disc_prod_conso = {
        "excess": disc_prod_old_slot["excess"] + disc_prod_new_slot["excess"],
        "deficit": disc_prod_old_slot["deficit"] + disc_prod_new_slot["deficit"],
    }

    return disc_prod_conso


def compute_discrepancy_production(self, conso, production):
    discrepancy_prod_conso = production - conso
    excess = discrepancy_prod_conso[
        discrepancy_prod_conso >= 0
    ].sum()  # TODO in visualisation, strigency of the inequality also hard-coded, improve
    deficit = discrepancy_prod_conso[discrepancy_prod_conso < 0].sum()

    disc_prod_conso = {"excess": excess, "deficit": deficit}
    return disc_prod_conso

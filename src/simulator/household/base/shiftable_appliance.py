# import modules
import pandas as pd
import numpy as np
from datetime import datetime, date, timedelta, time

from typing import List, Dict, Tuple, Optional
import logging
from pydantic import BaseModel
from itertools import chain
import copy

# custom modules
from src.real_world_classes import flexibility_potential as fp


NB_SECONDS_UPPER_EQUAL_LOWER = (
    15 * 60
)  # TODO when upper_bound == lower_bound, we arbitraritly assume the usage was 15 minutes, in order to avoid dividing by zero, make sure design is sound!
RATIO_OVERLAP_W_RED = 0.5  # TODO put in config


class UseInterval(BaseModel):
    """Use interval for a shiftable appliance."""

    lower_bound: datetime
    upper_bound: datetime

    @property
    def length(self) -> timedelta:
        delta = self.upper_bound - self.lower_bound
        total_seconds = delta.total_seconds()
        if total_seconds == 0:
            return timedelta(
                seconds=NB_SECONDS_UPPER_EQUAL_LOWER
            )  # TODO when upper_bound == lower_bound, we arbitraritly assume the usage was 15 minutes, in order to avoid dividing by zero, make sure design is sound!
        return delta


# class
class ShiftableAppliance:
    def __init__(
        self,
        energy_profile: pd.Series,
        timeline_data: dict,
        str_time_max_shifting: str,  # should be in pandas compliant format (e.g. "6H", "2D")
    ):
        self.name = energy_profile.name
        logging.debug(f"Initialising appliance {self.name}")
        self.all_use_intervals: List[UseInterval] = compute_use_intervals(
            energy_profile.loc[timeline_data["sim_begin"] : timeline_data["sim_end"]]
        )  # TODO list, or set? make sure no problems with duplicates, or other! #FIXME should be stored by date or something, to improve the algorithmic complexity in the various searches done in the module!! #TODO should maybe make sure the number of use intervals is invariant!
        self.all_use_intervals.sort(
            key=lambda use_interval: use_interval.lower_bound, reverse=False
        )
        # TODO we assume there are non overlaps in the energy profile (in the intervals computed by compute_use_intervals), but I guess this should be true? make sure, but an appliance cannot be "used twice at the same time"

        self.energy_profile = energy_profile

        # FIXME add an admissible time for shifting! for instance, 6 hours for the dish-washer, and 2 days for the washing-machine!
        self.time_max_shifting: timedelta = pd.Timedelta(
            str_time_max_shifting
        )  # timedelta and pd.Timedelta are compatible, but make sure it's fine for typing (else, convert the pd.Timedelta to a timdelta; using pd.Timedelta because timedelta cannot be constructed from str_time_max_shifting)

        self.display_initial_info(timeline_data)

    def shift_if_possible(
        self, advice: fp.FlexibilityPotential, day: date
    ) -> Optional[List]:
        # breakpoint()  #FIXME total energy diminishes as the simulation goes, problem!
        moved_usages = list()

        if advice.has_green:
            logging.debug(f"appliance {self.name} dealing with advice")

            if not advice.has_red and not advice.has_orange:
                use_intervals_potential_change = self.get_use_intervals_in_validity_period(  # FIXME potentially many more choice of intervals here, than when just looking at validity periods! however, maybe not "realistic", in the sense that it demands a lot of "thinking" from the user! maybe modify? On the other hand, using "red" advice is more specific, and therefore reduces the "thinking overload"...
                    advice.begin_validity, advice.end_validity, day
                )
                logging.debug(
                    f"use_intervals_potential_change:\n{use_intervals_potential_change}"
                )
                breakpoint()

            elif advice.has_red:
                use_intervals_potential_change = (
                    self.get_use_intervals_from_red_periods(advice.red_periods)
                )
                logging.debug(
                    f"use_intervals_potential_change:\n{use_intervals_potential_change}"
                )

            green_advice_by_day = get_advice_by_day(advice.green_periods)
            use_intervals_not_already_in_green_periods = (
                get_use_intervals_not_in_green_periods(
                    use_intervals_potential_change, green_advice_by_day
                )
            )
            logging.debug(
                f"use_intervals_not_already_in_green_periods:\n{use_intervals_not_already_in_green_periods}"
            )
            breakpoint()

            use_intervals_reco_period_matchings = compute_matchings(
                use_intervals_not_already_in_green_periods,
                green_advice_by_day,
                self.time_max_shifting,
                advice.begin_validity,
                advice.end_validity,
            )
            logging.debug(
                f"use_intervals_reco_period_matchings:\n{use_intervals_reco_period_matchings}"
            )
            breakpoint()

            new_usages_slots = (
                [  # FIXME new slots start at the beginning of the green period
                    {
                        "new": UseInterval(
                            lower_bound=reco.begin_as_datetime,
                            upper_bound=reco.begin_as_datetime + use_interval.length,
                        ),
                        "old": use_interval,
                    }
                    for (use_interval, reco) in use_intervals_reco_period_matchings
                ]
            )

            logging.debug(f"{self.name} new usage slots {day}\n{new_usages_slots}")

            breakpoint()

            return new_usages_slots

    def change_if_conform(
        self, to_move
    ) -> bool:  # FIXME awful implementation, with side effects and return value!!!!!
        if self.no_overlap(to_move["old"], to_move["new"]) and self.no_previous_conso(
            to_move["new"]
        ):  # FIXME maybe do not make the overlap check here, but only in shift_conso??
            self.all_use_intervals.remove(to_move["old"])
            self.all_use_intervals.append(to_move["new"])

            self.all_use_intervals.sort(
                key=lambda use_interval: use_interval.lower_bound, reverse=False
            )  # FIXME if we must sort each time, maybe we should use a data structure allowing quick sorted insert
            return True
        return False

    def no_overlap(
        self, old_itv: UseInterval, new_itv: UseInterval
    ) -> (
        bool
    ):  # FIXME make sure function works! not sure it does! underlying algorithm may not be correct!!
        no_overlap_taking_place = True

        all_wo_old = copy.deepcopy(self.all_use_intervals)  # FIXME costly!
        all_wo_old.remove(old_itv)
        all_wo_old.sort(
            key=lambda use_interval: use_interval.lower_bound, reverse=False
        )
        # all_wo_old should be sorted, since all_use_intervals is sorted, but make sure!

        # looking for the first interval with potential overlap, then the last, or something
        # FIXME  anyway, computations could be much improved!!!

        # looking for the first interval
        itv_to_consider = None
        num_itv_to_consider = 0
        for num_itv, use_interval in enumerate(all_wo_old):
            if (
                use_interval.upper_bound >= new_itv.lower_bound
            ):  # this "works" because all_wo_old is sorted by ascending order
                itv_to_consider = use_interval
                num_itv_to_consider = num_itv
                break

        if (
            itv_to_consider is not None
            and itv_to_consider.lower_bound <= new_itv.upper_bound
        ):
            no_overlap_taking_place = False

        return no_overlap_taking_place

    def no_previous_conso(
        self, new_itv: UseInterval
    ) -> bool:  # FIXME maybe very close from other functions!
        for itv in self.all_use_intervals:
            if (
                itv.lower_bound <= new_itv.lower_bound
                and new_itv.lower_bound < itv.upper_bound
            ):
                return False
        return True

    def get_use_intervals_in_validity_period(
        self, begin_validity: datetime, end_validity: datetime, day: date
    ) -> List[UseInterval]:  # FIXME awful algorithmic complexity!
        intervals_in_validity = list()
        day_at_midnight = datetime.combine(
            day, time(hour=0, minute=0)
        )  # FIXME checking that use_interval.lower_bound is greater than day at midnight seems to be necessary for there to be no incoherences when running the code (total consumption changes otherwise), but should make sure the whole way the shifting is done is sound!
        for use_interval in self.all_use_intervals:
            if (
                use_interval.lower_bound > day_at_midnight
                and use_interval.lower_bound > pd.Timestamp(begin_validity)
                and use_interval.upper_bound < pd.Timestamp(end_validity)
            ):
                intervals_in_validity.append(use_interval)

        return intervals_in_validity

    def get_use_intervals_from_red_periods(
        self, red_periods: List[fp.RecommandationPeriod]
    ) -> List[UseInterval]:  # FIXME awful algorithmic complexity!
        intervals_from_red_periods = list()
        for use_interval in self.all_use_intervals:
            for reco in red_periods:
                if (
                    use_interval.lower_bound >= reco.begin_as_datetime
                    and use_interval.lower_bound < reco.end_as_datetime
                ):
                    length_overlap = min(
                        use_interval.upper_bound, reco.end_as_datetime
                    ) - max(use_interval.lower_bound, reco.begin_as_datetime)
                    ratio_overlap = (
                        length_overlap.total_seconds()
                        / use_interval.length.total_seconds()
                    )
                    if ratio_overlap > RATIO_OVERLAP_W_RED:
                        intervals_from_red_periods.append(use_interval)

        return intervals_from_red_periods

    def display_initial_info(self, timeline_data: Dict[str, datetime]) -> None:
        total_power_hist = (
            self.energy_profile.loc[
                timeline_data["historical_begin"] : timeline_data["historical_end"]
            ]
            .sum()
            .sum()
        )
        total_power_sim = (
            self.energy_profile.loc[
                timeline_data["sim_begin"] : timeline_data["sim_end"]
            ]
            .sum()
            .sum()
        )
        logging.debug(
            f"iris appliance {self.name} consumption profile, total number of slots days (hist + sim): {len(self.energy_profile)} total power hist: {total_power_hist} total power sim: {total_power_sim}"
        )
        logging.debug(
            f"Init appliance {self.name}, total consumption: {self.energy_profile.sum()}, number of use intervals: {len(self.all_use_intervals)}; these are invariants."
        )


# functions
def not_in_green_period(
    green_bgn, green_end, lower_bound, upper_bound
) -> bool:  # FIXME so far, entirely out of period, could also consider overlaps
    return upper_bound.time() <= green_bgn or lower_bound.time() > green_end


def get_use_intervals_not_in_green_periods(
    to_check_use_intervals: List[UseInterval],
    green_advice_by_day: Dict[date, List[fp.RecommandationPeriod]],
) -> List[UseInterval]:  # FIXME awful algorithmic complexity!
    all_not_in_green_periods = list()

    for use_interval in to_check_use_intervals:
        is_not_in_green_period = True
        for day, all_recos_day in green_advice_by_day.items():
            for reco in all_recos_day:
                is_not_in_green_period = is_not_in_green_period and not_in_green_period(
                    reco.begin,
                    reco.end,
                    use_interval.lower_bound,
                    use_interval.upper_bound,
                )
        if is_not_in_green_period:
            all_not_in_green_periods.append(use_interval)

    return all_not_in_green_periods


def compute_matchings(
    all_use_intervals: List[UseInterval],
    green_advice_by_day: Dict[date, List[fp.RecommandationPeriod]],
    time_max_shifting: timedelta,
    begin_validity: datetime,
    end_validity: datetime,
) -> List[Tuple[UseInterval, fp.RecommandationPeriod]]:
    # FIXME in turns, improve way matching is performed!
    # FIXME make it so that only reco periods within "self.time_max_shifting" of the use intervals can be matched!
    all_matchings = list()
    all_periods = list(chain(*green_advice_by_day.values()))
    all_periods = sorted(all_periods, key=lambda record: record.strength, reverse=True)
    breakpoint()

    # logging.warning(
    #     f"when computing the matchings, should order the reco periods by relevance?"
    # )
    # logging.warning(f"should also use sorting wrt the red periods!!!")
    # logging.warning(
    #     f'probably improve the design of the matching!! in particular, why "iterating" through the recommandation periods, and why not put all use interval there? or maybe, find the matching period closest to the use interval??'
    # )

    def is_within_shiftability(
        reco, use_interval
    ):  # a use interval can only be matched to a reco period is there are close enough, in the sense that the shift is represents is admissible
        return (
            abs(reco.begin_as_datetime - use_interval.lower_bound) <= time_max_shifting
        )

    def check_validity(reco, length):  # checking validity of potential new use interval
        upr_bound = reco.begin_as_datetime + length
        return reco.begin_as_datetime > begin_validity and upr_bound < end_validity

    #    #old version
    #    for reco in all_periods:
    #        nb_matchings = len(all_matchings)
    #        if nb_matchings < len(all_use_intervals):  #each use interval can be matched to only one reco period
    #            use_interval = all_use_intervals[nb_matchings]
    #            if is_within_shiftability(reco, use_interval) and check_validity(reco, use_interval.length):
    #                all_matchings.append(
    #                    (all_use_intervals[nb_matchings], reco)
    #                )
    #        else:
    #            break

    for use_interval in all_use_intervals:
        for reco in all_periods:
            if is_within_shiftability(reco, use_interval) and check_validity(
                reco, use_interval.length
            ):
                all_matchings.append((use_interval, reco))
                break

    for use_interval, reco_period in all_matchings:
        if (
            reco_period.begin_as_datetime + use_interval.length > end_validity
            or reco_period.begin_as_datetime < begin_validity
        ):
            breakpoint()
    return all_matchings


def compute_use_intervals(energy_profile: pd.Series) -> List[UseInterval]:
    # FIXME make sure the function works correctly
    # FIXME it is probably not the case!!, if a machine has use intervals which are not continuous! for instance, a washing machine may have several cycles!!! in which case, the use intervals will be disjoint, and the extraction fails!
    use_intervals = list()
    lower_bound = None
    upper_bound = None
    last_positive_slot = None

    for (
        slot,
        conso,
    ) in energy_profile.items():  # zip(energy_profile.index, energy_profile):
        if not np.isclose(
            conso, 0
        ):  # FIXME so far, energy profile values have type int64, but it may change! and np.isclose may not be precise enough!
            if lower_bound is None:
                lower_bound = slot
            last_positive_slot = slot
        else:
            if lower_bound is not None:
                if upper_bound is None:
                    upper_bound = last_positive_slot  # slot
                    use_intervals.append(
                        UseInterval(lower_bound=lower_bound, upper_bound=upper_bound)
                    )
                    lower_bound = None
                    upper_bound = None
                    last_positive_slot = None
                else:
                    raise  # shoud not happen

    # if energy profile ends with positive values, these are missed!
    if lower_bound is not None and energy_profile[-1] > 0:
        upper_bound = energy_profile.index[
            -1
        ]  # FIXME this may cause use intervals with upper_bound == lower_bound, so that the length is 0, which may cause errors in divisions...
        if upper_bound == lower_bound:
            if np.isclose(energy_profile[-2], 0):  # should be the case, but to be sure
                lower_bound = energy_profile.index[
                    -2
                ]  # FIXME hack to fix the above, make sure does not cause problems!!!
        use_intervals.append(
            UseInterval(lower_bound=lower_bound, upper_bound=upper_bound)
        )

    #    print(use_intervals)

    # FIXME should check the use intervals are correct: no overlap, sum over them of power equals power in energy profile, for instance!
    total_power = energy_profile.sum()
    total_power_use_intervals = 0
    for record in use_intervals:
        lwr = record.lower_bound
        upr = record.upper_bound
        total_power_use_intervals += energy_profile.loc[lwr:upr].sum()
    logging.debug(
        f"init shiftable {total_power} in energy profile vs {total_power_use_intervals} in use intervals"
    )
    # if total_power != total_power_use_intervals:  #FIXME in iris, integer values, but in uk-dale, there are float values! therefore using np.isclose
    if not np.isclose(total_power, total_power_use_intervals):
        not_zero = energy_profile[energy_profile > 0]
        logging.debug(f"not zero dates:\n{not_zero}")
        breakpoint()

    # FIXME checking for overlaps
    use_intervals = sorted(use_intervals, key=lambda record: record.lower_bound)
    for idx, record in enumerate(use_intervals[:-1]):
        if record.upper_bound >= use_intervals[idx + 1].lower_bound:
            breakpoint()

    # FIXME should also check if issues with non invariance come with end of sim, as for computation of use intervals

    return use_intervals


def get_advice_by_day(all_recos: List[fp.RecommandationPeriod]):
    advice_by_day = dict()

    for reco in all_recos:
        day = reco.day
        if advice_by_day.get(day) is not None:
            advice_by_day[day].append(reco)
        else:
            advice_by_day.update({day: [reco]})

    return advice_by_day

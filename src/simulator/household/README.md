# Household
This contains classes which simulate the behaviour of an household.

It contains
- the **base directory**, which specifies the interface of an household, as well as implements some common functionalities;
- specific **"derived" directories**, instantiating households from various datasets, or models (e.g. iris);
- the **advice handler** module, where human behaviour wrt. nudges model logic should be implemented.

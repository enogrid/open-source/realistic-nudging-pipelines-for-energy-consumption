import datetime
import logging

from src.real_world_classes import flexibility_potential as fp


class AdviceHandler:
    """Gathers logic about advice handling (acceptance, etc.)."""

    def __init__(self, days_between_advice: int):
        self.days_between_advice = days_between_advice
        self.date_last_advice = None

    def accept_advice(self, date: datetime.date, advice: fp.FlexibilityPotential):
        # logging.warning(
        #     f"Make sure the advice handler does not refuse advice when it should not!"
        # )
        if (
            self.date_last_advice is None
            or (date - self.date_last_advice).days >= self.days_between_advice
        ):
            self.date_last_advice = date
            return True
        return False

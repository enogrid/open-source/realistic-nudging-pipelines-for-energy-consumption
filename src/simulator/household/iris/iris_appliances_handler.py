from typing import List, Dict, Union
from datetime import datetime, timedelta, date, time
import pandas as pd
import numpy as np
import logging

from src.real_world_classes.data_collection.data_models import HouseholdEnergyData
from src.simulator.household.base import base_appliances_handler as bah
from src.simulator.household.base import shiftable_appliance as sa


class IrisAppliancesHandler(bah.BaseAppliancesHandler):
    """Specific to Iris dataset."""

    def __init__(
        self,
        shiftable_appliances_info: Dict[str, str],
        path_to_datafile: str,
        timeline_data: dict,
    ):
        super().__init__()
        self.sim_power_profile: pd.DataFrame = None
        self.hist_power_profile: pd.DataFrame = None
        self.all_appliances_names: List[str] = None
        self.energy_profile: pd.DataFrame = None
        self.shiftable_energy_profile: pd.DataFrame = None
        self.energy_profile: pd.DataFrame = None
        self.shiftable_appliances_names: List[str] = list(
            shiftable_appliances_info.keys()
        )
        self.generate_energy_consumption_profile(
            path_to_datafile, timeline_data
        )  # FIXME these attributes are used directly in the base class, should probably improve!
        self.shiftable_appliances = self.init_shiftable_appliances(
            timeline_data, shiftable_appliances_info
        )
        self.analyse_shiftable_potential()  # implemented in base

        # FIXME for debugging
        conso = self.sim_power_profile.sum(axis="columns")
        shift = self.sim_power_profile[self.shiftable_appliances_names].sum(
            axis="columns"
        )
        if len(conso[conso < shift]) > 0:
            breakpoint()

    def generate_energy_consumption_profile(
        self, path_to_datafile: str, timeline_data: dict
    ) -> None:
        household_energy_profile = pd.read_csv(
            path_to_datafile, index_col=0, parse_dates=True
        )

        household_energy_profile = household_energy_profile[
            ~household_energy_profile.index.duplicated(keep="first")
        ]

        # to "reverse" consumption, in the sense that consumption in middle of the day is exchanged with that in the morning, and in the evenings
        household_energy_profile.index = (
            household_energy_profile.index.to_series()
        )  # .apply(reverse_times)
        household_energy_profile.sort_index(inplace=True)

        household_energy_profile = household_energy_profile.loc[
            timeline_data["historical_begin"] : timeline_data["sim_end"], :
        ]
        sim_power_profile = household_energy_profile.loc[
            timeline_data["sim_begin"] : timeline_data["sim_end"], :
        ]
        hist_power_profile = household_energy_profile.loc[
            timeline_data["historical_begin"] : timeline_data["historical_end"], :
        ]
        all_appliances_names = list(household_energy_profile.columns)

        logging.debug("Iris consumption profile generated")
        self.hist_power_profile = hist_power_profile
        self.sim_power_profile = sim_power_profile
        self.all_appliances_names = all_appliances_names
        self.shiftable_energy_profile = household_energy_profile[
            self.shiftable_appliances_names
        ]
        self.energy_profile = household_energy_profile

    def get_historical_data(
        self, begin: datetime, end: datetime
    ) -> List[HouseholdEnergyData]:
        whole_conso = self.hist_power_profile.loc[
            begin:end, self.all_appliances_names
        ].sum(axis="columns")
        historical_data = [
            HouseholdEnergyData(
                begin=date_and_time,
                length=timedelta(minutes=30),
                consumption=float(value_conso),
                production=0,
            )
            for date_and_time, value_conso in whole_conso.items()
        ]
        return historical_data

    def init_shiftable_appliances(
        self, timeline_data: dict, shiftable_appliances_info: Dict[str, str]
    ) -> Dict[str, sa.ShiftableAppliance]:
        shiftable_appliances = {
            name: sa.ShiftableAppliance(
                # self.energy_profile.loc[:, name],
                self.sim_power_profile.loc[:, name],
                timeline_data,
                str_time_max_shifting,
            )
            for name, str_time_max_shifting in shiftable_appliances_info.items()
        }
        return shiftable_appliances

    def update_daily_energy_profile(
        self, date: date
    ) -> float:  # FIXME float sent back for debug
        """Computes and stores the daily consumption."""
        # selecting the values of the day
        is_right_day = self.sim_power_profile.index.to_series().apply(
            lambda date_and_time: date_and_time.date() == date
        )
        day_profile = self.sim_power_profile[is_right_day]
        self.daily_energy_profile[date] = day_profile.copy(deep=True)

        # for tracking
        day = datetime(year=date.year, month=date.month, day=date.day)
        day_begin = datetime.combine(day, time(hour=0, minute=0))
        day_end = datetime.combine(day, time(hour=23, minute=30))
        total_conso_df = self.get_conso(day_begin, day_end, "whole").sum()
        logging.debug(f"total conso df for {date}: {total_conso_df}")

        return total_conso_df

    def get_conso(
        self,
        conso_from: datetime,
        conso_to: datetime,
        name_appliance: str,
        get_series: bool = False,
    ) -> (
        pd.Series
    ):  # FIXME starting to overhaul the whole energy load curves management system, as so far, cannot understand the non invariant issue  #FIXME return type is false, and in all functions duplicated from this one!  #the get_series parameter is a hack, to get either the series, or the float, improve! should modify the calls to the function accordingly, when improvements done!
        """
        Bounds included.

        Can only be used for sim data (not for historical data).
        If appliances == "whole": sum along all appliances
        """
        if name_appliance == "whole":
            return self.sim_power_profile.loc[
                conso_from:conso_to, self.all_appliances_names
            ].sum(
                axis="columns"
            )  # FIXME in turns, replace energy profile by sim_energy_profile, and make another profile for historical data
        else:
            conso = self.sim_power_profile.loc[
                conso_from:conso_to, name_appliance
            ]  # .sum()  #TODO why no axis in the sum, as for 'whole'?
            if get_series:
                return conso
            return conso.sum()

    def set_conso(
        self,
        conso_from: datetime,
        conso_to: datetime,
        name_appliance: str,
        values: Union[pd.Series, float],
    ) -> None:
        if not isinstance(values, float) and len(
            self.sim_power_profile.loc[conso_from:conso_to, :]
        ) != len(values):
            breakpoint()
        self.sim_power_profile.loc[
            conso_from:conso_to, name_appliance
        ] = values  # FIXME make sure broadcasting or stg works when values is a float #FIXME what happens when sizes mismatch?

    def shift_conso(
        self,
        old_from: datetime,
        old_to: datetime,
        new_from: datetime,
        new_to: datetime,
        name: str,  # FIXME should be an appliance name
    ) -> None:
        saved_values = np.copy(self.sim_power_profile.loc[old_from:old_to, name])
        self.set_conso(new_from, new_to, name, saved_values)
        self.set_conso(
            old_from, old_to, name, 0.0
        )  # 0. here, because the conso on new_from:new_to should be 0 (else, no sense in moving consos!) so, no "true exchange", but "true exchange when assumption of 0 conso is satisfied" (and if not satisfied, will break things in the code, or bring incoherent results - like non invariant consumptions!)

    def check_power_use_intervals(self, name, shiftable):
        total_power = self.sim_power_profile.loc[:, name].sum().sum()
        total_power_use_intervals = 0
        for record in shiftable.all_use_intervals:
            lwr = record.lower_bound
            upr = record.upper_bound
            total_power_use_intervals += self.get_conso(lwr, upr, name).sum()
        logging.debug(
            f"{total_power} in energy profile vs {total_power_use_intervals} in use intervals"
        )
        if total_power != total_power_use_intervals:
            not_zero = self.sim_power_profile.loc[:, name][
                self.sim_power_profile.loc[:, name] > 0
            ]
            logging.debug(f"not zero dates:\n{not_zero}")
            breakpoint()

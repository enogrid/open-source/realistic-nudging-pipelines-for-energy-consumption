# import modules

from typing import List, Dict
from datetime import datetime

# custom modules
from src.real_world_classes.data_collection.data_models import HouseholdEnergyData
from src.simulator import external_world as ew
from src.simulator.household.iris import iris_appliances_handler as iaph

# from src.simulator.household.base import base_household as bh
from src.simulator.household.base import BaseHousehold, ProductionUnit


COEFF_REDUCTION_PROD = 1.0  # 10.


class IrisHousehold(BaseHousehold):
    """Specific to iris dataset."""

    def __init__(
        self,
        days_between_advice: int,
        shiftable_appliances_info: Dict[str, str],
        path_to_datafile: str,
        production_unit: ProductionUnit,
        timeline_data: dict,
    ):  # FIXME reorganise the interaction between init and the factory functions, so far poor!
        super().__init__(days_between_advice)

        self.appliances_handler = iaph.IrisAppliancesHandler(
            shiftable_appliances_info, path_to_datafile, timeline_data
        )
        self.production_unit = production_unit

    @classmethod
    def generate_household(
        cls,
        cfg: dict,
        external_world: ew.ExternalWorld,
        timeline_data: dict,
    ) -> (
        BaseHousehold
    ):  # TODO "should be" the derived type IrisHousehold, but not yet defined, and maybe better to have all the derived methods be signed with the base type?
        """Generates the household, together with all the data needed to make it run."""
        production_unit = ProductionUnit.generate(
            cfg, external_world, timeline_data
        )  # TODO in turns, improve; there should probably be a class which deals with production
        household = cls(
            cfg["days_between_advice"],
            cfg["shiftable"],
            cfg["path"],
            production_unit,
            timeline_data,
        )
        return household

    def get_historical_data(
        self, begin: datetime, end: datetime
    ) -> List[HouseholdEnergyData]:
        historical_data = self.appliances_handler.get_historical_data(begin, end)
        for record in historical_data:
            record.production = self.production_unit.production_profile[record.begin]
        return historical_data

# import modules
from datetime import timedelta, datetime
from dateutil import parser
import pandas as pd
import logging
from typing import Optional, Dict

# custom modules
from src.simulator.household.base import base_household as hs
from src.simulator.household import IrisHousehold
from src.simulator import external_world as ew
from src.real_world_classes.data_collection import data_center as dc
from src.real_world_classes.advisers import base_adviser as ad

from src.utils import util_functions as uf


NUM_SUNDAY_IN_PANDAS = 6  # for pd.Timestamp dayofweek attribute


# class
class Simulator:
    """Controls the whole simulation process."""  # FIXME so far, kind of a hybrid functionality class: controls the simulation process, and represents also the various interfaces giving access to external world data, household data, and so on. Improve.

    def __init__(
        self,
        timeline_data: dict,
        length_simulation: int,
        external_world: ew.ExternalWorld,
        household: hs.BaseHousehold,
    ):
        # variables to control the simulation
        self.length = length_simulation

        # variables more akin to "real world variables/interfaces/APIs" or something
        self.historical_begin: datetime.datetime = timeline_data["historical_begin"]
        self.historical_end: datetime.datetime = timeline_data["historical_end"]
        self.sim_begin: datetime.datetime = timeline_data["sim_begin"]
        self.sim_end: datetime.datetime = timeline_data["sim_end"]

        self.external_world = external_world
        self.household = household

        # tracking variables
        self.conso_by_week = dict()
        self.changes_conso = list()
        self.running_adviser: str = None
        self.beginning_running_period = self.sim_begin

        self.analyse_consumption()

    @property
    def date_begin(
        self,
    ):  # FIXME this, and data_end below: keep, or remove, and just use the attribute?
        return self.sim_begin

    @property
    def date_end(self):
        return (
            self.sim_end
        )  # FIXME make sure that midnight, the day before/after is dealt with properly!

    def get_historical_data(self):
        historical_data = {
            "household": self.household.get_historical_data(
                self.historical_begin, self.historical_end
            ),
            "external_world": self.external_world.get_historical_data(
                self.historical_begin, self.historical_end
            ),
        }
        return historical_data

    def scheduler_requests_advice(self, day: datetime.date) -> bool:
        # FIXME make a (better) function, or a class, "Scheduler", which calls the adviser for an advise whenever needed (e.g. every sunday)
        return pd.Timestamp(day).dayofweek == NUM_SUNDAY_IN_PANDAS

    def run_simulation(self, data_center: dc.DataCenter, advisor: ad.Adviser):
        print(f"Starting simulation with {advisor.name}")
        self.display_init_conso_info()

        self.running_adviser = advisor.name  # FIXME only for debugging!
        advice = None  # FIXME to initialise, but probably bad design!
        data_center.collect_general_household_information(self.household)
        tracking_total_conso = 0
        total_sim_conso_beginning = self.compute_total_sim_consumption()

        for delta_days in range(self.length + 1):  # FIXME make sure +1 is fine
            day = (self.sim_begin + timedelta(days=delta_days)).date()

            day_tracking_data = self.household.elapse_day(day, advice)
            total_conso_day_elapse = day_tracking_data["total_conso_day"]
            self.record_changes_conso(
                day, day_tracking_data.get("changes_in_consumption")
            )

            daily_household_data = self.household.get_daily_data(day)

            data_center.collect_daily_household_data(day, daily_household_data)
            daily_external_world_data = self.external_world.get_daily_data(day)
            data_center.collect_daily_external_world_data(
                day, daily_external_world_data
            )

            advisor.collect_data(day, data_center)

            # FIXME in turns, reestablish probably
            if self.scheduler_requests_advice(day):  # day != self.sim_end.date():
                advice = advisor.advise(
                    day, data_center
                )  # FIXME should the advice be emitted only every 7 days (say), or should it belong to the household to discard advice coming too close to each other?
                self.record_consumption_week(day)
            else:
                advice = None

            # FIXME hack because, when reaching the last day, the weather adviser gets no sunshine_coeff prediction, and an error occcurs; I thought it better to put the hack in the simulator part, than in the real algorithm.
            if (
                advice is not None and not advice.is_void()
            ):  # FIXME should precise what is sim_end: the day at 23:30, or the previous day at 23:30??
                advice.end_validity = min(
                    advice.end_validity, self.date_end
                )  # making sure end of validity is before the end of the simulation. This is dealt with in the simulator, not in the flexibility potential, because this notion does not exist in "the real world"!

        # adding final days of conso?
        # total_conso_week = self.compute_week_consumption(day)
        self.record_consumption_final_days(day)
        self.display_end_of_simulation_info()

        sim_tracking_data = {
            "conso_by_week": self.conso_by_week,
            "changes_conso": self.changes_conso,
        }

        if advisor.name == "CombinedController":  # FIXME improve implementation
            sim_tracking_data.update({"predicted_excess": advisor.predicted_excess})

        return sim_tracking_data  # self.conso_by_week  #FIXME for debugging

    # FIXME functions below: tracking functions
    # should probably be moved, and implementation should be improved!!

    def record_consumption_final_days(self, day: datetime.date) -> None:
        total_conso_last_days = self.household.appliances_handler.get_conso(
            self.beginning_running_period, uf.get_date_at_2330(day), "whole"
        ).sum()
        week_number = (
            list(self.conso_by_week.keys())[-1] + 1
        )  # FIXME not sure why the following would erase the last key #int((day - self.sim_begin.date()).days / 7)
        logging.debug(
            f"final days conso week {week_number}: {total_conso_last_days}; should NOT vary between advisers"
        )
        self.conso_by_week[week_number] = total_conso_last_days  # FIXME only for debug!
        self.conso_by_week["total"] = sum(self.conso_by_week.values())

    def record_consumption_week(self, day: datetime.date) -> None:
        total_conso_week = self.compute_week_consumption(day)
        week_number = int((day - self.sim_begin.date()).days / 7)
        logging.debug(
            f"conso week {week_number}: {total_conso_week}; should NOT vary between advisers"
        )
        self.conso_by_week[week_number] = total_conso_week  # FIXME only for debug!

    def record_changes_conso(
        self, day: datetime.date, changes_conso: Optional[Dict]
    ) -> None:
        # total_conso_week = self.compute_week_consumption(day)
        week_number = int((day - self.sim_begin.date()).days / 7)
        # logging.debug(f'conso week {week_number}: {total_conso_week}; should NOT vary between advisers')
        # FIXME should track much more precisely the changes performed!
        # FIXME compute the total changes performed over the whole simulation, by adviser, for instance! and maybe by appliance?
        if changes_conso is not None:
            total_changes_performed_w_advice = sum(
                sum(change["new_conso"] for change in data_app)
                for data_app in changes_conso.values()
            )
            logging.debug(
                f"{day}: total changes in consumption: {total_changes_performed_w_advice}"
            )
            self.changes_conso.append(
                total_changes_performed_w_advice
            )  # FIXME only for debug!

    def display_end_of_simulation_info(self) -> None:
        logging.debug("Simulation elapsed successfully")
        logging.debug(
            f"total sim conso at end of simulation with {self.running_adviser}: {self.compute_total_sim_consumption()}"
        )
        logging.debug(f"conso by week by adviser:\n{self.conso_by_week}")
        # logging.warning(f"should compute the total consumption shifted!!")

    def display_init_conso_info(self) -> None:
        logging.debug(
            f"The simulation starts on {self.date_begin}, and will last for {self.length} days."
        )
        logging.debug(
            f"total overall consumption at beginning of simulation: {self.compute_total_overall_consumption()}"
        )
        logging.debug(
            f"total historical consumption at beginning of simulation: {self.compute_total_hist_consumption()}"
        )
        logging.debug(
            f"total sim conso at beginning of simulation: {self.compute_total_sim_consumption()}"
        )

    def compute_total_hist_consumption(self) -> int:
        #        total_hist_conso = self.household.appliances_handler.energy_profile.loc[self.historical_begin:self.historical_end, :].sum().sum()
        #        return total_hist_conso
        return -1  # FIXME temporary

    def compute_total_sim_consumption(self) -> int:
        # total_sim_conso = self.household.appliances_handler.energy_profile.loc[self.sim_begin:self.sim_end, :].sum().sum()
        total_sim_conso = self.household.appliances_handler.get_conso(
            self.sim_begin, self.sim_end, "whole"
        )
        return (
            total_sim_conso.sum()
        )  # FIXME let the sum here, or move it in the appliances handler? in order to move as much logic concerned with handling of load curves inside the class as possible?

    def compute_total_overall_consumption(self) -> int:
        #        total_overall_conso = self.household.appliances_handler.energy_profile.sum().sum()
        #        return total_overall_conso
        return -1  # FIXME temporary!

    def compute_week_consumption(self, day: datetime.date) -> int:
        end_week = uf.get_date_at_2330(day)
        begin_week = uf.get_date_at_midnight(day) - timedelta(
            days=7 - 1
        )  # FIXME not sure about the exact reason for the "-1", but incoherent begin/end without it! #+ timedelta(minutes=30)
        begin_week = max(
            begin_week, self.sim_begin
        )  # FIXME to deal with the first few days! moreover, if not using this, then I guess begin_week should be outside the range of sim_power_profile in appliances_handler, it seems pandas is not throwing exceptions? understand! pandas may be discarding earlier dates, thus obtaining the equivalent of what we do with the max, but make sure!
        total_week_conso = self.household.appliances_handler.get_conso(
            begin_week, end_week, "whole"
        )  # FIXME what guarantees that begin_week is necessarily after sim_begin?
        self.beginning_running_period = end_week + timedelta(
            minutes=30
        )  # FIXME for debugging
        return total_week_conso.sum()

    def analyse_consumption(self) -> None:
        conso = self.household.appliances_handler.get_conso(
            self.sim_begin, self.sim_end, "whole"
        )
        prod = self.household.production_unit.get_prod(self.sim_begin, self.sim_end)
        diff = prod - conso
        excess = prod[diff > 0]
        deficit = prod[diff < 0]

        total_prod = prod.sum()
        total_conso = conso.sum()

        pc_excess = excess.sum() / total_prod * 100
        pc_deficit = deficit.sum() / total_prod * 100

        shiftable_conso = pd.Series(
            0.0, index=conso.index
        )  # to get a series with the right length and the right column numbers
        for name_appliance in self.household.appliances_handler.all_appliances_names:
            shiftable_conso += self.household.appliances_handler.get_conso(
                self.sim_begin, self.sim_end, name_appliance, get_series=True
            )

        shiftable_in_deficit_times = shiftable_conso[diff < 0]
        total_shiftable_in_deficit = shiftable_in_deficit_times.sum()
        pc_shiftable_deficit_wrt_prod = total_shiftable_in_deficit / total_prod * 100

        logging.debug(
            f"WARNING make sure computations correct\ntotal prod: {total_prod}, total conso: {total_conso}\nexcess: {excess.sum()}, pc excess: {pc_excess}\ndeficit: {deficit.sum()}, pc deficit: {pc_deficit}"
        )
        logging.debug(
            f"total shiftable in deficit: {total_shiftable_in_deficit}, pc shiftable deficit wrt prod: {pc_shiftable_deficit_wrt_prod}"
        )


def make_simulator(cfg: dict) -> Simulator:
    """
    Choices for temporal bounds for simulation.

    All bounds are included.
    Begin bounds are at midnight.
    End bounds are at 23:30.
    """
    historical_begin = parser.parse(
        cfg["timeline"]["historical_data"]["begin"]
    )  # .date()
    # TODO for iris, historical begin is the 3rd, February as data for the 2nd only starts at 01:30
    historical_end = parser.parse(cfg["timeline"]["historical_data"]["end"])  # .date()
    # historical_end += timedelta(hours=23, minutes=30)  #FIXME works because the sampling step used is 30! improve!

    historical_begin = uf.get_date_at_midnight(historical_begin)
    historical_end = uf.get_date_at_2330(historical_end)

    sim_begin = parser.parse(cfg["timeline"]["begin_simulation"])  # .date()
    length = cfg["timeline"]["length"]
    sim_end = (
        sim_begin + timedelta(days=length) + timedelta(hours=23, minutes=30)
    )  # FIXME ad-hoc, improve!
    sim_begin = uf.get_date_at_midnight(sim_begin)
    sim_end = uf.get_date_at_2330(sim_end)

    timeline_data = {
        "sim_begin": sim_begin,
        "sim_end": sim_end,
        "historical_begin": historical_begin,
        "historical_end": historical_end,
    }
    logging.debug(f"timeline data:\n{timeline_data}")

    # initialisation
    external_world = ew.generate_external_world(cfg["external_world"])
    external_world.construct_simulated_sunshine_coefficient(historical_begin, sim_end)
    household = IrisHousehold.generate_household(
        cfg["household"], external_world, timeline_data
    )
    # external_world.reverse_estimate_sunshine_coeff(household.production_unit.production_profile)  #FIXME we reverse estimate the sunshine coefficient thanks to the production curve, but this is ad-hoc, should probably improve!
    # assert False, f'{external_world.sunshine_coefficient}'
    external_world.import_smoothed_weather_data(cfg["external_world"])
    return Simulator(timeline_data, length, external_world, household)

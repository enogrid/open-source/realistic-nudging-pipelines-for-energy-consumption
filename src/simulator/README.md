# Simulated part

This sub-directory contains all classes responsible for emulating an household consuming energy, getting energy from a production unit (e.g. photovoltaic pannels), receiving nudges from an adviser, an modifying its consumption from these nudges. It also simulated all things related to the weather (the amount of sunshine, for instance).

# Simulator class
The `Simulator` class, in the module `simulation_manager`, controls simulations.

## Set-up
It is set-up in the `make_simulator` factory function. This takes as input a dictionary of config. This config contains:
- parameters specific to the simulation flow (length of simulation, and so on);
- parameters needed for the several classes needed by the simulator (e.g. the household).

The function creates the household and the external world classes, and computes relevant values for the simulation flow, then instantiates the Simulator. 

## Workings
The main method is `run_simulation'. It takes as arguments the data center [maybe this could become an attribute of the simulator, as the household and the external world], and an adviser. Then, for each day of the simulation, it represents the fact the day elapses.

For each day:
1. the household class makes the day elapse for the household;
2. it sends back data, which the simulator sends to the data center (this represents data collection);
3. data from the external world class are likewise sent to the data center;
4. the adviser gives its advice.

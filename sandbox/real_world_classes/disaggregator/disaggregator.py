from src.real_world_classes.disaggregator.nilmtk.seq2point import Seq2Point

import numpy as np
import pandas as pd
from typing import List


class Disaggregator:
    """Based on Seq2Point."""

    def __init__(self, cfg: dict):
        self.classifiers = {
            "Seq2Point": Seq2Point({"n_epochs": 50, "batch_size": 1200})
        }  # FIXME parameters: should be in the config file
        self.train_appliances = []  # for the paramètres of classificator

        self.all_shiftable_appliances_names = None  # cfg['household']["shiftable"]

        self.output_rep = None
        self.models_filepaths = None
        self.shifted_energy_profile = []

    @classmethod
    def make_disaggregator(cls, cfg):
        disaggregator = cls(cfg)
        return disaggregator

    def set_general_information(self, cfg: dict, shiftable_appliances_names: List[str]):
        self.output_rep = cfg["output_rep"]
        self.all_shiftable_appliances_names = [
            name.replace(" ", "_")
            for name in shiftable_appliances_names  # FIXME handle properly the issue with the spaces in the names! present at multiple locations in the code!
        ]
        self.models_filepaths = {
            shiftable_appliance_name: self.output_rep + shiftable_appliance_name + ".h5"
            for shiftable_appliance_name in self.all_shiftable_appliances_names
        }

    def power_disaggregation(
        self, energy_to_disaggregate: List, appliances_historical_data: List
    ):
        for clf_name, clf in self.classifiers.items():
            clf.set_appliance_params(appliances_historical_data)

            for app_name, app_model_filename in self.models_filepaths.items():
                clf.models[app_name] = clf.return_network()
                model = clf.models[app_name]
                model.load_weights(app_model_filename)
                test_predictions = self.call_predict_iris(
                    clf, model, energy_to_disaggregate, app_name
                )
                #                elif app == "washing machine":
                #                    model.load_weights(self.cfg['disaggregator']['model']['washing_machine_file_path'])
                #                    test_predictions = self.call_predict_iris(clf, model, energy_to_disaggregate, app)

                self.shifted_energy_profile.append(test_predictions[0][1])

    def perfect_power_disaggregation(
        self, energy_to_disaggregate: List, appliances_historical_data: List
    ):
        index = energy_to_disaggregate[0].index.to_series()

        for data in appliances_historical_data:
            test_predictions = data[1][0][index].values.tolist()
            self.shifted_energy_profile.append(test_predictions)

    def call_predict_iris(self, clf, model, energy_to_disaggregate, app):
        test_main_list = clf.call_preprocessing(
            energy_to_disaggregate, submeters_lst=None, method="test"
        )
        test_predictions = []
        for test_main in test_main_list:
            test_main = test_main.values
            test_main = test_main.reshape((-1, clf.sequence_length, 1))

            prediction = model.predict(test_main, batch_size=clf.batch_size)
            prediction = (
                clf.appliance_params[app]["mean"]
                + prediction * clf.appliance_params[app]["std"]
            )
            valid_predictions = prediction.flatten()
            valid_predictions = np.where(valid_predictions > 0, valid_predictions, 0)
            df = pd.Series(valid_predictions)
            # disggregation_dict['shiftable'] = df
            # results = pd.DataFrame(disggregation_dict, dtype='float32')
            test_predictions.append((app, list(df.values)))

        return test_predictions

from . import Disaggregator, PerioOccaDisaggregator

DICT_DISAGGREGATORS = {
    "from_seq2point": Disaggregator,
    "perio_occas": PerioOccaDisaggregator,
}

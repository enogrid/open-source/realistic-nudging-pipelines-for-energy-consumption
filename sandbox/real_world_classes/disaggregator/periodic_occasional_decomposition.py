# import modules
import pandas as pd
from typing import List
from src.real_world_classes.data_collection.data_models import (
    HouseholdEnergyData,
    DisaggregationData,
)


# class
class PerioOccaDisaggregator:
    """
    Dissagregates consumtion.

    In the case of a decomposition: consumption = periodic + occasional
    """

    def __init__(self):
        self.historical_data_as_dataframe = pd.DataFrame()

        self.data = list()
        print("Periodic-occasional disaggregator initialised.")

    @classmethod
    def make_disaggregator(cls, cfg: dict):
        return cls()

    def set_historical_data(self, data: List[HouseholdEnergyData]):
        all_datetimes = [record.begin for record in data]
        index = pd.DatetimeIndex(all_datetimes)
        df_hist = pd.DataFrame(0, index=index, columns=["consumption"])
        df_hist.sort_index(ascending=True, inplace=True)

        for record in data:
            df_hist.loc[record.begin, "consumption"] = record.consumption

        self.historical_data_as_dataframe = df_hist

    def set_daily_data(self, day, data):
        if data is not None:
            self.data.extend(data)

    def disaggregate(self) -> List[DisaggregationData]:
        # FIXME implement function!
        return []

    def disaggregate_historical_data(self) -> List[DisaggregationData]:
        return []

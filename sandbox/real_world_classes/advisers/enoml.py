from datetime import date
from src.real_world_classes.advisers import base_adviser as ad
from src.real_world_classes import flexibility_potential as fp
from src.real_world_classes.data_collection import data_center as dc
from src.real_world_classes.predictors import predictor as pred
from src.real_world_classes.disaggregator import disaggregator as ds
from src.real_world_classes.advisers import weather_controller as exps


# class
class EnoML(ad.Adviser):
    def __init__(
        self, cfg: dict, predictor: pred.Predictor, disaggregator: ds.Disaggregator
    ):
        super().__init__()

        self.name = "EnoML"
        self.threshold_green = cfg["threshold_green"]
        self.power_active_threshold = cfg["power_active_threshold"]
        self.prediction_days_ahead = cfg["predictor"][
            "prediction_days_ahead"
        ]  # nb of days ahead requested for weather forecasts
        print("EnoML adviser initialised")

        self.cfg = cfg  # FIXME relevant data should be stored here, I think, and the cfg dict should not be stored entirely

        self.predictor = predictor
        self.disaggregator = disaggregator

    @classmethod
    def make_adviser(cls, cfg: dict) -> ad.Adviser:
        predictor = pred.make_predictor(cfg)
        disaggregator = ds.make_disaggregator(cfg)
        return cls(cfg, predictor, disaggregator)

    def set_historical_data(self, data_center: dc.DataCenter):
        self.predictor.set_historical_data(
            data_center.whole_energy_profile, data_center.household_production
        )

    def set_general_information(self, config: dict, data_center: dc.DataCenter):
        self.disaggregator.set_general_information(
            config, data_center.all_shiftable_appliances_names
        )

    def advise(self, day: date, data_center: dc.DataCenter) -> fp.FlexibilityPotential:
        """Computes, then ouputs the advise, as a FlexibilityPotential object."""
        test_mains = self.predictor.get_consumption_predict(
            day, self.prediction_days_ahead
        )
        train_submeters = data_center.shiftable_appliances_historical_data
        self.disaggregator.perfect_power_disaggregation([test_mains], train_submeters)
        power_disaggregated = self.disaggregator.shifted_energy_profile

        predicted_sunshine_coeff = data_center.get_weather_forecast(
            day, data_type="sunshine_coeff", day_ahead=self.prediction_days_ahead
        )
        # print(predicted_sunshine_coeff)
        # assert False

        fl_pot = exps.create_flex_pot_from_sunshine(
            predicted_sunshine_coeff, self.threshold_green
        )

        shiftable_energy = []
        # if returning an empty FlexibilityPotential, will behave as if there is no adviser

        for data in power_disaggregated:
            shiftable_energy.append(data[1])

        if sum(shiftable_energy) > 0:
            return fl_pot  # fp.FlexibilityPotential()
        return fp.FlexibilityPotential()

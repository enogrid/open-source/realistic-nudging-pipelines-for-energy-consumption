# import modules
import matplotlib.pyplot as plt

# import matplotlib.dates as mdates
# import matplotlib.ticker as ticker

# import seaborn as sns
import random


# custom modules

# class


class PlotHandler:
    def __init__(self, output_rep):
        self.output_rep = output_rep

    def plot_df(self, df, name_plot=None):
        fig, ax = plt.subplots()
        df.plot(ax=ax)

        if name_plot is None:
            name_plot = "statistics_from_df"

        plt.title(f"{name_plot}")
        filename = self.output_rep + name_plot.replace(" ", "_") + ".png"

        plt.savefig(filename)

    def plot_bar_df(self, df, name_plot=None):
        fig, ax = plt.subplots()

        df.plot(ax=ax, kind="bar")

        if name_plot is None:
            name_plot = "statistics_from_df"

        plt.setp(ax.get_xticklabels(), rotation=30, ha="right")

        plt.title(f"{name_plot}")

        filename = self.output_rep + name_plot.replace(" ", "_") + ".png"
        plt.savefig(filename)

    #    def plot_sns_bar_df(self, df, name_plot=None):
    #        fig, ax = plt.subplots()
    #
    #        sns.barplot(x='usage_point', y='ratio_csc_to_c', hue='is_producer', data=df)
    #
    #        if name_plot is None:
    #            name_plot = 'statistics_from_df'
    #
    #        plt.setp(ax.get_xticklabels(), rotation=30, ha="right")
    #
    #        plt.title(f'{name_plot}')
    #
    #        filename = self.output_rep + 'sns_' + name_plot.replace(' ', '_') + '.png'
    #        plt.savefig(filename)

    def plot_mean_daily_ratio_csc_to_c_through_time(self, df, name_plot=None):
        fig, ax = plt.subplots()

        df.plot(ax=ax, legend=False)

        if name_plot is None:
            name_plot = "statistics_from_df"

        plt.setp(ax.get_xticklabels(), rotation=30, ha="right")

        plt.title(f"{name_plot}")

        filename = self.output_rep + name_plot.replace(" ", "_") + ".png"
        plt.savefig(filename)

    def plot_hist_errors_on_energy_indexes(self, type_payment, df, name_plot=None):
        fig, ax = plt.subplots()

        df.plot(ax=ax, kind="hist", legend=False)

        if name_plot is None:
            name_plot = type_payment + "_errors_on_energy_indexes"

        plt.setp(ax.get_xticklabels(), rotation=30, ha="right")

        plt.title(f"{name_plot}")

        filename = self.output_rep + name_plot.replace(" ", "_") + ".png"
        plt.savefig(filename)

    def plot_hist_relative_errors_on_energy_indexes(
        self, type_payment, df, name_plot=None
    ):
        fig, ax = plt.subplots()

        df.plot(ax=ax, kind="hist", legend=False)

        if name_plot is None:
            name_plot = type_payment + "_relative_errors_on_energy_indexes"

        plt.setp(ax.get_xticklabels(), rotation=30, ha="right")

        plt.title(f"{name_plot}")

        filename = self.output_rep + name_plot.replace(" ", "_") + ".png"
        plt.savefig(filename)

    def plot_relative_errors_index_vs_conso_totale(
        self, type_payment, df, name_plot=None
    ):
        fig, ax = plt.subplots()

        ax.scatter(
            x=df["sum_conso"], y=df["rel_error_index"] * 100
        )  # *100 : to get percentages
        ax.set_xlabel("Consommation totale (Wh)")
        ax.set_ylabel("% d'erreur sur l'index")

        if name_plot is None:
            name_plot = (
                type_payment + "_relative_errors_on_energy_indexes_vs_total_conso"
            )

        plt.setp(ax.get_xticklabels(), rotation=30, ha="right")

        plt.title(f"{name_plot}")

        filename = self.output_rep + name_plot.replace(" ", "_") + ".png"
        plt.savefig(filename)

    def plot_errors_for_pdl(self, type_payment, df, name_plot=None, use_all=False):
        fig, ax = plt.subplots()

        if not use_all:
            all_pdls = df["pdl"].unique()
            pdl = random.choice(all_pdls)

            df = df[df["pdl"] == pdl]

        ax.scatter(
            x=df["begin_period"], y=df["rel_error_index"] * 100
        )  # *100 : to get percentages
        ax.set_xlabel("Début de la période de calcul d'index")
        ax.set_ylabel("% d'erreur sur l'index")

        if name_plot is None:
            if not use_all:
                name_plot = type_payment + "_errors_for_pdl_" + pdl
            else:
                name_plot = type_payment + "_errors_for_all_pdls"

        plt.setp(ax.get_xticklabels(), rotation=30, ha="right")

        plt.title(f"{name_plot}")

        filename = self.output_rep + name_plot.replace(" ", "_") + ".png"
        plt.savefig(filename)

    def plot_errors_for_acc(self, type_payment, df, name_plot=None, use_all=False):
        fig, ax = plt.subplots()

        if not use_all:
            all_accs = df.index.unique()
            acc = random.choice(all_accs)

            df = df.loc[acc, :]

        ax.scatter(
            x=df.loc[:, ("sum_conso", "mean")],
            y=df.loc[:, ("rel_error_index", "mean")] * 100,
        )  # *100 : to get percentages
        ax.set_xlabel("Consommation totale moyenne par ACC")
        ax.set_ylabel("% d'erreur sur l'index")

        if name_plot is None:
            if not use_all:
                name_plot = type_payment + "_errors_for_acc_" + pdl
            else:
                name_plot = type_payment + "_errors_for_all_accs"

        plt.setp(ax.get_xticklabels(), rotation=30, ha="right")

        plt.title(f"{name_plot}")

        filename = self.output_rep + name_plot.replace(" ", "_") + ".png"
        plt.savefig(filename)

    def plot_wm_usages(self, df, name_plot=None):
        fig, ax = plt.subplots()
        df["has_shiftable"] = df["has_shiftable"].apply(lambda boolean: 1 * boolean)
        df["detection"] = df["detection"].apply(
            lambda boolean: 0.5 * boolean
        )  # .5 and not 1: to distringuish the has_shiftable from detection
        columns_to_plot = ["has_shiftable", "detection"]
        df.reset_index(inplace=True)

        df.plot.scatter(
            ax=ax,
            x="date_and_time",
            y="has_shiftable",
            color="green",
            label="has_shiftable",
        )
        df.plot.scatter(
            ax=ax, x="date_and_time", y="detection", color="orange", label="detection"
        )

        if name_plot is None:
            name_plot = "wm_usages"

        plt.title(f"{name_plot}")
        filename = self.output_rep + name_plot.replace(" ", "_") + ".png"

        plt.savefig(filename)

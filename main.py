# import modules
import sys
import os
import logging


# functions
def deactivate_debug_log_messages():
    # logging.basicConfig(level=logging.ERROR)  #did not work
    loggers = [logging.getLogger(name) for name in logging.root.manager.loggerDict]
    for logger in loggers:
        logger.setLevel(logging.INFO)


def main():
    logging.basicConfig(level=logging.DEBUG)
    # TODO use load profiles coming from this site: https://www.loadprofilegenerator.de/

    # FIXME deactivating breakpoints
    os.environ["PYTHONBREAKPOINT"] = "0"

    all_file_path = ut.get_all_file_path_in_one_dir("./data/inputs/synthetic_data/")
    nb_periods_max = [3]
    predictor_type = "simple"
    coeff_shunshin_to_prode = [1900]
    nb_run = 1
    run_optimisation = True

    if type(all_file_path) == str:
        all_file_path = [all_file_path]

    for file in all_file_path:
        simconf = sconf.SimulationConfig(file, predictor_type=predictor_type)
        sim = sconf.RunSimulation(
            simconf,
            run_optimisation=run_optimisation,
            nb_runs=nb_run,
            nb_periods_max=nb_periods_max,
            coeff_sunshine=coeff_shunshin_to_prode,
        )
        result = sim.run_simulation()
    result.to_csv("simulation_result.csv")
    deactivate_debug_log_messages()

    # vis.collective_simulation_result_plot(collection_infos_df, cfg["output_rep"])


if __name__ == "__main__":
    sys.path.append("")

    from src.utils import util_functions as ut
    from src.utils import set_simulation_config as sconf
    from src.utils.visualisation import visualisation as vis

    main()

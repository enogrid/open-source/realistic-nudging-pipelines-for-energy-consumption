# Documentation
The project has been set up using the Poetry virtual environment. To run the simulation, the first step is to install Poetry and configure the virtual environment. This project contains the code for the article "Realistic Nudging through ICT Pipelines to Help
Improve Energy Self-Consumption for Management
in Energy Communities", soon to be available on arxiv.
## Installing Poetry
Install Poetry using pip
`pip install poetry`

## Installing dependencies
To install all the dependencies for your project, run:
`poetry install`

## Activating virtual environment
To activate the virtual environment, use the `shell` command:
`poetry shell`

# Dependencies
All dependencies for the project :  [here](pyproject.toml)
# Jupyter notebook

To run the examples in [notebooks](Notebook_Exemple), we need to intall jupyter notebook with the command : `poetry add jupyter`
and run it with the command `jupyter notebook` in poetry shell

Here is a straightforward example that introduces a basic use case of our simulator:

[Simple example](Notebook_Exemple/Simple_example.ipynb)

The notebooks below represent the results of our simulation, which are presented in our article:

[Section 4.2. Synthetic Data, Efficiency of Nudges.ipynb](Notebook_Exemple/Section_4_2_Synthetic_Data_Efficiency_of_Nudges.ipynb)

[Section 4.3. Synthetic data, Production not Predictive of Excess](Notebook_Exemple/Section_4_3_Synthetic_data_Production_not_Predictive_of_Excess.ipynb)

[Section 4.4. AMPds Dataset, individual building simulation](Notebook_Exemple/Section_4_4_AMPds_Dataset_individual_building_simulation.ipynb)

[Section 4.4. Iris Dataset , individual building simulation](Notebook_Exemple/Section_4_4_Iris_Dataset_individual_building_simulation.ipynb)

[Section 4.5. Iris dataset, collective runs](Notebook_Exemple/Section_4_5_Iris_dataset_collective_runs.ipynb)

[Section 4.5. Iris dataset, subsets runs](Notebook_Exemple/Section_4_5_Iris_dataset_subsets_runs.ipynb)

[Section 4.6. Influence of the Amount of Production](Notebook_Exemple/Section_4_6_Influence_of_the_Amount_of_Production.ipynb)

[Section 4.7. Influence of the Number of Green Periods](Notebook_Exemple/Section_4_7_Influence_of_the_Number_of_Green_Periods.ipynb)
